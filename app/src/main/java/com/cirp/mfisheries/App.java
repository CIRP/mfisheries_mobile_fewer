package com.cirp.mfisheries;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.cirp.mfisheries.core.AppDatabase;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public class App extends Application {

	private Tracker mTracker;
	private static final String DATABASE_NAME = "MFISHERIES_DB";
	private static AppDatabase db;

	synchronized public Tracker getDefaultTracker() {
		if (mTracker == null) {
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			mTracker = analytics.newTracker(R.xml.global_tracker);
		}
		return mTracker;
	}

	public static AppDatabase getDatabase(Context context) {
		if(db == null)
			db = Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME).allowMainThreadQueries().build();
		return db;
	}
}
