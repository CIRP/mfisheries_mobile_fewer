package com.cirp.mfisheries.firstaid;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.ModuleActivity;
import com.cirp.mfisheries.core.ModuleInfo;
import com.cirp.mfisheries.util.FileUtil;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

public class FirstAidActivity extends ModuleActivity {

	private String[] options;
	private FirstAidFragment firstAidFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		module = new FirstAid(this);
		loadData();
	}

	//loads downloaded module data and parses it
	public void loadData() {
		try {
			//load json
			String json = FileUtil.readJSONFile(module.getId() + "/module.json");
			//parse json
			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<ModuleInfo> jsonAdapter = moshi.adapter(ModuleInfo.class);

			ModuleInfo info = jsonAdapter.fromJson(json);
			options = info.options;
			displayData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//displays the downloaded first aid options in a list
	public void displayData() {
		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, options);
		ListView listView = (ListView) findViewById(R.id.listView);
		//shows fragment with multimedia content on click
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				showInfoFragment(options[position]);
			}
		});
		listView.setAdapter(adapter);
	}

	//shows multimedia fragment
	public void showInfoFragment(String type) {
		firstAidFragment = FirstAidFragment.newInstance(this, type, (FirstAid) module);
		getSupportFragmentManager().beginTransaction()
				.add(R.id.container, firstAidFragment)
				.addToBackStack(null)
				.commit();
	}

	@Override
	public void onPause() {
		if (firstAidFragment != null) {
			firstAidFragment.pauseAudio();
		}
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_first_aid;
	}

	@Override
	public int getColor(){
		return R.color.red;
	}

	@Override
	public int getColorDark(){
		return R.color.redDark;
	}
}
