package com.cirp.mfisheries.fewer;

import android.content.Context;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.messaging.FirebaseMessaging;


/**
 * Created by kyle on 2/13/17.
 */

public class Fewer extends Module {

    public Fewer(Context context){
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "FEWER";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "FEWER";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.cap;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = FewerActivity.class;
        return this;
    }

    @Override
    public void onInstalled() {
        Log.d(moduleId, name + " Module Installed, Launching check to ensure logged in");
        FirebaseMessaging.getInstance().subscribeToTopic("cap-" + PrefsUtil.getCountry(context).toLowerCase());
    }
}
