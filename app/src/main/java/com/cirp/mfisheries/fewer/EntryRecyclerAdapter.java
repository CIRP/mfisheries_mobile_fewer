package com.cirp.mfisheries.fewer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cirp.mfisheries.R;

import java.util.List;


public class EntryRecyclerAdapter extends RecyclerView.Adapter<EntryRecyclerAdapter.ViewHolder> {

    private final Context mContext;
    private List<Feed.Entry> mValues;

    public EntryRecyclerAdapter(Context context, List<Feed.Entry> items) {
        mValues = items;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cap_entry, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        if(holder.mItem.getTitle() != null)
            holder.titleTextView.setText(holder.mItem.getTitle().trim());
        if(holder.mItem.getName() != null)
            holder.authorTextView.setText(holder.mItem.getName().trim());
        if(holder.mItem.getUpdated() != null)
            holder.dateTextView.setText(holder.mItem.getUpdated().trim());
        if(holder.mItem.getCapExpires() != null)
            holder.expireTextView.setText(holder.mItem.getCapExpires().trim());
        if(holder.mItem.getCapAreaDesc() != null)
            holder.areaTextView.setText(holder.mItem.getCapAreaDesc().trim());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void update(List<Feed.Entry> entries){
        Log.d("CAP", "updating to " + entries.size());
        mValues = entries;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView titleTextView;
        public final TextView authorTextView;
        public final TextView dateTextView;
        public final TextView expireTextView;
        public final TextView areaTextView;

        public Feed.Entry mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            titleTextView = (TextView) view.findViewById(R.id.titleTextView);
            authorTextView = (TextView) view.findViewById(R.id.authorTextView);
            dateTextView = (TextView) view.findViewById(R.id.dateTextView);
            expireTextView = (TextView) view.findViewById(R.id.expireTextView);
            areaTextView = (TextView) view.findViewById(R.id.areaTextView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + titleTextView.getText() + "'";
        }
    }
}

