package com.cirp.mfisheries.fewer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Feed {

    private String title;
    private String xmlns;
    private String link;
    private String id;
    private String updated;
    private List<Entry> entry;
    private String generator;
    private String xmlnsCap;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @Override
    public String toString() {
        return "Feed{" +
                "title='" + title + '\'' +
                ", xmlns='" + xmlns + '\'' +
                ", link=" + link +
                ", id='" + id + '\'' +
                ", updated='" + updated + '\'' +
                ", cap_entry=" + entry +
                ", generator='" + generator + '\'' +
                ", xmlnsCap='" + xmlnsCap + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public List<Entry> getEntry() {
        return entry;
    }

    public void setEntry(List<Entry> entry) {
        this.entry = entry;
    }

    public String getGenerator() {
        return generator;
    }

    public void setGenerator(String generator) {
        this.generator = generator;
    }

    public String getXmlnsCap() {
        return xmlnsCap;
    }

    public void setXmlnsCap(String xmlnsCap) {
        this.xmlnsCap = xmlnsCap;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


    public static class Entry {

        private String capSeverity;
        private String capResponseType;
        private String capExpires;
        private String title;
        private String capCategory;
        private String link;
        private String id;
        private String capCertainty;
        private String updated;
        private String capAreaDesc;
        private String capUrgency;
        private String name;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        public Entry() {
        }

        public Entry(String capSeverity, String capResponseType, String capExpires, String title, String capCategory, String link, String id, String capCertainty, String updated, String capAreaDesc, String capUrgency, String name) {
            this.capSeverity = capSeverity;
            this.capResponseType = capResponseType;
            this.capExpires = capExpires;
            this.title = title;
            this.capCategory = capCategory;
            this.link = link;
            this.id = id;
            this.capCertainty = capCertainty;
            this.updated = updated;
            this.capAreaDesc = capAreaDesc;
            this.capUrgency = capUrgency;
            this.name = name;
        }

        public String getCapSeverity() {
            return capSeverity;
        }

        public void setCapSeverity(String capSeverity) {
            this.capSeverity = capSeverity;
        }

        public String getCapResponseType() {
            return capResponseType;
        }

        public void setCapResponseType(String capResponseType) {
            this.capResponseType = capResponseType;
        }

        public String getCapExpires() {
            return capExpires;
        }

        public void setCapExpires(String capExpires) {
            this.capExpires = capExpires;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCapCategory() {
            return capCategory;
        }

        public void setCapCategory(String capCategory) {
            this.capCategory = capCategory;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCapCertainty() {
            return capCertainty;
        }

        public void setCapCertainty(String capCertainty) {
            this.capCertainty = capCertainty;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getCapAreaDesc() {
            return capAreaDesc;
        }

        public void setCapAreaDesc(String capAreaDesc) {
            this.capAreaDesc = capAreaDesc;
        }

        public String getCapUrgency() {
            return capUrgency;
        }

        public void setCapUrgency(String capUrgency) {
            this.capUrgency = capUrgency;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

}