package com.cirp.mfisheries.fewer;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageButton;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.ModuleActivity;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.JsonArray;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class FewerActivity extends ModuleActivity {

    private final int TIMER_DELAY = 30000;
    private EntryRecyclerAdapter adapter;
    private ImageButton btnSpeak;
    private TextToSpeech tts;

    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_cap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (NetUtil.isOnline(this)){
            loadData();
        }else{
            displayErrorMessage();
        }

        // Setup Ability to talk to User
        //setUpSpeech();
    }

    private void displayErrorMessage(){
        new AlertDialog.Builder(this)
                .setTitle("FEWER CAP Module")
                .setMessage("Unfortunately, the CAP Demo cannot work without ")
                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        createDummyAlerts();
                    }
                }).setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void loadData() {
        Ion.with(this)
                .load("https://fewercapdemo.herokuapp.com/feed.json?audience=" + PrefsUtil.getCountry(this))
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        Log.d("CAP", "Data fetched: " + result);
                        List<Feed.Entry> entries = getEntries(result);

                        if(entries != null) {
                            Log.d("CAP", "fetched " + entries.size());
                            if(adapter == null) {
                                setupList(entries);
                                startTimer();
                            }
                            else {
                                adapter.update(entries);
                            }
                        }
                    }
                });
    }

    private List<Feed.Entry> getEntries(JsonArray result) {
        Moshi moshi = new Moshi.Builder().build();

        Type entryListType = Types.newParameterizedType(List.class, Feed.Entry.class);
        JsonAdapter<List<Feed.Entry>> jsonAdapter = moshi.adapter(entryListType);
        try {
            return jsonAdapter.fromJson(result.toString());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }

    private void setupList(List<Feed.Entry> entries){
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new EntryRecyclerAdapter(this, entries);
        recyclerView.setAdapter(adapter);
    }


    private void createDummyAlerts(){
        ArrayList<Feed.Entry> entries = new ArrayList<>();
        entries.add(
                new Feed.Entry("warning", "type", "March 28, 2017, 11:01 a.m","Alert Message","warning", "", "", "", "", "", "", "Tester")
        );
        entries.add(
                new Feed.Entry("warning", "type", "March 28, 2017, 11:01 a.m","Alert Message 2","warning", "", "", "", "", "", "", "Tester")
        );
        setupList(entries);
    }

    private void startTimer(){
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {
                            Log.d("CAP", "Running task");
                            loadData();
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        timer.schedule(task, TIMER_DELAY, TIMER_DELAY);
    }

/*    private void setUpSpeech() {
        btnSpeak = (ImageButton) findViewById(R.id.talkbtn);
        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                talkHelp();
            }
        });

        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("ShelterAct-TTS", "This Language is not supported");
                    } else {
                        btnSpeak.setEnabled(true);
                    }
                } else {
                    Log.e("ShelterAct-TTS", "Initialization Failed!");
                }
            }
        });

    }

    public void talkHelp() {
        String textHelp = ((TextView) findViewById(R.id.txtHelpContent)).getText().toString();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (tts != null) tts.speak(textHelp, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {

            if (tts != null) {
                //noinspection deprecation
                tts.speak(textHelp, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }
*/

}
