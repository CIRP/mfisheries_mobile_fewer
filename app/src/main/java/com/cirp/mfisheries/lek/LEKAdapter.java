package com.cirp.mfisheries.lek;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.nav.NavigationActivity;
import com.cirp.mfisheries.util.NetUtil;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class LEKAdapter extends ArrayAdapter<LEKPost> {

	private final Context context;
	private final List<LEKPost> posts;
	private MediaPlayer mediaPlayer;

	static class ViewHolder {
		public TextView user;
		public TextView text;
		public TextView location;
		public ImageView imageView;
		public TextView aDate;
		public VideoView videoView;
	}

	public LEKAdapter(Context context, List<LEKPost> posts) {
		super(context, R.layout.layout_lek, posts);
		this.context = context;
		this.posts = posts;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.layout_lek, parent, false);

			ViewHolder viewHolder = new ViewHolder();
			viewHolder.text = (TextView) rowView.findViewById(R.id.lek_text);
			viewHolder.location = (TextView) rowView.findViewById(R.id.lek_location);
			viewHolder.imageView = (ImageView) rowView.findViewById(R.id.lek_image);
			viewHolder.aDate = (TextView) rowView.findViewById(R.id.lek_date);
			viewHolder.videoView = (VideoView) rowView.findViewById(R.id.lek_video);
			rowView.setTag(viewHolder);
		}
		final LEKPost post = posts.get(position);

		Log.d("LEKAdapter", post.text + " " + post.media + " " + post.filetype);
		ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.text.setText(post.text);
		String location = post.latitude + ", " + post.longitude;
		holder.location.setText(location);
		holder.location.setTag(post);
		holder.location.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, NavigationActivity.class);
				LEKPost post1 = (LEKPost) v.getTag();
				intent.putExtra("name", post1.text);
				intent.putExtra("details", "hello world");
				intent.putExtra("lat", post1.latitude);
				intent.putExtra("lng", post1.longitude);
				intent.putExtra("timestamp", post1.timestamp);
				context.startActivity(intent);
			}
		});
		holder.aDate.setText(post.applicableDate);

		if (post.media == null || post.media.equals("")) {
			if (post.location == null) {
				holder.imageView.setVisibility(View.GONE);
				return rowView;
			}
			if (post.filetype.contains("image")) {
				Picasso.with(context)
						.load(NetUtil.SITE_URL + post.location)
						.resize(400, 400)
						.centerCrop()
						.into(holder.imageView);
			} else if (post.filetype.contains("audio")) {
				holder.imageView.setImageResource(R.drawable.ic_mic_none_white_48dp);
				holder.imageView.setBackgroundResource(R.color.accent);
				holder.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
				holder.imageView.setTag(post);
				holder.imageView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						LEKPost post1 = (LEKPost) v.getTag();
						createAudioPlayer(NetUtil.SITE_URL + post1.location);
					}
				});
			} else if (post.filetype.contains("video")) {
				holder.imageView.setVisibility(View.GONE);
				holder.videoView.setVisibility(View.VISIBLE);
				holder.videoView.setTag(post);
				String path = NetUtil.SITE_URL + post.location.replace(" ", "%20");
				holder.videoView.setVideoPath(path);
				MediaController controller = new MediaController(context);
				holder.videoView.setMediaController(controller);
				holder.videoView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						playVideo((VideoView) v);
					}
				});
			}
		} else {
			final File file = new File(post.media);
			if (post.filetype.contains("image")) {
				Picasso.with(context)
						.load(file)
						.resize(400, 400)
						.centerCrop()
						.into(holder.imageView);
			} else if (post.filetype.contains("audio")) {
				holder.imageView.setImageResource(R.drawable.ic_mic_none_white_48dp);
				holder.imageView.setBackgroundResource(R.color.accent);
				holder.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			} else if (post.filetype.contains("video")) {
				holder.imageView.setImageResource(R.drawable.ic_file_video_white_48dp);
				holder.imageView.setBackgroundResource(R.color.primary);
				holder.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			}
			holder.imageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent i = new Intent();
					i.setAction(android.content.Intent.ACTION_VIEW);
					i.setDataAndType(Uri.fromFile(file), post.filetype);
					context.startActivity(i);
				}
			});
		}

		return rowView;
	}

	@Override
	public void add(LEKPost post) {
		posts.add(post);
		this.notifyDataSetChanged();
	}

	private void killMediaPlayer() {
		if (mediaPlayer != null) {
			try {
				mediaPlayer.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void createAudioPlayer(String url) {
		final View view = LayoutInflater.from(context).inflate(R.layout.layout_audio_player, null);
		Button play = (Button) view.findViewById(R.id.play);
		play.setVisibility(View.GONE);
		play.setTag(url);
		play.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				v.setVisibility(View.GONE);
				playAudio((String) v.getTag());
			}
		});

		Button pause = (Button) view.findViewById(R.id.pause);
		pause.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				v.setVisibility(View.GONE);
				view.findViewById(R.id.play).setVisibility(View.VISIBLE);
				mediaPlayer.pause();
			}
		});

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Playing Audio");
		builder.setView(view);
		builder.show();
		url = url.replace(" ", "%20");
		Log.d("Audio", url + "");

		playAudio(url);
	}

	public void playAudio(String url) {
		try {
			killMediaPlayer();

			mediaPlayer = new MediaPlayer();
			mediaPlayer.setDataSource(url);
			mediaPlayer.prepare();
			mediaPlayer.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void stopAudio() {
		if (mediaPlayer != null && mediaPlayer.isPlaying()) {
			mediaPlayer.pause();
		}
	}

	private void playVideo(VideoView videoView) {
		videoView.start();
	}
}