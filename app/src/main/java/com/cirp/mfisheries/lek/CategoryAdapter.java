package com.cirp.mfisheries.lek;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.nav.NavigationActivity;
import com.cirp.mfisheries.util.NetUtil;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class CategoryAdapter extends ArrayAdapter<LEKCategory> {

	private final Context context;
	private final List<LEKCategory> categories;

	static class ViewHolder {
		public TextView name;
	}

	public CategoryAdapter(Context context, List<LEKCategory> categories) {
		super(context, R.layout.spinner_item2, categories);
		this.context = context;
		this.categories = categories;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.spinner_item2, parent, false);

			ViewHolder viewHolder = new ViewHolder();
			viewHolder.name = (TextView) rowView;
			rowView.setTag(viewHolder);
		}
		final LEKCategory category = categories.get(position);

		ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.name.setText(category.name);

		return rowView;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.spinner_item2, parent, false);

			ViewHolder viewHolder = new ViewHolder();
			viewHolder.name = (TextView) rowView;
			rowView.setTag(viewHolder);
		}
		final LEKCategory category = categories.get(position);

		ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.name.setText(category.name);
		return rowView;
	}
}