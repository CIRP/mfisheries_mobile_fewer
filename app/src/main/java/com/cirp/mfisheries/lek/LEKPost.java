package com.cirp.mfisheries.lek;

public class LEKPost {

	public int id;
	public String userid;
	public String username;
	public String text;
	public String latitude;
	public String longitude;
	public String filetype;
	public String media;
	public String timestamp;
	public String applicableDate;
	public String category;
	public boolean isSpecific;
	public boolean synced;
	public String location;
	public String countryid;

	public LEKPost() {
	}

	@Override
	public String toString() {
		return "LEKPost{" +
				", username='" + username + '\'' +
				", text='" + text + '\'' +
				", userid='" + userid + '\'' +
				", latitude='" + latitude + '\'' +
				", longitude='" + longitude + '\'' +
				", filetype='" + filetype + '\'' +
				", media='" + media + '\'' +
				", timestamp='" + timestamp + '\'' +
				", applicableDate='" + applicableDate + '\'' +
				", category='" + category + '\'' +
				", isSpecific=" + isSpecific +
				", synced=" + synced +
				", countryid=" + countryid +
				", location='" + location + '\'' +
				'}';
	}
}
