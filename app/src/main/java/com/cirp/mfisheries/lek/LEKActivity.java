package com.cirp.mfisheries.lek;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.core.location.LocationDependentActivity;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.LocationUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class LEKActivity extends LocationDependentActivity implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter, GoogleMap.OnInfoWindowClickListener, DatePickerDialog.OnDateSetListener, GoogleMap.OnInfoWindowCloseListener {

	private static final int REQUEST_IMAGE_GET = 0x1;
	private static final int REQUEST_AUDIO_GET = 0x3;
	private static final int REQUEST_VIDEO_GET = 0x2;
	private static final int REQUEST_NEW_IMAGE_GET = 0x4;
	private static final int REQUEST_NEW_VIDEO_GET = 0x5;
	private static final int PLACE_PICKER_REQUEST = 121;

	private static Date postDate;
	private String postCategory = "";

	private String currentFilePath;
	private String currentFileType = "text/*";
	private Uri currentFileUri;

	private Location selectedLocation;
	private DatabaseHandler database;
	private List<LEKPost> posts = new ArrayList<>();
	private GoogleMap map;
	private String selectedType = "all";
	private String selectedCategory = "all";
	private Date selectedDate;
	private String datePickerType = "post";
	private boolean offlinePosts;

	private TextView pickDateTextView;

	private int selectedPostIndex;

	private boolean IS_ZOOMED;
	private boolean FILTER_DISPLAYED;
	private boolean SHARE_DISPLAYED;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		module = new LEK(this);

		//initialize map
		MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);

		//set click listener for fab
		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.lek_fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				togglePostBox();
			}
		});

		//set today's date
		Calendar c = Calendar.getInstance();
		postDate = c.getTime();

		c.add(Calendar.DAY_OF_YEAR, -7);
		selectedDate = c.getTime();

		permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA,
				Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE};

		getLEKCategories();
		setUserFilter();
		setTypesFilter();
		setDatesFilter();

		setFilterDropdown();
	}

	private void setFilterDropdown() {
		findViewById(R.id.filter).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				View grid = findViewById(R.id.filter_grid);
				TextView filter = (TextView) findViewById(R.id.filter);
				if(FILTER_DISPLAYED) {
					grid.setVisibility(View.GONE);
					filter.setCompoundDrawablesWithIntrinsicBounds(null , null,
							getResources().getDrawable(R.drawable.ic_chevron_down_grey600_36dp), null);
				}else{
					grid.setVisibility(View.VISIBLE);
					filter.setCompoundDrawablesWithIntrinsicBounds(null , null,
							getResources().getDrawable(R.drawable.ic_chevron_up_grey600_36dp), null);
				}
				FILTER_DISPLAYED = !FILTER_DISPLAYED;
			}
		});
	}

	@Override
	public void onMapReady(GoogleMap map) {
		this.map = map;
		Location location = LocationService.userLocation;
		if(location != null) {
			LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
			this.map.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 9));
			IS_ZOOMED = true;
		}
		this.map.setOnInfoWindowClickListener(this);
		this.map.setInfoWindowAdapter(this);
		this.map.setOnInfoWindowCloseListener(this);
		if(NetUtil.isOnline(this))
			getLEK();
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			getLocalLEK(selectedDate, calendar.getTime());
		}
	}

	@Override
	public void onPermissionGranted(String permission) {
		getLocation();
	}

	private void getLocation(){
		selectedLocation = LocationService.userLocation;
	}

	//gets LEK data stored on server
	private void getLEK() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, 2);
		if(offlinePosts)
			getLocalLEK(selectedDate, calendar.getTime());
		else
			getLEK(NetUtil.GET_LEK_DATE_RANGE(PrefsUtil.getUserId(this), selectedType, selectedCategory, selectedDate, calendar.getTime()));
	}

	private void getLEKByDate(int day, int month, int year) {
		Log.d("LEK", "Getting LEK by date");
		getLEK(NetUtil.GET_LEK_DATE(PrefsUtil.getUserId(this), selectedType, selectedCategory, day, month, year));
	}

	private void getLEK(String path){
		Log.d("LEK", "Getting LEK at path " + path);
		if (NetUtil.isOnline(this)) {
			Ion.with(this)
					.load(path)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							try {
								clearMarkers();
								JsonArray array = result.getAsJsonArray("data");
								Moshi moshi = new Moshi.Builder().build();
								Type listOfLEKType = Types.newParameterizedType(List.class, LEKPost.class);
								JsonAdapter<List<LEKPost>> jsonAdapter = moshi.adapter(listOfLEKType);
								posts = jsonAdapter.fromJson(array.toString());
								addMarkers(posts);
							} catch (Exception exc) {
								exc.printStackTrace();
							}
						}
					});
		}
	}

	//loads all LEK saved locally on the device
	private class LEKDatabaseTask extends AsyncTask<Date, Void, Boolean> {

		protected Boolean doInBackground(Date... params) {
			try {
				Date date1 = params[0];
				Date date2 = params[1];
				database = new DatabaseHandler(LEKActivity.this, 5);
				database.open();
				posts = database.offlineLEKDBHelper.filterLEK(selectedCategory, selectedType, date1, date2);
				return true;
			} catch (Exception e) {
				Log.d("LEK", "LEK DB LOADING: " + e.getMessage());
			}
			return false;
		}

		protected void onPostExecute(Boolean finished) {
			try {
				if (finished && posts != null) {
                    Log.d("LEK", "LEK Size = " + posts.size());
					addMarkers(posts);
					database.close();
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void addMarkers(List<LEKPost> posts) {
		for(int i = 0; i < posts.size(); i ++){
			addMarker(posts.get(i), i);
		}
	}

	private void addMarker(LEKPost post, int i){
		LatLng latLng = new LatLng(Double.parseDouble(post.latitude), Double.parseDouble(post.longitude));
		Marker marker = map.addMarker(new MarkerOptions()
				.position(latLng)
				.title(post.text)
				.snippet(post.latitude + ", " + post.longitude));

		marker.setTag(i);
		if(!IS_ZOOMED){
			this.map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 9));
			IS_ZOOMED = true;
		}
	}

	private void clearMarkers(){
		map.clear();
		posts = new ArrayList<>();
	}

	@Override
	public View getInfoContents(Marker marker) {
		selectedPostIndex = (int)marker.getTag();
		LEKPost post = posts.get(selectedPostIndex);
		Log.d("LEK", "Getting info contents for " + post.toString());

		SHARE_DISPLAYED = true;
		invalidateOptionsMenu();

		View infoWindow = getLayoutInflater().inflate(R.layout.layout_lek_window, null);

		((TextView) infoWindow.findViewById(R.id.lek_user)).setText(post.username);
		((TextView) infoWindow.findViewById(R.id.lek_date)).setText(post.timestamp);
		((TextView) infoWindow.findViewById(R.id.lek_text)).setText(post.text);
		((TextView) infoWindow.findViewById(R.id.lek_category)).setText("Category: " + post.category);
		((TextView) infoWindow.findViewById(R.id.lek_location)).setText(LocationUtil.convertLatitude(Double.parseDouble(post.latitude)) +
				" " + LocationUtil.convertLongitude(Double.parseDouble(post.longitude)));

		String type = "";
		if(post.filetype.contains("image")){
			type = "image";
			((Button)infoWindow.findViewById(R.id.lek_button)).setText("Open " + type);
		}
		else if(post.filetype.contains("video")){
			type = "video";
			((Button)infoWindow.findViewById(R.id.lek_button)).setText("Open " + type);
		}
		else if(post.filetype.contains("audio")){
			type = "audio";
			((Button)infoWindow.findViewById(R.id.lek_button)).setText("Open " + type);
		}
		else{
			infoWindow.findViewById(R.id.lek_button).setVisibility(View.GONE);
		}

		return infoWindow;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		return null;
	}

	@Override
	public void onInfoWindowClose(Marker marker){
		SHARE_DISPLAYED = false;
		invalidateOptionsMenu();
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		selectedPostIndex = (int)marker.getTag();
		LEKPost post = posts.get(selectedPostIndex);
		if(post.filetype.contains("text"))
			return;

		Uri data;
		if(post.media == null)
			data = Uri.parse(NetUtil.SITE_URL + post.location);
		else
			data = getLocalFileUri(post);

		Log.d("Opening LEK", post.filetype + " " +  post.location);

		Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(data, post.filetype);
		intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		startActivity(intent);
	}

	private Uri getLocalFileUri(LEKPost post) {
		File file = new File(post.media);
		Uri uri = FileUtil.getOutputMediaFileUri(file);
		if (Build.VERSION.SDK_INT >= 24)
			uri = FileProvider.getUriForFile(this, getPackageName() + ".provider", file);
		return uri;
	}

	private void getLEKCategories(){
		Log.d("LEK", "Getting LEK Categories");
		if (NetUtil.isOnline(this)) {
			Ion.with(this)
					.load(NetUtil.GET_LEK_CATEGORIES(PrefsUtil.getUserId(this)))
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							try {
								JsonArray array = result.getAsJsonArray("data");
								Moshi moshi = new Moshi.Builder().build();
								Type listOfLEKType = Types.newParameterizedType(List.class, LEKCategory.class);
								JsonAdapter<List<LEKCategory>> jsonAdapter = moshi.adapter(listOfLEKType);
								List<LEKCategory> categories = new ArrayList();
								categories.add(new LEKCategory(0, "All"));
								categories.addAll(jsonAdapter.fromJson(array.toString()));
								setCategorySpinner(categories);

							} catch (Exception exc) {
								exc.printStackTrace();
							}
						}
					});
		}
	}

	private void setCategorySpinner(List<LEKCategory> categories){
		try {
			Spinner spinner = (Spinner) findViewById(R.id.categoriesSpinner);
			CategoryAdapter adapter = new CategoryAdapter(this, categories);

			spinner.setAdapter(adapter);
			spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
					selectedCategory = ((LEKCategory)adapterView.getItemAtPosition(i)).name.toLowerCase();
					Log.d("LEK", "Filtering by " + selectedCategory);
					getLEK();
				}

				@Override
				public void onNothingSelected(AdapterView<?> adapterView) { }
			});

			Spinner spinner2 = (Spinner) findViewById(R.id.selectCtgSpinner);
			CategoryAdapter adapter2 = new CategoryAdapter(this, categories);
			spinner2.setAdapter(adapter2);
			spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
					Log.d("LEK", "Selected category for post " + postCategory);
					postCategory = ((LEKCategory)adapterView.getItemAtPosition(i)).name.toLowerCase();
				}

				@Override
				public void onNothingSelected(AdapterView<?> adapterView) { }
			});

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setUserFilter(){
		Spinner spinner = (Spinner) findViewById(R.id.userSpinner);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				clearMarkers();
				String user = (String)adapterView.getItemAtPosition(i);
				if(user.equalsIgnoreCase("all")) {
					offlinePosts = false;
					getLEK();
				}else {
					offlinePosts = true;
					Calendar calendar = Calendar.getInstance();
					calendar.add(Calendar.DAY_OF_YEAR, 1);
					getLocalLEK(selectedDate, calendar.getTime());
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) { }
		});
	}

	private void getLocalLEK(Date earlier, Date later) {
		Log.d("LEK", "Getting locally saved LEK");
		clearMarkers();
		new LEKDatabaseTask().execute(earlier, later);
	}

	public void setTypesFilter(){
		Spinner spinner = (Spinner) findViewById(R.id.typeSpinner);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				selectedType = ((String)adapterView.getItemAtPosition(i)).toLowerCase();
				getLEK();
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) { }
		});
	}

	public void setDatesFilter(){
		Spinner spinner = (Spinner) findViewById(R.id.dateSpinner);
		ArrayAdapter adapter = new ArrayAdapter(this, R.layout.spinner_item2, getResources().getStringArray(R.array.lek_dates));
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				Log.d("LEK", "item selected");
				Calendar calendar = Calendar.getInstance();
				if(i == 0){
					calendar.add(Calendar.DAY_OF_YEAR, -7);
					selectedDate = calendar.getTime();
				}
				else if (i == 1){
					calendar.add(Calendar.MONTH, -1);
					selectedDate = calendar.getTime();
				}
				else if (i == 2){
					calendar.add(Calendar.YEAR, -1);
					selectedDate = calendar.getTime();
				}
				else {
					pickDateTextView = (TextView)view;
					datePickerType = "filter";
					DatePickerFragment datePickerFragment = new DatePickerFragment();
					datePickerFragment.setListener(LEKActivity.this);
					datePickerFragment.show(getSupportFragmentManager(), "datePicker");
					return;
				}
				getLEK();
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {
				Log.d("LEK", "nothing selected");
			}
		});
	}

	//toggles display of box to create a new lek post
	public void togglePostBox() {
		requestPermissions();
		FrameLayout layout = (FrameLayout) findViewById(R.id.post_box);
		if (layout.isShown())
			layout.setVisibility(View.GONE);
		else {
			layout.setVisibility(View.VISIBLE);
			Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH) + 1;
			int day = c.get(Calendar.DAY_OF_MONTH);

			Button datePicker = (Button) findViewById(R.id.datePicker);
			datePicker.setText(day + "/" + month + "/" + year);

			if (selectedLocation != null) {
				showMapPreview(selectedLocation);
			}
		}
	}

	//shows an image of the username's current location on a map
	public void showMapPreview(Location location) {
		String url = "http://maps.google.com/maps/api/staticmap?center=" +
				location.getLatitude() + "," + location.getLongitude() +
				"&zoom=" + 14 + "&scale=2&size=" + 960 + "x" + 960 +
				"&sensor=false&markers=color:red%7C" + location.getLatitude() + "," + location.getLongitude();
		Picasso.with(this)
				.load(url)
				.into((ImageView) findViewById(R.id.mapView));

	}

	//attaches different types of media
	public void attach(View view) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		String[] items = {"Attach Image", "Attach Video", "Attach Audio", "New Image", "New Video"};
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);

		builder.setAdapter(arrayAdapter,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case 0:
								attachImage();
								return;
							case 1:
								attachVideo();
								return;
							case 2:
								attachAudio();
								return;
							case 3:
								attachNewImage();
								return;
							case 4:
								attachNewVideo();
						}
					}
				});
		builder.show();
	}

	//attach audio files
	public void attachAudio() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
		intent.setType("audio/*");
		if (intent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(intent, REQUEST_AUDIO_GET);
		}
	}

	//attach video files
	public void attachVideo() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
		intent.setType("video/*");
		if (intent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(intent, REQUEST_VIDEO_GET);
		}
	}

	//attach image files
	public void attachImage() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		if (intent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(intent, REQUEST_IMAGE_GET);
		}
	}

	public void attachNewImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File file = FileUtil.getOutputMediaFile(this, "");
		currentFileUri = FileUtil.getOutputMediaFileUri(file);
		currentFilePath = currentFileUri.getPath();
		if (Build.VERSION.SDK_INT >= 24)
			currentFileUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", file);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, currentFileUri);

		// start the image capture Intent
		startActivityForResult(intent, REQUEST_NEW_IMAGE_GET);
	}

	public void attachNewVideo() {
		Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		File file = FileUtil.getOutputMediaFile(this, FileUtil.generateVideoName(this));
		currentFileUri = FileUtil.getOutputMediaFileUri(file);
		currentFilePath = currentFileUri.getPath();
		if (Build.VERSION.SDK_INT >= 24)
			currentFileUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", file);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, currentFileUri);

		// start the image capture Intent
		startActivityForResult(intent, REQUEST_NEW_IMAGE_GET);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		ImageView thumb = (ImageView) findViewById(R.id.thumbnail);
		if (requestCode == REQUEST_IMAGE_GET && resultCode == RESULT_OK) {
			Uri uri = data.getData();
			Picasso.with(this).load(uri).resize(200, 200).into(thumb);
			currentFilePath = FileUtil.getAbsolutePath(this, uri, "image");
			currentFileType = "image/*";
		}
		if (requestCode == REQUEST_AUDIO_GET && resultCode == RESULT_OK) {
			Uri uri = data.getData();
			thumb.setImageResource(R.drawable.ic_mic_none_white_48dp);
			thumb.setBackgroundColor(ContextCompat.getColor(this, R.color.primary));
			currentFilePath = FileUtil.getAbsolutePath(this, uri, "audio");
			currentFileType = FileUtil.getFileType("audio", currentFilePath);
		}
		if (requestCode == REQUEST_VIDEO_GET && resultCode == RESULT_OK) {
			Uri uri = data.getData();
			Picasso.with(this).load(uri).into(thumb);
			thumb.setImageResource(R.drawable.ic_file_video_white_48dp);
			thumb.setBackgroundColor(ContextCompat.getColor(this, R.color.primary));
			currentFilePath = FileUtil.getAbsolutePath(this, uri, "video");
			currentFileType = FileUtil.getFileType("video", currentFilePath);
		}
		if (requestCode == REQUEST_NEW_IMAGE_GET && resultCode == RESULT_OK) {
			Picasso.with(this).load(currentFileUri).resize(200, 200).into(thumb);
			currentFileType = "image/*";
		}
		if (requestCode == REQUEST_NEW_VIDEO_GET && resultCode == RESULT_OK) {
			Picasso.with(this).load(currentFileUri).into(thumb);
			thumb.setImageResource(R.drawable.ic_file_video_white_48dp);
			thumb.setBackgroundColor(ContextCompat.getColor(this, R.color.primary));
			currentFileType = "video/mp4";
		}
		if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
			Place place = PlacePicker.getPlace(this, data);
			String toastMsg = String.format("Place: %s", place.getName());
			Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
			selectedLocation = new Location("GPS");
			selectedLocation.setLatitude(place.getLatLng().latitude);
			selectedLocation.setLongitude(place.getLatLng().longitude);
			showMapPreview(selectedLocation);
		}
	}

	//creates an LEKPost object with all entered data
	public LEKPost createLEKPost() {
		try {
			if (selectedLocation == null) {
				Toast.makeText(LEKActivity.this, "Could not get your location", Toast.LENGTH_LONG).show();
				return null;
			}
			EditText editText = (EditText) findViewById(R.id.lek_text2);
			LEKPost lekPost = new LEKPost();
			lekPost.latitude = selectedLocation.getLatitude() + "";
			lekPost.longitude = selectedLocation.getLongitude() + "";
			lekPost.applicableDate = postDate.toString();
			String text = editText.getText().toString();
			if (text.equals("")) {
				Toast.makeText(LEKActivity.this, "Must enter some text", Toast.LENGTH_LONG).show();
				return null;
			}
			lekPost.text = text;
			lekPost.timestamp = new Date().toString();
			lekPost.media = currentFilePath;
			lekPost.filetype = currentFileType;
			if(postCategory.equals("all"))
				lekPost.category = "";
			else
				lekPost.category = postCategory;
			Log.d("LEK", PrefsUtil.getUserId(this) + " " + currentFilePath);
			lekPost.userid = PrefsUtil.getUserId(this) + "";
			lekPost.username = PrefsUtil.getUser(this);
			lekPost.countryid = PrefsUtil.getCountryId(this);
			return lekPost;
		} catch (Exception e) {
			e.printStackTrace();
		}
		Toast.makeText(this, "Error saving post, try again", Toast.LENGTH_SHORT).show();
		return null;
	}

	//uploads data to the server
	public synchronized void uploadData(View view) {
		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.posting));
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(true);
		progress.setCancelable(false);
		progress.show();

		final LEKPost post = createLEKPost();
		if (post == null) {
			progress.dismiss();
			return;
		}

		if(!NetUtil.isOnline(this)){
			PrefsUtil.setLekToSync(LEKActivity.this, true);
			saveLEK(post);
			progress.dismiss();
			Toast.makeText(LEKActivity.this, "Offline: Post will be uploaded when online", Toast.LENGTH_LONG).show();
			return;
		}
		File file = null;
		if (post.media != null)
			file = new File(post.media);

		FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				if (e == null && result != null) {
					try {
						Log.d("LEK", "Post Result: " + result.toString());
						int status = result.get("status").getAsInt();
						if (status == 201) {
							post.synced = true;
							Toast.makeText(LEKActivity.this, "Post successful!", Toast.LENGTH_LONG).show();
							togglePostBox();
							clearMarkers();
							posts.add(post);
							addMarkers(posts);
						}
					} catch (Exception exc) {
						PrefsUtil.setLekToSync(LEKActivity.this, true);
						exc.printStackTrace();
						Toast.makeText(LEKActivity.this, "Error: " + exc.getMessage(), Toast.LENGTH_LONG).show();
					}
				} else if (e != null) {
					PrefsUtil.setLekToSync(LEKActivity.this, true);
					Toast.makeText(LEKActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
				}

				saveLEK(post);
				progress.dismiss();
			}
		};

		sendLEK(post, file, callback);
	}

	public void sendLEK(@NonNull final LEKPost post, File file, FutureCallback<JsonObject> callback) {
		//if file attached
		if (file != null) {
			Ion.with(this)
					.load(NetUtil.API_URL + NetUtil.ADD_LEK_PATH)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.setMultipartFile("file", post.filetype, file)
					.setMultipartParameter("latitude", post.latitude)
					.setMultipartParameter("longitude", post.longitude)
					.setMultipartParameter("text", post.text)
					.setMultipartParameter("filetype", post.filetype)
					.setMultipartParameter("isSpecific", "true")
					.setMultipartParameter("userid", post.userid)
					.setMultipartParameter("aDate", post.applicableDate)
					.setMultipartParameter("category", post.category)
					.setMultipartParameter("countryid", post.countryid)
					.asJsonObject()
					.setCallback(callback);
		} else {//if just text
			Ion.with(this)
					.load(NetUtil.API_URL + NetUtil.ADD_LEK_PATH)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.setMultipartParameter("latitude", post.latitude)
					.setMultipartParameter("longitude", post.longitude)
					.setMultipartParameter("text", post.text)
					.setMultipartParameter("filetype", "text/*")
					.setMultipartParameter("isSpecific", "true")
					.setMultipartParameter("userid", post.userid)
					.setMultipartParameter("aDate", post.applicableDate)
					.setMultipartParameter("category", post.category)
					.setMultipartParameter("countryid", post.countryid)
					.asJsonObject()
					.setCallback(callback);
		}
	}

	//save LEK to database
	public void saveLEK(LEKPost post) {
		database = new DatabaseHandler(this, 5);
		database.open();
		database.offlineLEKDBHelper.addOfflineLEK(post);
		database.close();
	}

	//shows place picker
	public void pickPlace(View view) {
		if (NetUtil.isOnline(this)) {
			try {
				PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
				startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	//shows data picker
	public void pickDate(View view) {
		datePickerType = "post";
		DatePickerFragment datePickerFragment = new DatePickerFragment();
		datePickerFragment.setListener(this);
		datePickerFragment.show(getSupportFragmentManager(), "datePicker");
	}

	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		int actualMonth = month + 1;
		String date = day + "/" + actualMonth + "/" + year;
		if(datePickerType.equals("post")) {

			Calendar c = Calendar.getInstance();
			c.set(year, month, day);
			postDate = c.getTime();

			Button datePicker = (Button) findViewById(R.id.datePicker);
			datePicker.setText(date);
		}
		else{
			pickDateTextView.setText(date);
			getLEKByDate(day, month, year);
		}
	}

	public static class DatePickerFragment extends DialogFragment {

		int day, month, year = 0;
		DatePickerDialog.OnDateSetListener listener;

		public void setListener(DatePickerDialog.OnDateSetListener listener){
			this.listener = listener;
		}

		@Override
		@NonNull
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			Calendar c = Calendar.getInstance();
			if (year == 0) {
				year = c.get(Calendar.YEAR);
				month = c.get(Calendar.MONTH);
				day = c.get(Calendar.DAY_OF_MONTH);
			}
			DatePickerDialog dialog = new DatePickerDialog(getActivity(), listener, year, month, day);
			dialog.getDatePicker().setMaxDate(new Date().getTime());
			// Create a new instance of DatePickerDialog and return it
			return dialog;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_lek, menu);
		Log.d("LEK", "creating menu");
		MenuItem menuItem = menu.findItem(R.id.action_share);
		menuItem.setVisible(SHARE_DISPLAYED);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_share) {
			sharePost();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void sharePost() {
		LEKPost post = posts.get(selectedPostIndex);
		String text = post.text + " by " + post.username;
		Log.d("LEK", "Sharing post " + post.filetype);
		if(!post.filetype.contains("text")){
			text += " " + NetUtil.SITE_URL + post.location;
		}
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, text);
		sendIntent.setType("text/plain");
		startActivity(sendIntent);
	}

	@Override
	public void onBackPressed() {
		FrameLayout layout = (FrameLayout) findViewById(R.id.post_box);
		if (layout.isShown())
			layout.setVisibility(View.GONE);
		else
			super.onBackPressed();
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_lek;
	}
}
