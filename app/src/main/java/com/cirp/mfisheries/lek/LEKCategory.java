package com.cirp.mfisheries.lek;

public class LEKCategory {

	public int id;
	public String name;

	public LEKCategory() {
	}

	public LEKCategory(int id, String name) {
		this.id = id;
		this.name = name;
	}
}
