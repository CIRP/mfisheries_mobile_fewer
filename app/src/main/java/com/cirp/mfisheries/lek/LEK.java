package com.cirp.mfisheries.lek;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;

public class LEK extends Module {

	public LEK(Context context){
		super(context);
	}

	@Override
	protected Module setModuleId() {
		this.moduleId = "LEK";
		return this;
	}

	@Override
	protected Module setModuleName() {
		this.name = "LEK";
		return this;
	}

	@Override
	protected Module setIsDisplayed() {
		this.displayed = false;
		return this;
	}

	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_lek;
		return this;
	}

	@Override
	protected Module setNeedsRegistration() {
		this.needsRegistration = true;
		return this;
	}

	@Override
	protected Module setActivityClass() {
		this.activityClass = LEKActivity.class;
		return this;
	}
}
