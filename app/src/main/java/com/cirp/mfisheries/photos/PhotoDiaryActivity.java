package com.cirp.mfisheries.photos;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.cirp.mfisheries.MainActivity;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.ModuleActivity;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PhotoDiaryActivity extends ModuleActivity {

	private static int userid;

	private Button uploadButton;
	private ImageView imgPreview;
	private Uri photoUri;
	private String imgPath;
	private static String imageName;

	public static final int MEDIA_TYPE_IMAGE = 1;
	private static final int RESULT_LOAD_IMG = 2;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		module = new PhotoDiary(this);

		userid = PrefsUtil.getUserId(this);

		this.imgPreview = (ImageView) findViewById(R.id.imgPreview);
		this.uploadButton = (Button) findViewById(R.id.uploadButton);
		this.uploadButton.setVisibility(View.GONE);

		permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
		requestPermissions();
	}

	public void openCamera(View view) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		imageName = FileUtil.generateImageName(this);
		File file = FileUtil.getOutputMediaFile(this, imageName);
		photoUri = FileUtil.getOutputMediaFileUri(file);

		imgPath = photoUri.getPath();

		if (Build.VERSION.SDK_INT >= 24)
			photoUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", file);

		System.out.println(photoUri.toString() + " here");

		intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}

	//opens the gallery application
	public void openGallery(View view) {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		if (intent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(intent, RESULT_LOAD_IMG);
		}
	}

	//building json to send to server
	public void uploadImage(View view) {
		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.upload_photo));
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(true);
		progress.setCancelable(false);
		progress.show();

		JsonObject jsonObject = createJsonObject(userid, imageName, imgPath);
		Ion.with(getApplicationContext())
				.load(NetUtil.API_URL + NetUtil.POST_IMAGE_URL)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.setJsonObjectBody(jsonObject)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						try {
							progress.dismiss();
							if (e == null) {
                                Toast.makeText(getApplicationContext(), "Image uploaded", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(PhotoDiaryActivity.this, MainActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(), "Image upload failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				});
	}

	@NonNull
	private JsonObject createJsonObject(int userid, String imageName, String decodableString) {
		Location location = LocationService.userLocation;
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("userid", userid);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d hh:mm:ss", Locale.ENGLISH);
		jsonObject.addProperty("deviceTimestamp", format.format(new Date()));
		jsonObject.addProperty("imgname", imageName);
		jsonObject.addProperty("data", encodeImage(decodableString));
		if (location == null) {
			jsonObject.addProperty("hasgps", "no");
			jsonObject.addProperty("latitude", "null");
			jsonObject.addProperty("longitude", "null");
		} else {
			jsonObject.addProperty("latitude", location.getLatitude());
			jsonObject.addProperty("longitude", location.getLongitude());
			jsonObject.addProperty("hasgps", "yes");
		}
		return jsonObject;
	}

	//encoding the image
	public String encodeImage(String decodeableString) {
		/*Bitmap bm = BitmapFactory.decodeFile(decodeableString);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int h = 480; // height in pixels
		int w = 640; // width in pixels
		Bitmap scaled = Bitmap.createScaledBitmap(bm, h, w, true);

		scaled.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] b = baos.toByteArray();

		return Base64.encodeToString(b, Base64.DEFAULT);*/

		//TODO resize image
		byte[] b = FileUtil.getByteArray(this, photoUri);
		return Base64.encodeToString(b, Base64.DEFAULT);

	}

	private void previewCapturedImage() {
		try {
			imgPreview.setVisibility(View.VISIBLE);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = 32;

			Bitmap bitmap;
			//if (Build.VERSION.SDK_INT >= 24)
				bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri);
			bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth()/4, bitmap.getHeight()/4, true);
			//else
			//	bitmap = BitmapFactory.decodeFile(imgPath, options);
			imgPreview.setImageBitmap(bitmap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				previewCapturedImage();
				this.uploadButton.setVisibility(View.VISIBLE);

			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(getApplicationContext(), "Image capture canceled", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getApplicationContext(), "Failed to capture image", Toast.LENGTH_SHORT).show();
			}
		}

		if (requestCode == RESULT_LOAD_IMG) {
			if (resultCode == RESULT_OK && data != null) {
				photoUri = data.getData();
				imgPath = FileUtil.getAbsolutePath(this, photoUri, "image");
				//this.imgDecodableString = imgPath;
				imageName = FileUtil.generateImageName(this);
				previewCapturedImage();
				this.uploadButton.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable("photo_uri", photoUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		photoUri = savedInstanceState.getParcelable("photo_uri");
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_photo_diary;
	}

	@Override
	public int getColor(){
		return R.color.blue;
	}

	@Override
	public int getColorDark(){
		return R.color.blueDark;
	}
}