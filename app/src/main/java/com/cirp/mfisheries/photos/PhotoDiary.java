package com.cirp.mfisheries.photos;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Module;

public class PhotoDiary extends Module {

	public PhotoDiary(Context context){
		super(context);
	}

	@Override
	protected Module setModuleId() {
		this.moduleId = "PhotoDiary";
		return this;
	}

	@Override
	protected Module setModuleName() {
		this.name = "Photo Diary";
		return this;
	}

	@Override
	protected Module setIsDisplayed() {
		this.displayed = true;
		return this;
	}

	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_photos;
		return this;
	}

	@Override
	protected Module setNeedsRegistration() {
		this.needsRegistration = true;
		return this;
	}

	@Override
	protected Module setActivityClass() {
		this.activityClass = PhotoDiaryActivity.class;
		return this;
	}
}
