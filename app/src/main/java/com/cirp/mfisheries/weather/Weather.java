package com.cirp.mfisheries.weather;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Module;

public class Weather extends Module {

	public Weather(Context context) {
		super(context);
	}

	@Override
	protected Module setModuleId() {
		this.moduleId = "Weather";
		return this;
	}

	@Override
	protected Module setModuleName() {
		this.name = "Weather";
		return this;
	}

	@Override
	protected Module setIsDisplayed() {
		this.displayed = false;
		return this;
	}

	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_weather;
		return this;
	}

	@Override
	protected Module setNeedsRegistration() {
		this.needsRegistration = false;
		return this;
	}

	@Override
	protected Module setActivityClass() {
		this.activityClass = WeatherActivity.class;
		return this;
	}

}
