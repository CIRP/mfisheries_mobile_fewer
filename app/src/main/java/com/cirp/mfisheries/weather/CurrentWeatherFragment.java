package com.cirp.mfisheries.weather;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.weather.models.CurrentWeather;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CurrentWeatherFragment extends Fragment {

	private static final String TAG = "CurrentWeather";
	private static double lat;
	private static double lng;
	private static Context mContext;
	private static Module module;
	public static String FILE_NAME = "weather.json";
	private View currentView;

	public static CurrentWeatherFragment newInstance(Context context, double latitude, double longitude, Weather wModule) {
		CurrentWeatherFragment fragment = new CurrentWeatherFragment();
		lat = latitude;
		lng = longitude;
		mContext = context;
		module = wModule;
		return fragment;
	}

	public CurrentWeatherFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_current_weather, container, false);
		currentView = view;
		String file = module.getName() + "/" + FILE_NAME;
		//checks if data is cached and loads into view if it is
		if (FileUtil.fileExists(Environment.getExternalStorageDirectory().getPath() + FileUtil.FOLDER + file)) {
			String json = FileUtil.readJSONFile(file);
			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<CurrentWeather> jsonAdapter = moshi.adapter(CurrentWeather.class);

			try {
				CurrentWeather data = jsonAdapter.fromJson(json);
				displayData(data);
			} catch (Exception e) {
				e.printStackTrace();
				Log.v("JSONError", "Error: " + e.getLocalizedMessage());
			}
		}
		//checks if online and loads data from api
		if (NetUtil.isOnline(mContext)) {
			Log.i(TAG, "Is Online");
			getWeather(lat, lng);
		}
		return view;
	}

	public void getWeather(double lat, double lng) {
		Ion.with(this)
                .load(mContext.getString(R.string.weather_api_current, lat, lng, mContext.getString(R.string.weather_appid)))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            //caches data
                            FileUtil.writeJSONFile(result.toString(), "/" + module.getName() + "/", FILE_NAME);
                            //parses json
                            Moshi moshi = new Moshi.Builder().build();
                            JsonAdapter<CurrentWeather> jsonAdapter = moshi.adapter(CurrentWeather.class);
                            CurrentWeather data = jsonAdapter.fromJson(result.toString());

                            displayData(data);
                        } catch (Exception exc) {
                            exc.printStackTrace();
                        }
                    }
                });
	}

	//displays current weather data in view
	public void displayData(CurrentWeather data) {
		ProgressBar progressBar = (ProgressBar) currentView.findViewById(R.id.weather_progress);
		progressBar.setVisibility(View.GONE);

		TextView temp = (TextView) currentView.findViewById(R.id.weather_temp);
		temp.setText(Math.round(data.main.temp) + " \u00B0" + "C");

		TextView condition = (TextView) currentView.findViewById(R.id.weather_condition);
		condition.setText(data.weather[0].main);

		ImageView imageView = (ImageView) currentView.findViewById(R.id.weather_icon);
		imageView.setImageResource(CurrentWeather.getIcon(data.weather[0].icon, mContext));

		TextView sunset = (TextView) currentView.findViewById(R.id.weather_sunset);
		SimpleDateFormat format = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
		sunset.setText(getString(R.string.sunset) + ": " + format.format(new Date(data.sys.sunset * 1000)));

		TextView sunrise = (TextView) currentView.findViewById(R.id.weather_sunrise);
		sunrise.setText(getString(R.string.sunrise) + ": " + format.format(new Date(data.sys.sunrise * 1000)));

		TextView cloudy = (TextView) currentView.findViewById(R.id.weather_cloudy);
		cloudy.setText(getString(R.string.percent_cloudy) + ": " + data.clouds.all + "%");
	}

}
