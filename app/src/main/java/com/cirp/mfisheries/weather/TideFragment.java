package com.cirp.mfisheries.weather;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;

public class TideFragment extends Fragment {

	//opens the external tide app
	public void openTideApp(Context context) {
		Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.globaltide&hl=en");
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		context.startActivity(intent);
		//return super.onCreateView(inflater, container, savedInstanceState);
	}
}
