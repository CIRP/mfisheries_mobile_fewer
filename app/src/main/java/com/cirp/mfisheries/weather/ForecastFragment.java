package com.cirp.mfisheries.weather;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.weather.models.Forecast;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

public class ForecastFragment extends Fragment {

	private static double lat;
	private static double lng;
	private static Context mContext;
	private static Module module;
	public static String FILE_NAME = "forecast.json";
	private View currentView;

	public static ForecastFragment newInstance(Context context, double latitude, double longitude, Weather wModule) {
		ForecastFragment fragment = new ForecastFragment();
		lat = latitude;
		lng = longitude;
		mContext = context;
		module = wModule;
		return fragment;
	}

	public ForecastFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_forecast, container, false);
		currentView = view;
		String file = module.getName() + "/" + FILE_NAME;
		//check if data is cached and loads into the view
		if (FileUtil.fileExists(Environment.getExternalStorageDirectory().getPath() + FileUtil.FOLDER + file)) {
			String json = FileUtil.readJSONFile(file);
			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<Forecast> jsonAdapter = moshi.adapter(Forecast.class);

			try {
				Forecast data = jsonAdapter.fromJson(json);
				displayData(data);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//if online loads data from api
		if (NetUtil.isOnline(mContext)) {
			getForecast(lat, lng);
		}
		return view;
	}

	public void getForecast(double lat, double lng) {
		Ion.with(this)
                .load(mContext.getString(R.string.weather_api_forecast, lat, lng, 5, mContext.getString(R.string.weather_appid)))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            //caches data
                            FileUtil.writeJSONFile(result.toString(), "/" + module.getName() + "/", FILE_NAME);

                            //parses json
                            Moshi moshi = new Moshi.Builder().build();
                            JsonAdapter<Forecast> jsonAdapter = moshi.adapter(Forecast.class);
                            Forecast data = jsonAdapter.fromJson(result.toString());

                            displayData(data);
                        } catch (Exception exc) {
                            exc.printStackTrace();
                        }
                    }
                });
	}

	//creates the 5-day forecast listview
	public void displayData(Forecast data) {
		ListView listView = (ListView) currentView.findViewById(R.id.forecast_list_view);
		ForecastAdapter adapter = new ForecastAdapter(mContext, data.list);
		listView.setAdapter(adapter);
	}

}
