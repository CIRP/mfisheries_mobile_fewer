package com.cirp.mfisheries.weather;

import android.Manifest;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.location.LocationActivity;

import java.util.ArrayList;
import java.util.List;

public class WeatherActivity extends LocationActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		module = new Weather(this);

		permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
		requestPermissions();

		setupViewPager();
	}

	@Override
	public void onPermissionGranted(String permission) {
		super.onPermissionGranted(permission);
		setupViewPager();
	}

	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
		stopLocationUpdates();
	}

	//sets up the fragments for each page of the screen
	private void setupViewPager() {
		final ViewPager viewPager = (ViewPager) findViewById(R.id.tabanim_viewpager);
		//gts current location
		if (mCurrentLocation == null) {
			//if current location not available, gets last known location
			mCurrentLocation = new Location("Weather");
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
			mCurrentLocation.setLatitude(preferences.getFloat("lat", 0));
			mCurrentLocation.setLongitude(preferences.getFloat("lng", 0));
		}

		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
		//adds fragments to viewpager adapter
		adapter.addFrag(CurrentWeatherFragment.newInstance(this, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), (Weather) module), "Weather");
		adapter.addFrag(ForecastFragment.newInstance(this, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), (Weather) module), "Forecast");
		final TideFragment tideFragment = new TideFragment();
		adapter.addFrag(tideFragment, "Tide");

		//sets adapter to viewpager
		viewPager.setAdapter(adapter);
		TabLayout tabLayout = (TabLayout) findViewById(R.id.tabanim_tabs);
		tabLayout.setupWithViewPager(viewPager);

		//opens the external tide app when the last fragment is selected
		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

			}

			@Override
			public void onPageSelected(int position) {
				if (position == 2)
					tideFragment.openTideApp(WeatherActivity.this);
			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});
	}

	static class ViewPagerAdapter extends FragmentPagerAdapter {
		private final List<Fragment> mFragmentList = new ArrayList<>();
		private final List<String> mFragmentTitleList = new ArrayList<>();

		public ViewPagerAdapter(FragmentManager manager) {
			super(manager);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragmentList.get(position);
		}

		@Override
		public int getCount() {
			return mFragmentList.size();
		}

		public void addFrag(Fragment fragment, String title) {
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return mFragmentTitleList.get(position);
		}
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_weather;
	}

	@Override
	public int getColor(){
		return R.color.blue;
	}

	@Override
	public int getColorDark(){
		return R.color.blueDark;
	}
}
