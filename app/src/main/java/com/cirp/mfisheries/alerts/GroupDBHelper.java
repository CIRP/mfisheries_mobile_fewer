package com.cirp.mfisheries.alerts;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.cirp.mfisheries.core.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

public class GroupDBHelper {

	private static final String GROUP_TABLE = "GroupTable";
	private static final String GROUP_ID = "groupId";
	private static final String GROUP_NAME = "groupName";
	private static final String GROUP_SUBSCRIPTION = "isSubscribed";
	private static final String GROUP_ONE_WAY = "oneWay";

	public static String CREATE_GROUP_TABLE =
			"CREATE TABLE " + GROUP_TABLE + " ("
					+ GROUP_ID + " INTEGER PRIMARY KEY,"
					+ GROUP_NAME + " TEXT,"
					+ GROUP_SUBSCRIPTION + " TEXT,"
					+ GROUP_ONE_WAY + " INTEGER" + ")";


	public float addGroup(GroupDM groupDM) {
		if (findGroupById(groupDM.id)) {
			Log.d("AlertDB", "group in db");
			return 0;
		} else {
			try {
				Log.d("AlertDB", "Reaching in the try");
				ContentValues groupValues = new ContentValues();
				groupValues.put(GROUP_ID, groupDM.id);
				groupValues.put(GROUP_NAME, groupDM.groupName);
				groupValues.put(GROUP_SUBSCRIPTION, groupDM.isSubscribed + "");
				groupValues.put(GROUP_ONE_WAY, groupDM.oneWay + "");
				return DatabaseHandler.database.insert(GROUP_TABLE, null, groupValues);
			} catch (Exception ex) {
				Log.wtf("mFisheries", "Alert DB Helper, Add Alert, Error adding alert" + ex.getLocalizedMessage());
			}
		}
		return 0;
	}

	public List<GroupDM> getAllGroups() {
		List<GroupDM> groups = new ArrayList<>();
		try {
			String selectQuery = "SELECT  * FROM " + GROUP_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					GroupDM group = new GroupDM();
					group.id = cursor.getInt(0);
					group.groupName = cursor.getString(1);
					group.isSubscribed = Boolean.parseBoolean(cursor.getString(2));
					group.oneWay = cursor.getInt(3);
					groups.add(group);
				}
				while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception ex) {
			Log.d("Group DB Helper", "get all groups, Error: " + ex.getLocalizedMessage());
		}
		return groups;
	}


	public GroupDM findGroup(int groupId) {
		GroupDM groupDM = new GroupDM();
		Log.d("GroupDB", "1");
		try {
			String selectQuery = "SELECT  * FROM " + GROUP_TABLE + " WHERE '" + GROUP_ID + "' = " + groupId;
			Log.d("GroupDB", "2");
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				Log.d("GroupDB", "3" + cursor.getInt(0) + cursor.getString(1) + cursor.getString(3));
				groupDM.id = cursor.getInt(0);
				groupDM.groupName = cursor.getString(1);
				groupDM.isSubscribed = Boolean.parseBoolean(cursor.getString(2));
				groupDM.oneWay = cursor.getInt(3);
			}
			cursor.close();
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Alert DB Helper, get Alerts, Error " + ex.getLocalizedMessage());
		}
		return groupDM;
	}

	public boolean findGroupById(int groupId) {
		String selectQuery = "SELECT  * FROM " + GROUP_TABLE + " WHERE '" + GROUP_ID + "' = '" + groupId + "'";
		Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			cursor.close();
			return true;
		} else {
			cursor.close();
			return false;
		}
	}

	public String findGroupNamebyId(int groupId) {
		GroupDM group_info = new GroupDM();
		String selectQuery = "SELECT  * FROM " + GROUP_TABLE + " WHERE '" + GROUP_ID + "' = '" + groupId + "'";
		Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			group_info.id = cursor.getInt(0);
			group_info.groupName = cursor.getString(1);
			group_info.isSubscribed = Boolean.parseBoolean(cursor.getString(2));
		}
		cursor.close();
		return group_info.groupName;
	}

	public void updateGroupSubscription(GroupDM group) {
		updateGroupSubscription(group.groupName, group.isSubscribed);
	}

	public void updateGroupSubscription(String groupName, boolean isSubscribed) {
		String updateQuery = "UPDATE " + GROUP_TABLE + " SET " + GROUP_SUBSCRIPTION + " = '" + isSubscribed + "' WHERE " + GROUP_NAME + " = '" + groupName + "'";
		DatabaseHandler.database.execSQL(updateQuery);
	}
}
