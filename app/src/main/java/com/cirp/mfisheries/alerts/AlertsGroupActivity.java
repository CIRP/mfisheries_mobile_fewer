package com.cirp.mfisheries.alerts;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.core.ModuleActivity;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class AlertsGroupActivity extends ModuleActivity {

	private SwipeRefreshLayout swLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		module = new Alerts(this);
		retrieveGroups();
		setUpSwipeRefresh();
	}

	private void retrieveGroups(){
		if (NetUtil.isOnline(getApplicationContext())) {
			getGroupsFromServer();
		} else {
			Toast.makeText(this, "Application is offline. Using cached groups.", Toast.LENGTH_SHORT).show();
			getLocalGroups();
		}
	}

	private void setUpSwipeRefresh(){
		swLayout = (SwipeRefreshLayout)findViewById(R.id.alert_group_swiperefresh);
		swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				retrieveGroups();
			}
		});

	}

	public void getLocalGroups(){
		List<GroupDM> groupsFromDB = new ArrayList<>();
		try {
			DatabaseHandler database = new DatabaseHandler(AlertsGroupActivity.this, 6);
			database.open();
			groupsFromDB = database.groupDBHelper.getAllGroups();
			database.close();
		} catch (Exception e) {
			Log.d("Alert Group", "Error " + e.getMessage());
		}
		displayGroups(groupsFromDB);
	}

	//getting a list of country relevant groups from the server
	public void getGroupsFromServer() {
		int uId = PrefsUtil.getUserId(this);

		if (swLayout != null){
			swLayout.setRefreshing(true);
		}

		Log.d("Get Groups", NetUtil.API_URL + NetUtil.USER_GROUPS(uId));
		//building json
		Ion.with(getApplicationContext())
				.load(NetUtil.API_URL + NetUtil.USER_GROUPS(uId))
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						if (e == null && result != null) {
							Log.d("Alerts Group", "Result: " + result.toString());
							int status = result.get("status").getAsInt();
							if (status == 200) {
								try {
									JsonArray array = result.getAsJsonArray("data");
									Moshi moshi = new Moshi.Builder().build();
									Type listOfGroupDMType = Types.newParameterizedType(List.class, GroupDM.class);
									JsonAdapter<List<GroupDM>> jsonAdapter = moshi.adapter(listOfGroupDMType);
									List<GroupDM> groups = jsonAdapter.fromJson(array.toString());
									//adding to the database
									try {
										DatabaseHandler database = new DatabaseHandler(AlertsGroupActivity.this, 6);
										database.open();
										for (GroupDM group : groups) {
											Log.d("GroupDirection", group.oneWay + "");
											database.groupDBHelper.addGroup(group);
										}
										database.close();
									} catch (Exception exc) {
										Log.d("Alert Group", "Error adding groups in " + exc.getLocalizedMessage());
									}
									displayGroups(groups);
									if (swLayout != null){
										swLayout.setRefreshing(false);
									}
								} catch (Exception ex) {
									Log.d("Alert Group", "Failure to parse groups" + ex.getMessage());
								}
							}
						} else {
                            Log.d("alert", "Groups not fetched from server");
							getLocalGroups();
						}
					}
				});
	}

	//displaying groups from the database
	public void displayGroups(List<GroupDM> groups) {
		ListView listView = (ListView) findViewById(R.id.groupList);
		final GroupAdapter groupAdapter = new GroupAdapter(this, groups);
		listView.setAdapter(groupAdapter);

		ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
		progressBar.setVisibility(View.GONE);
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_alerts_group;
	}

	@Override
	public int getColor(){
		return R.color.yellow;
	}

	@Override
	public int getColorDark(){
		return R.color.yellowDark;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_refresh)
			retrieveGroups();
		return super.onOptionsItemSelected(item);
	}
}
