package com.cirp.mfisheries.alerts;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.location.LocationActivity;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

//{"deviceTimestamp":"2017-7-11 12:34:43","dislikes":0,"groupid":2,"id":0,"isPublic":false,"isVerified":false,"latitude":"0.0","likes":0,"longitude":"0.0","messageDescription":"Heavy Rain","messageSeverity":"1 - Good","messagecontent":"Bad Weather","userid":392,"username":"Kyle De Freitas"}

public class SendAlertActivity extends LocationActivity {

	private static final String TAG = "AlertActivity";
	private Spinner alertType;
	private SharedPreferences preferences;
	private String message;
	private String weatherDescription  = "";
	private String weatherSeverity = "0 - Not Applicable";
	private int groupId;
	private boolean sendToAll;

	private final int COORDINATE_RADIUS = 15;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Layout configured in Parent. XML Files is specified in method getLayoutResourceId

		// Retrieve information from previous activity
		Intent intent = getIntent();
		groupId = intent.getIntExtra("groupId", 0);
		String group = intent.getStringExtra("group");
		((TextView) findViewById(R.id.groupName)).setText(group);

		module = new Alerts(this);


		// Setup UI Components
		setUpIsAllListener();
		setupAlertType();
		setupWeatherSpinners();
	}

	private void setUpIsAllListener(){
		CheckBox checkbox = (CheckBox) findViewById(R.id.checkBox);
		checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				sendToAll = isChecked;
			}
		});
	}

	private void setupWeatherSpinners() {
		final Spinner weatherTypeSpinner = (Spinner) findViewById(R.id.weatherTypeSpinner);
		((ArrayAdapter)weatherTypeSpinner.getAdapter()).setDropDownViewResource(R.layout.custom_spinner_item);
		final Spinner weatherSeveritySpinner = (Spinner) findViewById(R.id.weatherSeveritySpinner);
		((ArrayAdapter)weatherSeveritySpinner.getAdapter()).setDropDownViewResource(R.layout.custom_spinner_item);
	}


	private void setupAlertType() {

		alertType = (Spinner) findViewById(R.id.alertSpinner);
//		getting the alert types from res/values/strings.xml
//		String[] options = getResources().getStringArray(R.array.alert_types);
//		ArrayAdapter<String> optionAdapter = new ArrayAdapter<>(SendAlertActivity.this, R.layout.large_spinner_item, options);
//		optionAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
//		alertType.setAdapter(optionAdapter);

		((ArrayAdapter)alertType.getAdapter()).setDropDownViewResource(R.layout.custom_spinner_item);

		alertType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				Log.d(TAG, "option selected");
				TextView textView = (TextView) view;
				String option = "";
				if(textView != null)
					option = textView.getText().toString();
				Log.d(TAG, "option = " + option);
				EditText editText = (EditText) findViewById(R.id.alertText);

				final int other_visibility = (option.equalsIgnoreCase("other"))?View.VISIBLE :View.GONE;
				editText.setVisibility(other_visibility);

				final int [] weather_ui_components = new int [] {
						R.id.lblWeatherType,
						R.id.weatherTypeSpinner,
						R.id.lblWeatherSeverity,
						R.id.weatherSeveritySpinner
				};
				final int weather_visibility = (option.equalsIgnoreCase("bad weather"))?View.VISIBLE :View.GONE;
				for (int ui_component : weather_ui_components){
					View weather_view = findViewById(ui_component);
					weather_view.setVisibility(weather_visibility);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});
	}

	//gets the message from the dropdown/textbox
	public void sendAlert(View view) {
		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.posting));
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(true);
		progress.setCancelable(false);
		progress.show();

		message = alertType.getSelectedItem().toString();
		EditText alertMessage = (EditText) findViewById(R.id.alertText);
		if (message.equalsIgnoreCase("other")) {
			message = message + " " + alertMessage.getText().toString();
		}

		weatherDescription = "";
		weatherSeverity = "0 - Not Applicable";
		if (message.equalsIgnoreCase("bad weather")){
			weatherDescription = ((Spinner)findViewById(R.id.weatherTypeSpinner)).getSelectedItem().toString();
			weatherSeverity = ((Spinner)findViewById(R.id.weatherSeveritySpinner)).getSelectedItem().toString();
		}


		AlertDM alert = createAlert();
		Log.d("Alerts:", "Alert created successfully");

		JsonObject jsonObject = alert.getJsonObject();

		FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				progress.dismiss();
				if (e == null && result != null) {
					Log.d("Alert", "alert sent " + result.toString());
					Toast.makeText(getApplicationContext(), "Alert sent!", Toast.LENGTH_SHORT).show();
					onBackPressed();
				} else {
					if (e != null) {
						Log.d("Alert", "Alert not sent " + e.getMessage());
					} else {
						Log.d("Alert", "Alert not sent");
					}
					Toast.makeText(getApplicationContext(), "Sorry, try again", Toast.LENGTH_SHORT).show();
				}
			}
		};

		sendAlert(jsonObject, callback);
	}

	//creating an alert
	private AlertDM createAlert() {
		AlertDM alert = new AlertDM();
		alert.userid = PrefsUtil.getUserId(this);
		alert.groupid = groupId;
		alert.username = PrefsUtil.getUser(this);

		if (mCurrentLocation != null) {
			alert.latitude = String.valueOf(mCurrentLocation.getLatitude());
			alert.longitude = String.valueOf(mCurrentLocation.getLongitude());
		} else {
			preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			alert.latitude = String.valueOf(preferences.getFloat("lat", 0));
			alert.longitude = String.valueOf(preferences.getFloat("lng", 0));
		}

		SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d hh:mm:ss", Locale.ENGLISH);
		alert.deviceTimestamp = format.format(new Date());

		alert.messagecontent = message;
		alert.messageDescription = weatherDescription;
		alert.messageSeverity = weatherSeverity;

		alert.isPublic = sendToAll;
		return alert;
	}

	public void sendAlert(JsonObject jsonObject, FutureCallback<JsonObject> callback) {
		Log.d("SendAlertActivity", String.format("Sending%s to %s%s", jsonObject.toString(), NetUtil.API_URL, NetUtil.ALERTS));
		//building JSON
		Ion.with(getApplicationContext())
			.load("POST", NetUtil.API_URL + NetUtil.ALERTS)
			.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
			.setJsonObjectBody(jsonObject)
			.asJsonObject()
			.setCallback(callback);
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_send_alert;
	}

	@Override
	public int getColor(){
		return R.color.yellow;
	}

	@Override
	public int getColorDark(){
		return R.color.yellowDark;
	}

}