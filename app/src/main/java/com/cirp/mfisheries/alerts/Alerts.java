package com.cirp.mfisheries.alerts;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Module;

public class Alerts extends Module {

	public Alerts(Context context){
		super(context);
	}

	@Override
	protected Module setModuleId() {
		this.moduleId = "Alerts";
		return this;
	}

	@Override
	protected Module setModuleName() {
		this.name = "Alerts";
		return this;
	}

	@Override
	protected Module setIsDisplayed() {
		this.displayed = false;
		return this;
	}

	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_alert;
		return this;
	}

	@Override
	protected Module setNeedsRegistration() {
		this.needsRegistration = true;
		return this;
	}

	@Override
	protected Module setActivityClass() {
		this.activityClass = AlertsGroupActivity.class;
		return this;
	}
}
