package com.cirp.mfisheries.alerts;

public class GroupDM {

	public int id;
	public String groupName;
	public boolean isSubscribed;
	public int oneWay;

	public GroupDM(int id, String groupName, boolean isSubscribed, int oneWay) {
		this.id = id;
		this.groupName = groupName;
		this.isSubscribed = isSubscribed;
		this.oneWay = oneWay;
	}

	public GroupDM() {
	}
}
