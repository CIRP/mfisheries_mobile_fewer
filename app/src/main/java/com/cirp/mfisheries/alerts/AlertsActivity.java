package com.cirp.mfisheries.alerts;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.core.location.LocationActivity;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.reflect.Type;
import java.util.List;

public class AlertsActivity extends LocationActivity {

	private static final String TAG = "AlertActivity";
	private String group;
	private int groupId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		group = intent.getStringExtra("groupName");
		groupId = intent.getIntExtra("groupId", 0);
		int oneWay = intent.getIntExtra("oneWay", 0);

		getSupportActionBar().setTitle(group);

		module = new Alerts(this);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.alertsFab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AlertsActivity.this, SendAlertActivity.class);
				intent.putExtra("groupId", groupId);
				intent.putExtra("group", group);
				startActivity(intent);
			}
		});
		if(oneWay == 1){
			fab.setVisibility(View.GONE);
		}

		getAlerts();
	}

	public void getAlerts() {
		Log.d(TAG, NetUtil.API_URL + NetUtil.ALERTS +"?group=" + groupId);
		Ion.with(getApplicationContext())
				.load(NetUtil.API_URL + NetUtil.ALERTS +"?group=" + groupId)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonArray()
				.setCallback(new FutureCallback<JsonArray>() {
					@Override
					public void onCompleted(Exception e, JsonArray result) {
						if (e == null && result != null) {
							try {
								Moshi moshi = new Moshi.Builder().build();
								Type listOfAlertDMType = Types.newParameterizedType(List.class, AlertDM.class);
								JsonAdapter<List<AlertDM>> jsonAdapter = moshi.adapter(listOfAlertDMType);
								List<AlertDM> alerts = jsonAdapter.fromJson(result.toString());

								displayAlerts(alerts);
							}
							catch (Exception exc){
								exc.printStackTrace();
							}
						} else {
							Log.d(TAG, "Error: " + e.getMessage());
							getLocalAlerts();
						}
					}
				});
	}

	private void displayAlerts(List<AlertDM> alerts) {
		ListView listView = (ListView) findViewById(R.id.msgList);
		listView.setAdapter(new AlertAdapter(AlertsActivity.this, alerts));

		ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
		progressBar.setVisibility(View.GONE);
	}

	public void getLocalAlerts() {
		try {
			DatabaseHandler database = new DatabaseHandler(AlertsActivity.this, 4);
			database.open();
			List<AlertDM> messages = database.alertDBHelper.getAlertsByGroupId(groupId);
			Log.d(TAG, "Local Alerts Found: " + messages.size());
			database.close();

			displayAlerts(messages);

		} catch (Exception ex) {
			Log.wtf("mFisheries", "Alerts, Error loading alerts by group in act" + ex.getLocalizedMessage());
		}
	}

	public void unsubscribeFromGroup() {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(String.valueOf(groupId));

		int userid = PrefsUtil.getUserId(this);
		JsonObject object = new JsonObject();
		object.addProperty("userid", userid);
		object.addProperty("gcm", PrefsUtil.getToken(this));

		Log.d(TAG, NetUtil.API_URL + NetUtil.SUBSCRIBE +"/" + userid + "/" + groupId);
        Log.d(TAG, object.toString());
		//building JSON
		Ion.with(getApplicationContext())
				.load("DELETE", NetUtil.API_URL + NetUtil.SUBSCRIBE +"/" + userid + "/" + groupId)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.setJsonObjectBody(object)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						if (e == null && result != null) {
							Log.d("Alerts", "Unsubscription complete " + result.toString());
							try {
								//updating the group subscription in database
								DatabaseHandler database = new DatabaseHandler(getApplicationContext(), 6);
								database.open();
								database.groupDBHelper.updateGroupSubscription(group, false);
								database.close();

								Toast.makeText(getApplicationContext(), "Unsubscribed Successfully", Toast.LENGTH_SHORT).show();

								//redirecting to AlertsGroupActivity
								Intent intent = new Intent(AlertsActivity.this, AlertsGroupActivity.class);
								startActivity(intent);
							} catch (Exception exc) {
								Log.d("Alerts", "Error updating subscription" + exc.getLocalizedMessage());
								Toast.makeText(getApplicationContext(), "Sorry, try again", Toast.LENGTH_SHORT).show();
							}
						} else {
							if (e != null) {
								Log.d("Alerts", "Subscription error " + e.getMessage());
							} else {
								Log.d("Alerts", "Subscription error");
							}
							Toast.makeText(getApplicationContext(), "Sorry, Cannot unsubscribe, Try again", Toast.LENGTH_SHORT).show();
						}
					}
				});
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_alerts;
	}

	@Override
	public int getColor(){
		return R.color.yellow;
	}

	@Override
	public int getColorDark(){
		return R.color.yellowDark;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_alerts, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.unsubscribe) {
			unsubscribeFromGroup();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}