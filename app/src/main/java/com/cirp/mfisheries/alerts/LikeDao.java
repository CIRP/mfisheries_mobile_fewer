package com.cirp.mfisheries.alerts;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface LikeDao {

    @Query("SELECT * FROM Like WHERE alertId = :alertId AND "
            + "state = :state LIMIT 1")
    Like findById(int alertId, boolean state);

    @Insert
    void insertAll(Like... Likes);

    @Delete
    void delete(Like Like);
}