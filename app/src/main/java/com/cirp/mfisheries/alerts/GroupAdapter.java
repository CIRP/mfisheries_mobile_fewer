package com.cirp.mfisheries.alerts;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.List;

public class GroupAdapter extends ArrayAdapter<GroupDM> {
	private final Context context;
	private final List<GroupDM> groups;

	public static class ViewHolder {
		public View view;
		public GroupDM group;
		public TextView groupName;
		public Button subscribeBtn;
		public boolean isSubscribed;
	}

	public GroupAdapter(Context context, List<GroupDM> groups) {
		super(context, R.layout.layout_group, groups);
		this.context = context;
		this.groups = groups;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.layout_group, parent, false);

			ViewHolder viewHolder = new ViewHolder();
			viewHolder.view = rowView;
			viewHolder.groupName = (TextView) rowView.findViewById(R.id.groupLabel);
			viewHolder.subscribeBtn = (Button) rowView.findViewById(R.id.groupSubscribeBtn);
			rowView.setTag(viewHolder);
		}

		final GroupDM group = groups.get(position);

		final ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.group = group;
		holder.groupName.setText(group.groupName);
		holder.isSubscribed = group.isSubscribed;
		Log.d("GroupOneWay", group.oneWay + "");

		if (!group.isSubscribed) {
			holder.subscribeBtn.setVisibility(View.VISIBLE);
			holder.subscribeBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					subscribe(group, holder);
				}
			});
		} else {
			holder.subscribeBtn.setVisibility(View.GONE);
		}

		holder.view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (holder.isSubscribed) {
					Intent intent = new Intent(context, AlertsActivity.class);
					intent.putExtra("groupName", group.groupName);
					intent.putExtra("groupId", group.id);
					intent.putExtra("oneWay", group.oneWay);
					context.startActivity(intent);
				} else {
					Toast.makeText(context, "You need to subscribe", Toast.LENGTH_SHORT).show();
				}
			}
		});

		return rowView;
	}

	public void subscribe(final GroupDM group, final ViewHolder holder) {
        FirebaseMessaging.getInstance().subscribeToTopic(String.valueOf(group.id));

		JsonObject object = new JsonObject();
		object.addProperty("userid", PrefsUtil.getUserId(context));
		object.addProperty("groupid", group.id);
		object.addProperty("gcm", PrefsUtil.getToken(context));

		Log.d("GroupAdapter", NetUtil.API_URL + NetUtil.SUBSCRIBE);
		Log.d("GroupAdapter", object.toString());

		Ion.with(getContext())
            .load("POST", NetUtil.API_URL + NetUtil.SUBSCRIBE)
			.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
			.setJsonObjectBody(object)
            .asJsonObject()
            .setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    if (e == null && result != null) {
                        Toast.makeText(getContext(), "Subscription successful", Toast.LENGTH_SHORT).show();
                        Log.d("Alert Group", "Subscription complete " + result.toString());
						group.isSubscribed = true;

						if(holder != null) {
							holder.isSubscribed = true;
							holder.subscribeBtn.setVisibility(View.GONE);
						}
						saveSubscription(group);
					} else if (e != null) {
                        Log.d("Alert Group", "Subscription error " + e.getMessage());
                        Toast.makeText(getContext(), "Sorry, cannot subscribe", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d("Alert Group", "Subscription error");
                        Toast.makeText(getContext(), "Sorry, cannot subscribe", Toast.LENGTH_SHORT).show();
                    }
                }
            });
	}

	public void saveSubscription(GroupDM group) {
		try {
            DatabaseHandler database = new DatabaseHandler(getContext(), 6);
            database.open();
            database.groupDBHelper.updateGroupSubscription(group);
            database.close();
        } catch (Exception exc) {
            Log.d("Alert Group", "Error updating subscription" + exc.getLocalizedMessage());
            Toast.makeText(getContext(), "Sorry, cannot update subscription", Toast.LENGTH_SHORT).show();
        }
	}
}


