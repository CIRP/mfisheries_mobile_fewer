package com.cirp.mfisheries.alerts;

import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

public class AlertDM {

	public int id;
	public int userid;
	public int groupid;
	public String username;
	public String latitude;
	public String longitude;
	public String deviceTimestamp;
	public String messagecontent;
	public String messageDescription;
	public String messageSeverity;
	public boolean isPublic;
	public boolean isVerified;
	public int likes;
	public int dislikes;

	public AlertDM() {
	}

	public AlertDM(int userid, int groupid, String latitude, String longitude, String deviceTimestamp, String messagecontent) {
		this.userid = userid;
		this.groupid = groupid;
		this.latitude = latitude;
		this.longitude = longitude;
		this.deviceTimestamp = deviceTimestamp;
		this.messagecontent = messagecontent;
		this.isPublic = false;
		this.isVerified = false;
	}

	public AlertDM(int userid, int groupid, String latitude, String longitude, String deviceTimestamp, String messagecontent, String description, String severity) {
		this.userid = userid;
		this.groupid = groupid;
		this.latitude = latitude;
		this.longitude = longitude;
		this.deviceTimestamp = deviceTimestamp;
		this.messagecontent = messagecontent;
		this.isPublic = false;
		this.isVerified = false;
		this.messageDescription = description;
		this.messageSeverity = severity;
	}

	public JsonObject getJsonObject() {
		try {
			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<AlertDM> jsonAdapter = moshi.adapter(AlertDM.class);
			String json = jsonAdapter.toJson(this);
			Log.d("Alerts:", "JSON: " + json);
			JsonParser parser = new JsonParser();
			return (JsonObject) parser.parse(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
