package com.cirp.mfisheries.alerts;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.AppDatabase;
import com.cirp.mfisheries.nav.MapMarkerDM;
import com.cirp.mfisheries.nav.NavigationActivity;
import com.cirp.mfisheries.util.LocationUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.List;

public class AlertAdapter extends ArrayAdapter<AlertDM> {
	private final Context context;
	private final List<AlertDM> alerts;
	private AppDatabase database;

	private static class ViewHolder {
		public CardView view;
		public AlertDM alert;
		public TextView msgDate;
		public TextView msgSender;
		public TextView msgBody;
		public TextView msgLocation;
		public Button likeButton;
		public Button dislikeButton;
		public TextView verified;
		public Like like;
		public Like dislike;
	}

	public AlertAdapter(Context context, List<AlertDM> alerts) {
		super(context, R.layout.layout_alert, alerts);
		this.context = context;
		this.alerts = alerts;
		database = App.getDatabase(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.layout_alert, parent, false);

			ViewHolder viewHolder = new ViewHolder();
			viewHolder.view = (CardView)rowView;
			viewHolder.msgDate = (TextView) rowView.findViewById(R.id.msgDate);
			viewHolder.msgSender = (TextView) rowView.findViewById(R.id.msgSender);
			viewHolder.msgBody = (TextView) rowView.findViewById(R.id.msgBody);
			viewHolder.msgLocation = (TextView) rowView.findViewById(R.id.msgLocation);
			viewHolder.likeButton = (Button) rowView.findViewById(R.id.likeButton);
			viewHolder.dislikeButton = (Button) rowView.findViewById(R.id.dislikeButton);
			viewHolder.verified = (TextView) rowView.findViewById(R.id.verified);

			rowView.setTag(viewHolder);
		}

		final AlertDM alert = alerts.get(position);

		final ViewHolder holder = (ViewHolder) rowView.getTag();

		if(alert.isVerified){
			holder.view.setCardBackgroundColor(context.getResources().getColor(R.color.transGreen));
			holder.verified.setVisibility(View.VISIBLE);
		}
		else {
			holder.view.setCardBackgroundColor(context.getResources().getColor(android.R.color.white));
			holder.verified.setVisibility(View.GONE);
		}

		holder.alert = alert;
		holder.msgDate.setText(alert.deviceTimestamp);
		holder.msgSender.setText(alert.username);
		holder.msgBody.setText(alert.messagecontent);

		//set location
		String loc = LocationUtil.getLocationString(Double.parseDouble(alert.latitude), Double.parseDouble(alert.longitude));
		MapMarkerDM mapMarkerDM = getMapMarker(alert);
		holder.msgLocation.setTag(mapMarkerDM);
		holder.msgLocation.setText(loc);
		holder.msgLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				openMap(view);

			}
		});

		//like and dislike setup
		holder.likeButton.setText(String.valueOf(alert.likes));
		holder.likeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(holder.like == null) {
					holder.alert.likes += 1;
					updateAlert(holder.alert);
					updateLike(alert.id, true, holder.alert.likes, (Button)v, R.drawable.ic_liked);
				}
			}
		});

		holder.like = database.likeDao().findById(alert.id, true);
		if(holder.like != null) {
			holder.likeButton.setCompoundDrawablesWithIntrinsicBounds
					(context.getResources().getDrawable(R.drawable.ic_liked), null, null, null);
		}
		else {
			holder.likeButton.setCompoundDrawablesWithIntrinsicBounds
					(context.getResources().getDrawable(R.drawable.ic_action_like), null, null, null);
		}

		holder.dislikeButton.setText(String.valueOf(alert.dislikes));
		holder.dislikeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(holder.dislike == null) {
					holder.alert.dislikes += 1;
					updateAlert(holder.alert);
					updateLike(alert.id, false, holder.alert.dislikes, (Button)v, R.drawable.ic_disliked);
				}
			}
		});

		holder.dislike = database.likeDao().findById(alert.id, false);
		if(holder.dislike != null) {
			holder.dislikeButton.setCompoundDrawablesWithIntrinsicBounds
					(context.getResources().getDrawable(R.drawable.ic_disliked), null, null, null);
		}
		else {
			holder.dislikeButton.setCompoundDrawablesWithIntrinsicBounds
					(context.getResources().getDrawable(R.drawable.ic_action_dislike), null, null, null);
		}

		return rowView;
	}

	private void updateLike(int alertId, boolean state, int count, Button button, @DrawableRes int icon){
		database.likeDao().insertAll(new Like(alertId, state));
		button.setText(String.valueOf(count));
		button.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(icon), null, null, null);
	}

	@NonNull
	private MapMarkerDM getMapMarker(AlertDM alert) {
		MapMarkerDM mapMarkerDM = new MapMarkerDM();
		mapMarkerDM.name = alert.userid + "";
		mapMarkerDM.details = alert.messagecontent;
		mapMarkerDM.latitude = alert.latitude;
		mapMarkerDM.longitude = alert.longitude;
		return mapMarkerDM;
	}

	private void updateAlert(AlertDM alert) {
		JsonObject jsonObject = alert.getJsonObject();

		Log.d("AlertAdapter", NetUtil.API_URL + NetUtil.ALERTS);
		Log.d("AlertAdapter", jsonObject.toString());

		Ion.with(context)
				.load("PUT", NetUtil.API_URL + NetUtil.ALERTS + "/" + alert.id)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.setJsonObjectBody(jsonObject)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						if(result != null) {
							Log.d("UpdateAlert", result.toString() + "");
						}
					}
				});
	}

	private void openMap(View view) {
		MapMarkerDM list = (MapMarkerDM) view.getTag();

		Intent intent = new Intent(context, NavigationActivity.class);
		intent.putExtra("name", list.name);
		intent.putExtra("details", list.details);
		intent.putExtra("lat", list.latitude);
		intent.putExtra("lng", list.longitude);
		context.startActivity(intent);
	}
}

