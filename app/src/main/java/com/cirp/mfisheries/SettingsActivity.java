package com.cirp.mfisheries;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.cirp.mfisheries.core.BaseActivity;
import com.cirp.mfisheries.util.PrefsUtil;

public class SettingsActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		if (PrefsUtil.hasSOSExtraPhone(this)) {
			EditText extra = (EditText) findViewById(R.id.sosExtra);
			extra.setText(PrefsUtil.getSOSExtraPhone(this));
		}
	}

	public void saveToPrefs(View v) {
		EditText extra = (EditText) findViewById(R.id.sosExtra);
		String num = extra.getText().toString();

		Log.d("SOS SETTINGS:", "Number " + num);
		int l = num.length();
		if (num.equals("")) {
			Toast.makeText(SettingsActivity.this, "Please enter a valid number", Toast.LENGTH_SHORT).show();
		} else if (l != 10) {
			Log.d("SOS SETTINGS:", "Length = " + l);
			Toast.makeText(SettingsActivity.this, "Please enter a valid number", Toast.LENGTH_SHORT).show();
		} else if (num.matches("[0-9]+")) {
			PrefsUtil.setSOSExtraPhone(this, num);
		} else {
			Toast.makeText(SettingsActivity.this, "Please enter a valid number", Toast.LENGTH_SHORT).show();
		}
	}
}
