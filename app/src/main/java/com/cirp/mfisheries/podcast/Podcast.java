package com.cirp.mfisheries.podcast;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;

public class Podcast extends Module {

	public Podcast(Context context){
		super(context);
	}

	@Override
	protected Module setModuleId() {
		this.moduleId = "Podcast";
		return this;
	}

	@Override
	protected Module setModuleName() {
		this.name = "Podcast";
		return this;
	}

	@Override
	protected Module setIsDisplayed() {
		this.displayed = false;
		return this;
	}

	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_podcast;
		return this;
	}

	@Override
	protected Module setNeedsRegistration() {
		this.needsRegistration = false;
		return this;
	}

	@Override
	protected Module setActivityClass() {
		this.activityClass = PodcastActivity.class;
		return this;
	}
}