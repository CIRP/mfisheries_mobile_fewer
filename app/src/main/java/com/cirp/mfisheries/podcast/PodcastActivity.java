package com.cirp.mfisheries.podcast;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.ModuleActivity;
import com.cirp.mfisheries.core.ModuleInfo;
import com.cirp.mfisheries.util.FileUtil;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.File;

public class PodcastActivity extends ModuleActivity {

	private String[] options;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		module = new Podcast(this);
		loadData();
	}

	//displaying a ListView of options
	public void displayData() {
		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, options);
		ListView listView = (ListView) findViewById(R.id.listView);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Log.d("Podcast", "Attempting to Retrieve filename for: " + options[position]);
				playPodcast(options[position]);
			}
		});
		listView.setAdapter(adapter);
	}

	//loading the data from the Podcast/modules.json file and displaying it
	public void loadData() {
		try {
			String json = FileUtil.readJSONFile(module.getId() + "/module.json");
			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<ModuleInfo> jsonAdapter = moshi.adapter(ModuleInfo.class);

			ModuleInfo info = jsonAdapter.fromJson(json);
			options = info.options;
			displayData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//converting the heading to the specified name format of the audio file
	public void playPodcast(String filename) {
		filename = filename.replaceAll(" ", "_").toLowerCase() + ".mp3";

		File file = new File(Environment.getExternalStorageDirectory().getPath() + FileUtil.FOLDER + module.getId() + "/" + filename);

		Uri uri;
		if (Build.VERSION.SDK_INT >= 24)
			uri = FileProvider.getUriForFile(this, getPackageName() + ".provider", file);
		else
			uri = FileUtil.getAudioUri(module.getId() + "/" + filename);

		playMedia(uri);
	}

	public void playMedia(Uri uri) {
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(uri, "audio/*");
		intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		startActivity(intent);
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_podcast;
	}

	@Override
	public int getColor(){
		return R.color.blue;
	}

	@Override
	public int getColorDark(){
		return R.color.blueDark;
	}
}
