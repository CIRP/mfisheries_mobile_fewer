package com.cirp.mfisheries.core.location;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.cirp.mfisheries.MainActivity;
import com.cirp.mfisheries.core.ModuleActivity;

public abstract class LocationDependentActivity extends ModuleActivity {

	protected boolean LOCATION_ENABLED;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		checkLocation();
	}

	public void checkLocation() {
		LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
		if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			// Build the alert dialog
			new AlertDialog.Builder(this)
					.setTitle("Location Services Not Active")
					.setMessage("Please enable Location Services and GPS to use this module")
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialogInterface, int i) {
								// Show location settings when the user acknowledges the alert dialog
								Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivity(intent);
							}
						})
					.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
							Intent intent = new Intent(LocationDependentActivity.this, MainActivity.class);
							startActivity(intent);
						}
					})
					.setCancelable(false)
					.show();
		}
		LOCATION_ENABLED = true;
	}

	@Override
	public void onResume() {
		super.onResume();
		checkLocation();
	}
}
