package com.cirp.mfisheries.core;

import android.content.Context;

import com.cirp.mfisheries.alerts.Alerts;
import com.cirp.mfisheries.fewer.Fewer;
import com.cirp.mfisheries.firstaid.FirstAid;
import com.cirp.mfisheries.lek.LEK;
import com.cirp.mfisheries.nav.Navigation;
import com.cirp.mfisheries.photos.PhotoDiary;
import com.cirp.mfisheries.podcast.Podcast;
import com.cirp.mfisheries.sos.SOS;
import com.cirp.mfisheries.weather.Weather;

import java.util.Map;

public class ModuleFactory {

	private static ModuleFactory instance;
	private Map<String, String> modules;
	private static Context context;

	private ModuleFactory() {

	}

	public static ModuleFactory getInstance(Context ctx) {
		if (instance == null)
			instance = new ModuleFactory();
		context = ctx;
		return instance;
	}

	public Module getModule(String module) {
		if (module.equals("First Aid")) {
			return new FirstAid(context);
		}
		if (module.equals("Podcast")) {
			return new Podcast(context);
		}
		if (module.equals("Photo Diary")) {
			return new PhotoDiary(context);
		}
		if (module.equals("LEK")) {
			return new LEK(context);
		}
		if (module.equals("Weather")) {
			return new Weather(context);
		}
		if (module.equals("SOS")) {
			return new SOS(context);
		}
		if (module.equals("Navigation")) {
			return new Navigation(context);
		}
		if (module.equals("Alerts")) {
			return new Alerts(context);
		}
		if (module.equals("FEWER")) {
			return new Fewer(context);
		}
		return null;
	}

}
