package com.cirp.mfisheries.core.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cirp.mfisheries.MainActivity;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.alerts.AlertsActivity;
import com.cirp.mfisheries.fewer.FewerActivity;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Date;

/**
 * Created by kyledefreitas on 2/13/17.
 */

public class NotifyCloudMessagingService extends FirebaseMessagingService {

    private final static String TAG = "NotifyCloudService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            String type = remoteMessage.getData().get("type");
            if(type.equals("cap")){
                String alertId = remoteMessage.getData().get("alertId");
                sendAck(alertId);

                Intent intent = new Intent(this, FewerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                String title = remoteMessage.getData().get("title");
                String message = remoteMessage.getData().get("message");
                sendNotification(title, message, R.drawable.ic_warning_white_24dp, intent);
            }
            else if(type.equals("alert")){
                Intent intent = new Intent(this, AlertsActivity.class);
                intent.putExtra("groupName", remoteMessage.getData().get("group"));
                intent.putExtra("groupId", remoteMessage.getData().get("groupid"));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                String title = remoteMessage.getData().get("title");
                String message = remoteMessage.getData().get("message");
                sendNotification(title, message, R.drawable.ic_warning_white_24dp, intent);
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            String title = remoteMessage.getNotification().getTitle();
            String message = remoteMessage.getNotification().getBody();
            Intent intent = new Intent(this, MainActivity.class);
            sendNotification(title, message, R.drawable.mfisheries_icon, intent);
        }
    }

    private void sendAck(String alertId) {
        String userId = String.valueOf(PrefsUtil.getUserId(getApplicationContext()));

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("alerts").child(alertId).child(userId).setValue(true);
    }


    private void sendNotification(String title, String message, @DrawableRes int icon, Intent intent) {
        Log.d(TAG, title + " " + message);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify((int) new Date().getTime(), notificationBuilder.build());
    }
}
