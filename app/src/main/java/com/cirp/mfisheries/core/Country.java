package com.cirp.mfisheries.core;

public class Country {

    private String id;
    private Authority authority;
    private String name;
    private String path;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Authority getAuthority() {
        return authority;
    }

    public void setAuthority(Authority authority) {
        this.authority = authority;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public static class Authority {

        private String email;
        private String mobileNum;
        private String homeNum;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobileNum() {
            return mobileNum;
        }

        public void setMobileNum(String mobileNum) {
            this.mobileNum = mobileNum;
        }

        public String getHomeNum() {
            return homeNum;
        }

        public void setHomeNum(String homeNum) {
            this.homeNum = homeNum;
        }
    }
}
