package com.cirp.mfisheries.core.gcm;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.nav.NavigationActivity;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONArray;
import org.json.JSONObject;

public class 	GCMIntentService extends IntentService {

	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	private String sos_message;

	public GCMIntentService() {
		super("GCMIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		extras.get("message");
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);
		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				sendNotification("Deleted messages on server: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				for (int i = 0; i < 5; i++) {
					Log.i("mFisheries", "Working... " + (i + 1) + "/5 @ " + SystemClock.elapsedRealtime());
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				Log.i("mFisheries", "Completed work @ " + SystemClock.elapsedRealtime());
				saveMessage(extras.get("message").toString());
				sendNotification(sos_message);
				Log.i("mFisheries", "Received: " + extras.toString());
			}
		}
		GCMBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void saveMessage(String message) {
		try {
			JSONObject message_object = new JSONArray(message).getJSONObject(0);
			final GCMHistoryDM gcm_message = new GCMHistoryDM();

			gcm_message.accuracy = message_object.getString("accuracy");
			gcm_message.altitude = message_object.getString("altitude");
			gcm_message.bearing = message_object.getString("bearing");
			gcm_message.latitude = message_object.getString("latitude");
			gcm_message.longitude = message_object.getString("longitude");
			gcm_message.provider = message_object.getString("mockprovider");
			gcm_message.rssi = message_object.getString("rssi");
			gcm_message.speed = message_object.getString("speed");
			gcm_message.timestamp = message_object.getString("deviceTimestamp");
			gcm_message.user = message_object.getString("username");
			gcm_message.message = message_object.getString("message");
			gcm_message.type = message_object.getString("message_type");

			sos_message = "Message " + gcm_message.user + " " + gcm_message.type + " " + gcm_message.message
					+ "\nLatitude: " + convertLatitude(Double.valueOf(gcm_message.latitude))
					+ "\nLongitdue: " + convertLongitude(Double.valueOf(gcm_message.longitude));

			Thread sos_save = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						DatabaseHandler database = new DatabaseHandler(GCMIntentService.this, 3);
						database.open();
						database.gcmHistoryDBHelper.addGCMHistory(gcm_message);
						database.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			});
			sos_save.start();
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Error parseing the SOS message " + ex.getLocalizedMessage());
		}
	}

	private void sendNotification(String msg) {
		mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, NavigationActivity.class), 0);
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher)
				.setContentTitle("GCM Notification").setStyle(new NotificationCompat.BigTextStyle().bigText(msg)).setContentText(msg);
		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}

	public String convertLatitude(double latitude) {
		double abs_latitude = Math.abs(latitude);
		int deg = (int) abs_latitude;
		int min = (int) Math.abs((abs_latitude - deg) * 60);
		float sec = (float) Math.abs((((abs_latitude - deg) * 60) - min) * 60);
		if (latitude < 0) {
			return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.valueOf(sec) + "'' " + "N");
		} else {
			return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.valueOf(sec) + "'' " + "S");
		}
	}

	public String convertLongitude(double longitude) {
		double abs_longitude = Math.abs(longitude);
		int deg = (int) abs_longitude;
		int min = (int) Math.abs((abs_longitude - deg) * 60);
		float sec = (float) Math.abs((((abs_longitude - deg) * 60) - min) * 60);
		if (longitude < 0) {
			return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.valueOf(sec) + "'' " + "W");
		} else {
			return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.valueOf(sec) + "'' " + "E");
		}
	}
}