package com.cirp.mfisheries.core.download;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.core.ModuleFactory;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.NetUtil;

import java.io.File;
import java.util.List;

public class ModuleDownloader {

    private static final String TAG = "ModuleDownloader";
    private OnAllDownloadsFinishedListener mListener;
    private Context mContext;
    public List<String> queue;

    public long lastReferenceId;
    public String lastModuleId;

    public ModuleDownloader() {
    }

    public ModuleDownloader(Context mContext, OnAllDownloadsFinishedListener mListener) {
        this.mContext = mContext;
        this.mListener = mListener;
    }

    //downloads the given list of modules
    public void getModules(Context context, List<String> modules) {
        if (modules.isEmpty()) {
            if (mListener != null) mListener.onFinished();
            return;
        }
        queue = modules;
        mContext = context;
        DownloadService.setDownloader(this);
        getNextModule();
    }

    //starts the download service if given module has not already been downloaded or is set up incorrectly
    private void getModule(String moduleName) {
        Log.d("DownloadManager", "Attempting to get module: " + moduleName);
        ModuleFactory factory = ModuleFactory.getInstance(mContext);
        Module module = factory.getModule(moduleName);
        if (module == null) {
            Log.d("DownloadManager", moduleName + " does not exist in factory");
            getNextModule();
            return;
        }
        if (module.getDataLoc() == null || module.getDataLoc().equals("")) {
            Log.d("DownloadManager", moduleName + " has no downloadable data");
            module.onInstalled();
            getNextModule();
            return;
        }

        String filePath = FileUtil.getFileUri(module.getId()).toString();
        File file = new File(filePath);
        // File already exists for the specific module
        if (file.exists() || FileUtil.directoryExists(filePath)) {
            Log.d("DownloadManager", moduleName + " data already downloaded");
            module.onInstalled();
            getNextModule();
            return;
        }

        if (NetUtil.isOnline(mContext)) {
            // Start service to perform download in the background
            Intent intent = new Intent(mContext, DownloadService.class);
            intent.putExtra("moduleId", module.getName());
            mContext.startService(intent);
        } else {
            Log.d("Download Manager", "Notify the User that No Internet Connection");
            if (mListener != null)
                mListener.onNotification("Unable to Connect to Internet", OnAllDownloadsFinishedListener.MSG_TYPE_NOTIFY);
        }
    }

    //downloads the next module in the queue
    public void getNextModule() {
        if (queue == null || queue.isEmpty()) {
            Log.d("DownloadManager", "All modules downloaded");
            if (mListener != null) mListener.onFinished();
            return;
        }
        getModule(queue.remove(queue.size() - 1));
    }

    //defines functionality to call for specific outcomes
    public interface OnAllDownloadsFinishedListener {

        String MSG_TYPE_NOTIFY = "NOTIFY";
        String MSG_TYPE_ERR = "ERROR";

        void onFinished();

        void onNotification(String message, String type);

    }
}
