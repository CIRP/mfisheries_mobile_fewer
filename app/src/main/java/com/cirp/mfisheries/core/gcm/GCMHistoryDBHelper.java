package com.cirp.mfisheries.core.gcm;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.cirp.mfisheries.core.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

public class GCMHistoryDBHelper {
	private static final String GCM_HISTORY_TABLE = "SOSHistory";
	private static final String GCM_HISTORY_ID = "id";
	private static final String GCM_HISTORY_LATITUDE = "latitude";
	private static final String GCM_HISTORY_LONGITUDE = "longitude";
	private static final String GCM_HISTORY_ACCURACY = "accuracy";
	private static final String GCM_HISTORY_ALTITUDE = "altitude";
	private static final String GCM_HISTORY_BEARING = "bearing";
	private static final String GCM_HISTORY_SPEED = "speed";
	private static final String GCM_HISTORY_PROVIDER = "provider";
	private static final String GCM_HISTORY_RSSI = "rssi";
	private static final String GCM_HISTORY_TIMESTAMP = "timestamp";
	private static final String GCM_HISTORY_USER = "username";
	private static final String GCM_HISTORY_MESSAGE = "message";
	private static final String GCM_HISTORY_TYPE = "type";

	public static String CREATE_SOS_HISTORY_TABLE =
			"CREATE TABLE " + GCM_HISTORY_TABLE + " ("
					+ GCM_HISTORY_ID + " INTEGER PRIMARY KEY autoincrement,"
					+ GCM_HISTORY_LATITUDE + " TEXT,"
					+ GCM_HISTORY_LONGITUDE + " TEXT,"
					+ GCM_HISTORY_ACCURACY + " TEXT,"
					+ GCM_HISTORY_ALTITUDE + " TEXT,"
					+ GCM_HISTORY_BEARING + " TEXT,"
					+ GCM_HISTORY_SPEED + " TEXT,"
					+ GCM_HISTORY_PROVIDER + " TEXT,"
					+ GCM_HISTORY_RSSI + " TEXT,"
					+ GCM_HISTORY_TIMESTAMP + " TEXT,"
					+ GCM_HISTORY_USER + " TEXT,"
					+ GCM_HISTORY_MESSAGE + " TEXT,"
					+ GCM_HISTORY_TYPE + " TEXT" + ")";
	
	public float addGCMHistory(GCMHistoryDM track_point) {
		try {
			ContentValues track_values = new ContentValues();
			track_values.put(GCM_HISTORY_LATITUDE, track_point.latitude);
			track_values.put(GCM_HISTORY_LONGITUDE, track_point.longitude);
			track_values.put(GCM_HISTORY_ACCURACY, track_point.accuracy);
			track_values.put(GCM_HISTORY_ALTITUDE, track_point.altitude);
			track_values.put(GCM_HISTORY_BEARING, track_point.bearing);
			track_values.put(GCM_HISTORY_SPEED, track_point.speed);
			track_values.put(GCM_HISTORY_PROVIDER, track_point.provider);
			track_values.put(GCM_HISTORY_RSSI, track_point.rssi);
			track_values.put(GCM_HISTORY_TIMESTAMP, track_point.timestamp);
			track_values.put(GCM_HISTORY_USER, track_point.user);
			track_values.put(GCM_HISTORY_MESSAGE, track_point.message);
			track_values.put(GCM_HISTORY_TYPE, track_point.type);
			return DatabaseHandler.database.insert(GCM_HISTORY_TABLE, null, track_values);
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Offline Track DB Helper, Add Offline Track Point, Error adding point" + ex.getLocalizedMessage());
		}
		return 0;
	}
	
	public int getGCMCount() {
		try {
			String offline_tracks_query = "SELECT * FROM " + GCM_HISTORY_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(offline_tracks_query, null);
			return cursor.getCount();
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Offline Track DB Helper, Get Offline Track Count, Error " + ex.getLocalizedMessage());
		}
		return 0;
	}
	
	public List<GCMHistoryDM> getAllGCMHistory() {
		List<GCMHistoryDM> track_points = new ArrayList<>();
		try {
			String selectQuery = "SELECT  * FROM " + GCM_HISTORY_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					GCMHistoryDM track_point = new GCMHistoryDM();
					track_point.id = Integer.parseInt(cursor.getString(0));
					track_point.latitude = cursor.getString(1);
					track_point.longitude = cursor.getString(2);
					track_point.accuracy = cursor.getString(3);
					track_point.altitude = cursor.getString(4);
					track_point.bearing = cursor.getString(5);
					track_point.speed = cursor.getString(6);
					track_point.provider = cursor.getString(7);
					track_point.rssi = cursor.getString(8);
					track_point.timestamp = cursor.getString(9);
					track_point.user = cursor.getString(10);
					track_point.message = cursor.getString(11);
					track_point.type = cursor.getString(12);
					track_points.add(track_point);
				}
				while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Offline Track DB Helper, Get Offline Tracks, Error " + ex.getLocalizedMessage());
		}
		return track_points;
	}
	
	public void deleteMarker(String marker_id) {
		if (!marker_id.equals("0")) {
			try {
				DatabaseHandler.database.delete(GCM_HISTORY_TABLE, GCM_HISTORY_ID + " = ?", new String[]{marker_id});
			} catch (Exception ex) {
				Log.wtf("mFisheries", "Map Marker db Helper, Error deleting marker " + ex.getLocalizedMessage());
			}
		}
	}
}
