package com.cirp.mfisheries.core.location;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class LocationService extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

	private static final String TAG = "LocationService";

	public final static long LOC_TIME_INTERVAL = 60000;
	public final static long LOC_DIST_INTERVAL = 100;

	public static boolean trackingEnabled, offlineTracksPresent;
	public static String signalStrengthInfo;
	public static Location userLocation;

	private int locationCounter, timeMultiplier = 1;
	private String trackType, trackingStatus;
	private NotificationCompat.Builder serviceNotification;
	private SharedPreferences.Editor editor;

	protected boolean mRequestingLocationUpdates = true;
	protected GoogleApiClient mGoogleApiClient;
	protected LocationRequest mLocationRequest;

	@Override
	public void onCreate() {
		super.onCreate();
		locationCounter = 0;
		Log.d(TAG, "Created Location Service");
		Toast.makeText(this, "Started mFisheries Location Service", Toast.LENGTH_LONG).show();

		editor = PreferenceManager.getDefaultSharedPreferences(LocationService.this).edit();

		try {
			getLastLocationState();
			initializePhoneStateListener();
			buildGoogleApiClient();
			displayNotification();
		} catch (Exception ex) {
			Log.wtf(TAG, "Error " + ex.getLocalizedMessage());
		}
	}

	protected void stopLocationUpdates() {
		if(mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
			LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
		}
	}

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addApi(LocationServices.API)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.build();
		mGoogleApiClient.connect();
	}

	public void onConnectionFailed(@NonNull ConnectionResult result) {
		Log.d(TAG, result.getErrorCode() + "");
	}

	public void onConnectionSuspended(int cause) {
		//   mGoogleApiClient.connect();
	}

	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(LOC_TIME_INTERVAL);
		mLocationRequest.setFastestInterval(LOC_TIME_INTERVAL);
		mLocationRequest.setSmallestDisplacement(LOC_DIST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	}

	public void onConnected(Bundle connectionHint) {
		Log.d(TAG, "Connected!");
		createLocationRequest();
		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
				.addLocationRequest(mLocationRequest);
		PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
		result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
			@Override
			public void onResult(@NonNull LocationSettingsResult result) {
				final Status status = result.getStatus();
				switch (status.getStatusCode()) {
					case LocationSettingsStatusCodes.SUCCESS:
						try {
							userLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
							if (mRequestingLocationUpdates) {
								startLocationUpdates();
							}
						} catch (SecurityException e) {
							e.printStackTrace();
						}
						break;
					case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
						break;
				}
			}
		});
	}

	protected void startLocationUpdates() {
		try {
			LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
		Toast.makeText(this, "Stopped mFisheries Location Service", Toast.LENGTH_LONG).show();
		Log.d(TAG, "Destroying Service");
		NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notifManager.cancel("LOCATION_SERVICE", 1);
		stopLocationUpdates();

		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Added the start sticky command to force the system to restart the service if terminated - https://developer.android.com/reference/android/app/Service.html
		return Service.START_STICKY;
	}

	private void getLastLocationState() {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(LocationService.this);
		trackingEnabled = preferences.getBoolean("is_tracking_enabled", false);
		offlineTracksPresent = preferences.getBoolean("are_offline_tracks_present", false);
		trackType = preferences.getString("track_type", "start");
		trackingStatus = preferences.getString("tracking_status", "Tracking Disabled");
	}

	private void initializePhoneStateListener() {
		signalStrengthInfo = "No Data";
		PhoneStatusListener phoneStatusListener = new PhoneStatusListener();
		TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		telephonyManager.listen(phoneStatusListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
	}

	private class PhoneStatusListener extends PhoneStateListener {
		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			super.onSignalStrengthsChanged(signalStrength);
			signalStrengthInfo = signalStrength.toString();
		}
	}

	private void displayNotification() {
		Log.i(TAG, "Displaying Notification");
		if (trackingStatus.equals("Tracking Disabled"))
			return;
		Intent stopServiceIntent = new Intent(this, StopLocationServiceActivity.class);
		PendingIntent pending_intent = PendingIntent.getActivity(this, 0, stopServiceIntent, 0);
		serviceNotification = new NotificationCompat.Builder(this);
		serviceNotification.setSmallIcon(R.drawable.map_marker_radius);
		serviceNotification.setContentTitle("mFisheries");
		serviceNotification.setContentText(trackingStatus);
		serviceNotification.addAction(R.drawable.ic_stop_white_24dp, "Stop GPS Polling", pending_intent);
		serviceNotification.setOngoing(true);
		serviceNotification.setPriority(10);
		NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notifManager.notify("LOCATION_SERVICE", 1, serviceNotification.build());
	}

	private void updateNotification() {
		String message;
		NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		serviceNotification = new NotificationCompat.Builder(this);
		serviceNotification.setSmallIcon(R.drawable.ic_map_marker_radius_white_24dp);
		serviceNotification.setContentTitle("mFisheries");
		if (trackingEnabled) {
			message = "Tracking Enabled";
			Log.d(TAG, message);
			Intent stopServiceIntent = new Intent(this, StopLocationServiceActivity.class);
			PendingIntent pending_intent = PendingIntent.getActivity(this, 0, stopServiceIntent, 0);
			serviceNotification.addAction(R.drawable.ic_stop_white_24dp, "Stop GPS Polling", pending_intent);
			serviceNotification.setOngoing(true);
			serviceNotification.setPriority(10);
			serviceNotification.setContentText(message);
			notifManager.notify("LOCATION_SERVICE", 1, serviceNotification.build());
		} else {
			message = "Tracking Disabled";
			Log.d(TAG, message);
			serviceNotification.setContentText(message);
			notifManager.notify("LOCATION_SERVICE", 1, serviceNotification.build());
		}
		editor.putString("tracking_status", message);
		editor.commit();
	}

	@Override
	public void onLocationChanged(Location loc) {
		userLocation = loc;
		Log.d(TAG, "Location: " + loc.getLatitude() + " " + loc.getLongitude());
		editor.putFloat("lat", (float) loc.getLatitude());
		editor.putFloat("lng", (float) loc.getLongitude());
		editor.apply();
		new LocationTask().execute(loc);
	}

	public class LocationTask extends AsyncTask<Location, Void, Void> {

		private Location location;

		protected Void doInBackground(Location... locations) {
			location = locations[0];
			if (locationCounter == 0) {
				Boolean withinCountry = Geofencer.isWithinCountry(LocationService.this, location.getLatitude(), location.getLongitude());
				Log.d(TAG, "Is Within Country = " + withinCountry);
				if (withinCountry != null) {
					if (withinCountry && trackingEnabled) { // Within country so we should disable tracking
						Log.d(TAG, "Tracking ended");
						trackingEnabled = false;
						trackType = "end";
					} else if (!withinCountry && !trackingEnabled) { // Out at sea and tracking is enabled then start tracking
						Log.d(TAG, "Tracking started");
						trackingEnabled = true;
						trackType = "start";
					}
				} else {
					Log.d(TAG, "Error occurred with tracking attempting to end tracking");
					trackingEnabled = false;
					trackType = "end";
				}
				editor.putBoolean("is_tracking_enabled", trackingEnabled);
				editor.putString("track_type", trackType);
				editor.commit();
				locationCounter = timeMultiplier;
			} else {
				locationCounter = locationCounter - 1;
			}
			updateNotification();
			Log.d(TAG, "Offline Tracks = " + offlineTracksPresent);
			if (trackingEnabled || offlineTracksPresent) {
				if (NetUtil.isOnline(getApplicationContext()))
					uploadTracks(location, trackType);
				else if (trackingEnabled)
					saveTrack(location, trackType);
				if (trackType.equals("start"))
					trackType = "track";
				editor.putString("track_type", trackType);
				editor.putBoolean("are_offline_tracks_present", offlineTracksPresent);
				editor.commit();
			}
			return null;
		}
	}

	public void uploadTracks(final Location location, final String trackType) {
		List<TrackPointDM> points = new ArrayList<>();

		if (offlineTracksPresent) {
			try {
				Log.d(TAG, "Offline tracks present");
				DatabaseHandler database = new DatabaseHandler(this, DatabaseHandler.OFFLINE_TRACK_DB_HELPER);
				database.open();
				points = database.offlineTrackDBHelper.getAllOfflineTracks();
				database.close();
			} catch (Exception ex) {
				Log.wtf(TAG, "Error reading offline tracks " + ex.getLocalizedMessage());
			}
		}
		if (points == null)
			points = new ArrayList<>();

		if (location != null) {
			TrackPointDM trackPointDM = createTrackPoint(location, PrefsUtil.getUserId(this));
			Log.d(TAG, trackPointDM.toString());
			points.add(trackPointDM);
		}

		FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				if (e == null && result != null) {
					Log.d(TAG, "track sent " + result.toString());

					DatabaseHandler database = new DatabaseHandler(LocationService.this, 2);
					database.open();
					database.offlineTrackDBHelper.deleteAllTracks();
					database.close();
					offlineTracksPresent = false;
				} else if (e != null){
					Log.d(TAG, "track not sent " + e.getMessage());
					saveTrack(location, trackType);
				}
			}
		};

		if (points.size() > 0)
			sendTracks(points, callback);
	}

	public void sendTracks(List<TrackPointDM> points, FutureCallback<JsonObject> callback) {
		JsonArray jsonArray = getJsonElements(points);

		Ion.with(getApplicationContext())
            .load(NetUtil.API_URL + NetUtil.POST_TRACK_URL)
			.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
            .setJsonArrayBody(jsonArray)
            .asJsonObject()
            .setCallback(callback);
	}

	public JsonArray getJsonElements(List<TrackPointDM> points) {
		Moshi moshi = new Moshi.Builder().build();
		JsonAdapter<List> jsonAdapter = moshi.adapter(List.class);

		String json = jsonAdapter.toJson(points);

		JsonParser parser = new JsonParser();
		return (JsonArray) parser.parse(json);
	}

	public boolean saveTrack(Location location, String trackType) {
		if (location == null)
			return false;
		try {
			if (trackType != null) this.trackType = trackType;
			TrackPointDM trackPoint = createTrackPoint(location, PrefsUtil.getUserId(this));
			DatabaseHandler database = new DatabaseHandler(LocationService.this, DatabaseHandler.OFFLINE_TRACK_DB_HELPER);
			database.open();
			database.offlineTrackDBHelper.addOfflineTrack(trackPoint);
			database.close();
			offlineTracksPresent = true;
			Log.d(TAG, "Saved Track: " + trackPoint.toString());
			return true;
		} catch (Exception ex) {
			Log.wtf(TAG, "Error saving track point " + ex.getLocalizedMessage());
			return false;
		}
	}

	public TrackPointDM createTrackPoint(@NonNull Location location, @NonNull int userId) {
		TrackPointDM trackPointDM = new TrackPointDM();
		trackPointDM.acc = String.valueOf(location.getAccuracy());
		trackPointDM.brg = String.valueOf(location.getBearing());
		trackPointDM.lat = String.valueOf(location.getLatitude());
		trackPointDM.lng = String.valueOf(location.getLongitude());
		trackPointDM.spd = String.valueOf(location.getSpeed());
		Log.d(TAG, "Location Provider was given as: " + location.getProvider());
		if (location.getProvider() == null) {
			trackPointDM.prov = "-1";
		} else if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
			trackPointDM.prov = "1";
		} else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
			trackPointDM.prov = "2";
		}else{
			trackPointDM.prov = "-1";
		}

		trackPointDM.rssi = signalStrengthInfo;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d hh:mm:ss", Locale.ENGLISH);
		trackPointDM.time = format.format(new Date());
		trackPointDM.type = trackType;
		trackPointDM.uId = userId;
		if (trackPointDM.uId == -999)
			Log.d(TAG, "Unable to Extract User id from the system");

		Log.d(TAG, trackPointDM.toString() + "");
		return trackPointDM;
	}


	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}
