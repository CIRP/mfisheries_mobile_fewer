package com.cirp.mfisheries.core;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.ModuleUtil;

import java.util.List;

public class ModuleAdapter extends ArrayAdapter<String> {
	private final Context mContext;
	private final List<String> modules;
	private View.OnClickListener placeholderListener;

	static class ViewHolder {
		public ImageView imageView;
		public TextView moduleName;
	}

	public ModuleAdapter(Context context, List<String> modules) {
		super(context, R.layout.layout_module, modules);
		this.mContext = context;
		this.modules = modules;
	}

	/*
		NB: The getView method handles the displaying of the data at the position passed in as a parameter
		More info: http://developer.android.com/reference/android/widget/ArrayAdapter.html#getView(int, android.view.View, android.view.ViewGroup)

	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;

		if (rowView == null) { //creates a viewholder object that holds references to views so the findViewById method is not called repeatedly
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.layout_module, parent, false);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.imageView = (ImageView) rowView.findViewById(R.id.moduleImage);
			viewHolder.moduleName = (TextView) rowView.findViewById(R.id.moduleName);
			//binds viewholder object to rowView
			rowView.setTag(viewHolder);
		}

		//gets viewholder from object
		ViewHolder holder = (ViewHolder) rowView.getTag();

		final String moduleName = modules.get(position);
		if (moduleName.equals(ModuleUtil.PLACEHOLDER_TILE))
			return createPlaceholder(holder, rowView);
		holder.moduleName.setText(moduleName);

		ModuleFactory moduleFactory = ModuleFactory.getInstance(mContext);
		final Module module = moduleFactory.getModule(moduleName);

		if (module != null) { // What should the system do if a module is not available
			if (module.getImageResource() != 0)
				holder.imageView.setImageResource(module.getImageResource());
			holder.imageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					module.display();
				}
			});
		} else {
			Log.wtf(ModuleAdapter.class.getSimpleName(), "No Module Created/Defined for: " + moduleName);
		}

		return rowView;
	}

	//creates a placeholder tile if the number of selected modules is odd
	public View createPlaceholder(ViewHolder holder, View view) {
		Log.d("Placeholder", "Creating placeholder");
		holder.moduleName.setText(mContext.getResources().getString(R.string.action_add));
		holder.imageView.setImageResource(R.drawable.icon_placeholder);
		holder.imageView.setOnClickListener(placeholderListener);
		view.setId(R.id.placeholder);
		return view;

	}

	public void setPlaceholderListener(View.OnClickListener placeholderListener) {
		Log.d("Placeholder", "Setting placeholder");
		this.placeholderListener = placeholderListener;
	}

	@Override
	public void add(String object) {
		this.notifyDataSetChanged();
	}
}
