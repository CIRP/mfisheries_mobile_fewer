package com.cirp.mfisheries.core;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public abstract class Module {

	private static final String TAG = "Module-Base";

	protected String moduleId;
	protected String dataUrl;
	protected boolean displayed = false;
	protected String name;
	protected int imageResource;
	protected Context context;
	public boolean needsRegistration;
	protected Class <?> activityClass;

	public Module() {
		initialize();
	}

	public Module(Context context) {
		this.context = context;
		initialize();
	}

	public Module sync() {
		return this;
	}

	public Module initialize(){
		setModuleId();
		setModuleName();
		setIsDisplayed();
		setImageResource();
		setNeedsRegistration();
		setActivityClass();
		return this;
	}
	protected abstract Module setModuleId();
	protected abstract Module setModuleName();
	protected abstract Module setIsDisplayed();
	protected abstract Module setImageResource();
	protected abstract Module setNeedsRegistration();
	protected abstract Module setActivityClass();


//	public Module initialize() {
//		return this;
//	}

	public Module loadData() {
		return this;
	}

	public void initDatabase() {
	}

	public String getId() {
		return moduleId;
	}

	public String getDataLoc() {
		return dataUrl;
	}

	public boolean hasDownload(){
		return dataUrl != null;
	}

	public boolean isDisplayed() {
		return displayed;
	}

	public String getName() {
		return name;
	}

	public int getImageResource() {
		return imageResource;
	}

	protected Module setId(String module_id) {
		this.moduleId = module_id;
		return this;
	}

	public Module setDataUrl(String data_url) {
		this.dataUrl = data_url;
		return this;
	}

	public Module setDisplayed(boolean displayed) {
		this.displayed = displayed;
		return this;
	}

	public Module setName(String name) {
		this.name = name;
		return this;
	}

	public Module setImageResource(int image_resource) {
		this.imageResource = image_resource;
		return this;
	}

	// Abstract Methods
	public void onInstalled() {
		Log.d(TAG, "Installed: " + name);
	}

	public Module display() {
		Log.i(TAG, "Displayed " + moduleId);
		Intent intent = new Intent(context, activityClass);
		context.startActivity(intent);
		return this;
	}
}
