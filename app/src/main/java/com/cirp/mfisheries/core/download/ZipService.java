package com.cirp.mfisheries.core.download;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.Zipper;

public class ZipService extends IntentService {

	private final static String TAG = "ZipService";

	public ZipService() {
		super("ZipService");
	}

	@Override
	public void onHandleIntent(final Intent intent) {
		final String moduleId = intent.getStringExtra("module_id");
		final int id = intent.getIntExtra("id", -1);

		Log.d(TAG, "Unzipping " + id);

		final NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		final NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
				.setContentTitle(getString(R.string.installing) + " " + moduleId + " " + getString(R.string.module))
				.setSmallIcon(R.drawable.ic_download_white_24dp);

		builder.setProgress(100, 0, true);
		nm.notify(id, builder.build());

		Zipper zipper = new Zipper(FileUtil.getFileUri("/" + moduleId + ".zip").toString(), FileUtil.getFileUri("").toString());

		zipper.setDeleteAfterUnzip(true);
		zipper.setOnZipFinishedListener(new Zipper.OnZipFinishedListener() {
			@Override
			public void onFinished() {
				Log.d(TAG, "Finished unzipping " + moduleId);
				//stores that module downloaded successfully and installed
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				prefs.edit().putBoolean(moduleId, true).apply();
				nm.cancel(id);
				ModuleDownloader downloader = DownloadService.downloader;
				downloader.getNextModule();
				stopSelf();
			}
		});

		zipper.unzip();
	}
}