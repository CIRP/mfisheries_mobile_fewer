package com.cirp.mfisheries.core.gcm;

public class GCMHistoryDM {
	public int id;
	public String type;
	public String latitude;
	public String longitude;
	public String accuracy;
	public String altitude;
	public String bearing;
	public String speed;
	public String provider;
	public String rssi;
	public String timestamp;
	public String user;
	public String message;
}
