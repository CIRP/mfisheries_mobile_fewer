package com.cirp.mfisheries.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.core.location.TrackPointDM;
import com.cirp.mfisheries.lek.LEK;
import com.cirp.mfisheries.lek.LEKActivity;
import com.cirp.mfisheries.lek.LEKPost;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class NetworkChangeReceiver extends BroadcastReceiver {

	private static final String LOG_TAG = "NetworkChangeReceiver";
	private boolean isConnected = false;
	private Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.v(LOG_TAG, "Received notification about network status");
		this.context = context;
		if (isNetworkAvailable(context)) {
			//do task now that network is available
			new SyncTask().execute();
		}
	}

	private class SyncTask extends AsyncTask<Void, Void, Boolean> {

		protected Boolean doInBackground(Void... params) {
			try {
				sendCachedData();
			} catch (Exception e) {
				Log.d("LEK", "LEK DB LOADING: " + e.getMessage());
			}
			return false;
		}

		protected void onPostExecute(Boolean finished) {

		}
	}

	private void sendCachedData() {
		Log.d(LOG_TAG, "Sending cached data");
		if (LocationService.offlineTracksPresent) {
			Log.d(LOG_TAG, "Sending cached tracks");
			sendCachedTracks();
		}
		if(PrefsUtil.hasLekToSync(context)){
			Log.d(LOG_TAG, "Sending cached LEK");
			sendCachedLEK();
		}
	}

	private void sendCachedTracks() {
		List<TrackPointDM> points;

		try {
            DatabaseHandler database = new DatabaseHandler(context, 2);
            database.open();
            points = database.offlineTrackDBHelper.getAllOfflineTracks();
            database.close();

            if (points == null)
                points = new ArrayList<>();

            if (points.size() > 0) {
                Moshi moshi = new Moshi.Builder().build();
                JsonAdapter<List> jsonAdapter = moshi.adapter(List.class);

                String json = jsonAdapter.toJson(points);

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = (JsonArray) parser.parse(json);

                Ion.with(context)
                        .load(NetUtil.API_URL + NetUtil.POST_TRACK_URL)
                        .setJsonArrayBody(jsonArray)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (e == null && result != null) {
                                    Log.d("location", "track sent " + result.toString());
                                    DatabaseHandler database = new DatabaseHandler(context, 2);
                                    database.open();
                                    database.offlineTrackDBHelper.deleteAllTracks();
                                    database.close();
                                    LocationService.offlineTracksPresent = false;
                                }
                            }
                        });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	private void sendCachedLEK() {
		try {
			final DatabaseHandler database = new DatabaseHandler(context, DatabaseHandler.LEK_DB_HELPER);
			database.open();
			List<LEKPost> lekPosts = database.offlineLEKDBHelper.getLEKToSync();
			Log.d("NetworkReceiver", "Posts to sync = " + lekPosts.size());
			database.close();

			for(int i = 0; i < lekPosts.size(); i++) {
				final int id = lekPosts.get(i).id;
				Log.d("LEK", lekPosts.get(i).toString());
				uploadLEK(lekPosts.get(i), new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						if (e == null && result != null) {
							Log.d(LOG_TAG, "LEK uploaded");
							database.open();
							database.offlineLEKDBHelper.updateLEKStatus(id);
							database.close();
						}
						else if(e != null){
							Log.d(LOG_TAG, "LEK not uploaded " + e.getMessage());
						}
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void uploadLEK(LEKPost post, FutureCallback<JsonObject> callback){
		if (post.media != null) {
			File file = new File(post.media);
			Ion.with(context)
					.load(NetUtil.API_URL + NetUtil.ADD_LEK_PATH)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.setMultipartFile("file", post.filetype, file)
					.setMultipartParameter("latitude", post.latitude)
					.setMultipartParameter("longitude", post.longitude)
					.setMultipartParameter("text", post.text)
					.setMultipartParameter("filetype", post.filetype)
					.setMultipartParameter("isSpecific", "true")
					.setMultipartParameter("userid", post.userid)
					.setMultipartParameter("aDate", post.applicableDate)
					.setMultipartParameter("category", post.category)
					.setMultipartParameter("countryid", PrefsUtil.getCountryId(context))
					.asJsonObject()
					.setCallback(callback);
		} else {//if just text

			Ion.with(context)
					.load(NetUtil.API_URL + NetUtil.ADD_LEK_PATH)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.setMultipartParameter("latitude", post.latitude)
					.setMultipartParameter("longitude", post.longitude)
					.setMultipartParameter("text", post.text)
					.setMultipartParameter("filetype", "text/*")
					.setMultipartParameter("isSpecific", "true")
					.setMultipartParameter("userid", post.userid)
					.setMultipartParameter("aDate", post.applicableDate)
					.setMultipartParameter("category", post.category)
					.setMultipartParameter("countryid", PrefsUtil.getCountryId(context))
					.asJsonObject()
					.setCallback(callback);
		}
	}

	private boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo info = connectivity.getActiveNetworkInfo();
			if (info != null) {
				if (info.getState() == NetworkInfo.State.CONNECTED) {
					if (!isConnected) {
						isConnected = true;
						Log.v(LOG_TAG, "You are connected to Internet!");
					}
					return true;
				}
			}
		}
		Log.v(LOG_TAG, "You are not connected to Internet!");
		isConnected = false;
		return false;
	}


}
