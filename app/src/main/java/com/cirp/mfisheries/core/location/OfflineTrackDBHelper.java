package com.cirp.mfisheries.core.location;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.cirp.mfisheries.core.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

public class OfflineTrackDBHelper {

	private static final String OFFLINE_TRACKS_TABLE = "OfflineTracks";
	private static final String OFFLINE_TRACK_ID = "id";
	private static final String OFFLINE_TRACK_LATITUDE = "lat";
	private static final String OFFLINE_TRACK_LONGITUDE = "lng";
	private static final String OFFLINE_TRACK_ACCURACY = "acc";
	private static final String OFFLINE_TRACK_BEARING = "brg";
	private static final String OFFLINE_TRACK_SPEED = "spd";
	private static final String OFFLINE_TRACK_PROVIDER = "prov";
	private static final String OFFLINE_TRACK_RSSI = "rssi";
	private static final String OFFLINE_TRACK_TYPE = "type";
	private static final String OFFLINE_TRACK_TIMESTAMP = "time";
	private static final String OFFLINE_TRACK_USER_ID = "uId";
	private static final String OFFLINE_TRACK_USER = "username";
	private static final String OFFLINE_START_TRACK_ID = "startId";

	public static String CREATE_OFFLINE_TRACK_TABLE =
			"CREATE TABLE " + OFFLINE_TRACKS_TABLE + " ("
					+ OFFLINE_TRACK_ID + " INTEGER PRIMARY KEY autoincrement,"
					+ OFFLINE_TRACK_LATITUDE + " TEXT,"
					+ OFFLINE_TRACK_LONGITUDE + " TEXT,"
					+ OFFLINE_TRACK_ACCURACY + " TEXT,"
					+ OFFLINE_TRACK_BEARING + " TEXT,"
					+ OFFLINE_TRACK_SPEED + " TEXT,"
					+ OFFLINE_TRACK_PROVIDER + " TEXT,"
					+ OFFLINE_TRACK_RSSI + " TEXT,"
					+ OFFLINE_TRACK_TYPE + " TEXT,"
					+ OFFLINE_TRACK_TIMESTAMP + " TEXT,"
					+ OFFLINE_TRACK_USER_ID + " TEXT,"
					+ OFFLINE_TRACK_USER + " TEXT,"
					+ OFFLINE_START_TRACK_ID + " TEXT" + ")";
	
	public float addOfflineTrack(TrackPointDM track_point) {
		try {
			ContentValues track_values = new ContentValues();
			track_values.put(OFFLINE_TRACK_LATITUDE, track_point.lat);
			track_values.put(OFFLINE_TRACK_LONGITUDE, track_point.lng);
			track_values.put(OFFLINE_TRACK_ACCURACY, track_point.acc);
			track_values.put(OFFLINE_TRACK_BEARING, track_point.brg);
			track_values.put(OFFLINE_TRACK_SPEED, track_point.spd);
			track_values.put(OFFLINE_TRACK_PROVIDER, track_point.prov);
			track_values.put(OFFLINE_TRACK_RSSI, track_point.rssi);
			track_values.put(OFFLINE_TRACK_TYPE, track_point.type);
			track_values.put(OFFLINE_TRACK_TIMESTAMP, track_point.time);
			track_values.put(OFFLINE_TRACK_USER_ID, track_point.uId);
			track_values.put(OFFLINE_TRACK_USER, track_point.user);
			track_values.put(OFFLINE_START_TRACK_ID, track_point.startId);
			return DatabaseHandler.database.insert(OFFLINE_TRACKS_TABLE, null, track_values);
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Offline Track DB Helper, Add Offline Track Point, Error adding point" + ex.getLocalizedMessage());
		}
		return 0;
	}
	
	public int getOfflineTracksCount() {
		try {
			String offline_tracks_query = "SELECT * FROM " + OFFLINE_TRACKS_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(offline_tracks_query, null);
			return cursor.getCount();
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Offline Track DB Helper, Get Offline Track Count, Error " + ex.getLocalizedMessage());
		}
		return -1;
	}
	
	public List<TrackPointDM> getAllOfflineTracks() {
		try {
			List<TrackPointDM> track_points = new ArrayList<>();
			String selectQuery = "SELECT  * FROM " + OFFLINE_TRACKS_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					TrackPointDM track_point = new TrackPointDM();
					track_point.id = Integer.parseInt(cursor.getString(0));
					track_point.lat = cursor.getString(1);
					track_point.lng = cursor.getString(2);
					track_point.acc = cursor.getString(3);
					track_point.brg = cursor.getString(5);
					track_point.spd = cursor.getString(6);
					track_point.prov = cursor.getString(7);
					track_point.rssi = cursor.getString(8);
					track_point.type = cursor.getString(9);
					track_point.time = cursor.getString(10);
					track_point.uId = Integer.parseInt(cursor.getString(11));
					track_point.user = cursor.getString(12);
					track_point.startId = Integer.parseInt(cursor.getString(13));
					track_points.add(track_point);
				}
				while (cursor.moveToNext());
			}
			cursor.close();
			return track_points;
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Offline Track DB Helper, Get Offline Tracks, Error " + ex.getLocalizedMessage());
		}
		return null;
	}
	
	public void deleteAllTracks() {
		try {
			DatabaseHandler.database.delete(OFFLINE_TRACKS_TABLE, null, null);
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Offline Track DB Helper, Delete all Track, Error " + ex.getLocalizedMessage());
		}
	}
}
