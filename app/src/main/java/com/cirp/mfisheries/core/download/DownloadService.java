package com.cirp.mfisheries.core.download;

import android.app.DownloadManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.core.ModuleFactory;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.NetUtil;

public class DownloadService extends IntentService {

	public static final String DOWNLOADS_COMPLETE_BROADCAST = "downloads-complete-broadcast";
	private final static String TAG = "DownloadService";

	public static ModuleDownloader downloader = new ModuleDownloader();
	private long lastReferenceId;
	private String lastModuleId;

	public DownloadService() {
		super("Download Service");
	}

	@Override
	public void onHandleIntent(Intent intent) {
		Log.d(TAG, "Download Service started");

		String moduleId = intent.getStringExtra("moduleId");
		downloadModule(getApplicationContext(), moduleId);
	}

	public void downloadModule(Context context, String moduleId){
		ModuleFactory factory = ModuleFactory.getInstance(context);
		Module module = factory.getModule(moduleId);
		downloadModule(context, module);
	}

	private void downloadModule(Context context, Module module) {
		Log.d(TAG, "Downloading " + module.getName() + " Module");
		// Create request for android download manager
		DownloadManager downloadManager = (android.app.DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
		DownloadManager.Request request = new android.app.DownloadManager.Request(Uri.parse(module.getDataLoc()));

		//Setting title of request
		request.setTitle("Downloading " + module.getName() + " Module");

		//Setting description of request
		request.setDescription("Downloading modules resources");

		request.setDestinationUri(FileUtil.getAudioUri(module.getId() + ".zip"));

		//Enqueue download and save into referenceId
		downloader.lastReferenceId = downloadManager.enqueue(request);
		downloader.lastModuleId = module.getId();
	}

	public static void setDownloader(ModuleDownloader downloader) {
		DownloadService.downloader = downloader;
	}

}