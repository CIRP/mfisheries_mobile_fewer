package com.cirp.mfisheries.core.location;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.PrefsUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Geofencer {

	private static String JSON_FILE_NAME = "_geofence.json";
	private static Polygon2 polygon;

	@Nullable
	public static Boolean isWithinCountry(Context context, double x, double y) {
		try {
			if(polygon == null) {
				String json = loadJsonFromFile(context);
				if (json == null) {
					Log.d("Geofencer", "Loading from asset");
					json = loadJsonFromAsset(context);
				}

				JSONArray points = new JSONObject(json).getJSONArray("points");
				List<Double> lats = new ArrayList<>();
				List<Double> lngs = new ArrayList<>();
				for (int i = 0; i < points.length(); i++) {
					lats.add(points.getJSONObject(i).getDouble("latitude"));
					lngs.add(points.getJSONObject(i).getDouble("longitude"));
				}
				polygon = new Polygon2(lats, lngs);
			}
			return polygon.isPointInPolygon(x, y);
		} catch (JSONException ex) {
			Log.wtf("mFisheries", "Error " + ex.getLocalizedMessage());
			return null;
		}
	}

	public static String getJSONFilePath(Context context) {
		return Environment.getExternalStorageDirectory().getPath() + FileUtil.FOLDER +
				PrefsUtil.getCountry(context).toLowerCase() + JSON_FILE_NAME;
	}

	@Nullable
	public static String loadJsonFromFile(Context context) {
		String file = getJSONFilePath(context);
		Log.d("Geofencer", file + "");
		//checks if data is cached and load if it is
		if (FileUtil.fileExists(file))
			return FileUtil.readJSONFile(PrefsUtil.getCountry(context).toLowerCase() + JSON_FILE_NAME);
		else
			return null;
	}

	@Nullable
	public static String loadJsonFromAsset(Context context) {
		try {
			String json;
			InputStream inputStream;
			String country = PrefsUtil.getCountry(context);
			Log.d("Geofencer", "Country = " + country);
			if (country.equalsIgnoreCase(CountryUtil.TRINIDAD)) {
				inputStream = context.getResources().openRawResource(R.raw.trinidad_geofence);
				Log.wtf("mFisheries Geofence", "Trinidad");
			} else if (country.equalsIgnoreCase(CountryUtil.GRENADA)) {
				inputStream = context.getResources().openRawResource(R.raw.grenada_geofence);
				Log.wtf("mFisheries Geofence", "Grenada");
			} else if (country.equalsIgnoreCase(CountryUtil.TOBAGO)) {
				inputStream = context.getResources().openRawResource(R.raw.tobago_geofence);
				Log.wtf("mFisheries Geofence", "Tobago");
			} else {
				inputStream = context.getResources().openRawResource(R.raw.grenada_geofence);
			}

			int size = inputStream.available();
			byte[] buffer = new byte[size];
			inputStream.read(buffer);
			inputStream.close();
			json = new String(buffer, "UTF-8");
			return json;
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Error" + ex.getLocalizedMessage());
			return null;
		}
	}

	private static class Polygon2 {

		private List<Double> yPoints;
		private List<Double> xPoints;
		private int numberOfNodes;

		public Polygon2(List<Double> xPoints, List<Double> yPoints) {
			this.yPoints = yPoints;
			this.xPoints = xPoints;
			numberOfNodes = yPoints.size();
		}

		public boolean isPointInPolygon(double y, double x) {
			boolean polygon_result = false;
			for (int i = 0, j = numberOfNodes - 1; i < numberOfNodes; j = i++) {
				if ((yPoints.get(i) < y && yPoints.get(j) >= y) || (yPoints.get(j) < y && yPoints.get(i) >= y)) {
					if (xPoints.get(i) + (y - yPoints.get(i)) / (yPoints.get(j) - yPoints.get(i)) * (xPoints.get(j) - xPoints.get(i)) < x) {
						polygon_result = !polygon_result;
					}
				}
			}
			Log.wtf("mFisheries", String.valueOf(polygon_result));
			return polygon_result;
		}
	}
}
