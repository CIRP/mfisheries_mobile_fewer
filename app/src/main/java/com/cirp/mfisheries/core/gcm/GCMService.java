package com.cirp.mfisheries.core.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.alerts.AlertDM;
import com.cirp.mfisheries.alerts.AlertsActivity;
import com.cirp.mfisheries.alerts.AlertsGroupActivity;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;


public class GCMService {//extends GCMListenerService

	/*@Override
	public void onMessageReceived(String from, Bundle data) {
		super.onMessageReceived(from, data);
		String message = data.getString("message");

		Log.d("GCM", "Alert: " + message);
		//TODO change what server sends back
		try {
			//Parsing json
			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<AlertDM> jsonAdapter = moshi.adapter(AlertDM.class);
			AlertDM alertMessage = jsonAdapter.fromJson(message);

			showNotification(alertMessage);

			//saving Alert to DB
			saveToDatabase(alertMessage);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void saveToDatabase(AlertDM alert) {
		try {
			DatabaseHandler database = new DatabaseHandler(GCMService.this, 4);
			database.open();
			database.alertDBHelper.addAlert(alert);
			database.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void showNotification(AlertDM alert) {
		Intent intent = new Intent(this, AlertsActivity.class);
		intent.putExtra("groupId", "1");
		intent.putExtra("groupName", "Public Advisories-Trinidad");

		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder alertNotification = new NotificationCompat.Builder(this)
				.setSmallIcon(R.drawable.ic_warning_white_24dp)
				.setOngoing(false)
				.setPriority(NotificationCompat.PRIORITY_HIGH)
				.setContentIntent(pendingIntent)
				.setContentTitle("Alert Received")
				.setContentText(alert.messagecontent);

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify("Alert", 3, alertNotification.build());
		Log.wtf("mFisheries Alerts", "Notification Created");
	}*/
}
