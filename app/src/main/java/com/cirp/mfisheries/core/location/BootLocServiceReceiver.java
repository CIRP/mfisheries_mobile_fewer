package com.cirp.mfisheries.core.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;


public class BootLocServiceReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("mFisheriesLocService", "Starting Location Service");
		Intent i = new Intent(context, LocationService.class);
		context.startService(i);
		Toast.makeText(context, "mFisheries configured on Boot", Toast.LENGTH_LONG).show();
		Log.d("mFisheriesLocService", "mFisheries Location Service Started");
	}
}
