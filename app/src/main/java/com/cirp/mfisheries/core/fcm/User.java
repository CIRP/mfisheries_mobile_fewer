package com.cirp.mfisheries.core.fcm;

/**
 * Created by kcred on 5/18/2017.
 */

public class User {

    public String userid;
    public String token;

    public User(String userid, String token) {
        this.userid = userid;
        this.token = token;
    }
}
