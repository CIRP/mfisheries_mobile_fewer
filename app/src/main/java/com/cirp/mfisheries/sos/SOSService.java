package com.cirp.mfisheries.sos;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.core.location.TrackPointDM;
import com.cirp.mfisheries.util.LocationUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.util.SMSUtil;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class SOSService extends Service {

	private Handler thread_result_handler;
	private WakeLock wake_lock;
	private Location current_location;
	private Timer sos_timed_task;
	private NotificationCompat.Builder service_notification;
	private final int sos_time_interval = 60000;
	private final int notification_id = 3;
	private final String notification_tag = "SOS_AMBER_SERVICE";

	private boolean web_call_status, sms_call_status, voice_call_status;
	private static final byte UPDATE_SOS_NOTIFICATION = 1;
	private String web_call_status_string, sms_call_status_string;
	SharedPreferences shared_preferences;
	private NotificationManager notification_manager;
	private Tracker appTracker = null;
	private long timeZero = SystemClock.elapsedRealtimeNanos(), timeServiceStarted, timeWebmsgSent, timeWebmsgAck, timeCallOpened, timeSmsSent, timeSmsDelivered;
	private long tempTime;

	@Override
	public void onCreate() {
		super.onCreate();
		thread_result_handler = new Handler(new ThreadResultHandler());
		web_call_status = false;
		sms_call_status = false;
		voice_call_status = false;
		web_call_status_string = "Preparing Web Message";
		sms_call_status_string = "Preparing SMS Message";

		PrefsUtil.setSOSInProgress(this, true);

		Log.i("SOSService", "Starting SOS Service");
		shared_preferences = PreferenceManager.getDefaultSharedPreferences(SOSService.this);
		try {
			PowerManager power_manager = (PowerManager) getSystemService(Context.POWER_SERVICE);
			wake_lock = power_manager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "LocationPollServiceWakeLock");
			wake_lock.acquire();
			Log.i("SOSService", "Lock to prevent device from sleeping acquired");
		} catch (Exception ex) {
			Log.wtf("SOSService", "Location Service On Create, Error acquiring wake lock " + ex.getLocalizedMessage());
		}

		try {
			SharedPreferences.Editor editor = shared_preferences.edit();
			editor.putBoolean("amber_sos_in_progress", true).apply();

			Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

			service_notification = new NotificationCompat.Builder(this)
					.setSmallIcon(R.drawable.ic_warning_white_24dp)
					.setOngoing(true)
					.setPriority(NotificationCompat.PRIORITY_MAX)
					.setContentTitle("Sending SOS Alert")
					.setSound(alarmSound)
					.setAutoCancel(true)
					.setContentText(web_call_status_string + "\n" + sms_call_status_string);

			notification_manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			notification_manager.notify(notification_tag, notification_id, service_notification.build());

			Log.i("SOSService", "SOS Service Notification Displayed");
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Location Service On Create, Error Creating Location Service Notification " + ex.getLocalizedMessage());
		}

		registerReceiver(send_receiver, new IntentFilter("SMS_SENT"));
		registerReceiver(delivered_receiver, new IntentFilter("SMS_DELIVERED"));
		registerReceiver(call_listener, new IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED));

		sos_timed_task = new Timer();
		sos_timed_task.schedule(new SendSos(), 0); //Why do we wait 60 seconds before executing actions?- changed to zero for first run

		try {
			////Analytics
			// Obtain the shared Tracker instance.
			App application = (App) getApplication();
			appTracker = application.getDefaultTracker();
			appTracker.setSessionTimeout(300); //starts a new session after user has placed the app in background for 300seconds
			appTracker.setClientId(PrefsUtil.getCountryId(getApplicationContext()) + "-" + ((Integer) PrefsUtil.getUserId(getApplicationContext())).toString());
			Log.d("mF-ClientID", PrefsUtil.getCountryId(getApplicationContext()) + "-" + ((Integer) PrefsUtil.getUserId(getApplicationContext())).toString());

			timeServiceStarted = SystemClock.elapsedRealtimeNanos() - timeZero;
			appTracker.send(new HitBuilders.EventBuilder()
					.setCategory("Performance")
					.setAction("SOS, Delays")
					.setLabel("Serv.Starts-" + (timeServiceStarted / 1000000))
					.build());


			////
		} catch (Exception e) {
			Log.d("mFisheries-Analytics", "Tracker exception - " + e.getLocalizedMessage());
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		SharedPreferences.Editor editor = shared_preferences.edit();
		editor.putBoolean("amber_sos_in_progress", false).apply();
		unregisterReceiver(send_receiver);
		unregisterReceiver(delivered_receiver);
		unregisterReceiver(call_listener);
		wake_lock.release();
	}

	private class SendSos extends TimerTask {
		@Override
		public void run() {
			PrefsUtil.setSOSInProgress(SOSService.this, false);
			tempTime = SystemClock.elapsedRealtimeNanos() - timeZero;
			Log.d("SOS:", "run");
			try {
				// current_location = new Location("prov");
				// current_location.setLatitude(10.56346); current_location.setLongitude(10.649036963); //
				current_location = LocationService.userLocation;
				Log.d("SOS:", "in perf wc" + current_location.toString());
			} catch (Exception ex) {
				Log.wtf("mFisheries", "SOS Service SendSos, Error acquiring current position " + ex.getLocalizedMessage());
			}
			if (current_location == null) {
				Log.d("SOSService", "Loc is null");
				//TODO Attempt to re-fetch current location
			} else if (current_location != null) {
				if (!web_call_status) {
					Log.d("SOSService", "Sending Web Service Info");
					performWebCall();
				}
				if (!voice_call_status) {
					Log.d("SOSService", "Performing Voice Call");
					performVoiceCall();
				}
			} else {
				sos_timed_task.schedule(new SendSos(), sos_time_interval);
			}

			if (web_call_status && sms_call_status && voice_call_status) {
				// Removes the Notification After sent
				if (notification_manager != null) {
//					notification_manager.cancel(notification_id);
					Log.d("SOSService", "All Three tasks was completed successfully for SOS");
					service_notification
							.setContentText(web_call_status_string + "\n" + sms_call_status_string)
							.setOngoing(false);

					notification_manager.notify(notification_tag, notification_id, service_notification.build());
				}
				stopSelf();
			} else {
				sos_timed_task.schedule(new SendSos(), sos_time_interval);
			}

			//Update Notification with changes if any on the SOS
			if (notification_manager != null && service_notification != null) {
				service_notification.setContentText(web_call_status_string + "\n" + sms_call_status_string);
				notification_manager.notify(notification_tag, notification_id, service_notification.build());
			}
		}
	}

	public void performWebCall() {
		Log.d("SOS:", "in perf wc");

		try {
			List<TrackPointDM> points = new ArrayList<>();
			Log.d("SOS:", "in perf try wc");
			TrackPointDM sos = SMSUtil.createSOS(current_location, getBaseContext());
			Log.d("SOS:", "SOS created successfully");
			points.add(sos);

			JsonArray jsonArray = getJsonElements(points);

			Log.d("SOS", "URL: " + NetUtil.API_URL + NetUtil.POST_TRACK_URL);
			timeWebmsgSent = SystemClock.elapsedRealtimeNanos() - timeZero;
			sendSOS(jsonArray, new FutureCallback<JsonObject>() {
				@Override
				public void onCompleted(Exception e, JsonObject result) {
					if (e == null && result != null) {
						Log.d("SOS", "SOS sent " + result.toString());
						web_call_status = true;
						web_call_status_string = "Web Message Sent";
						thread_result_handler.sendEmptyMessage(UPDATE_SOS_NOTIFICATION);
						timeWebmsgAck = SystemClock.elapsedRealtimeNanos() - timeZero;
						try {
							appTracker.send(new HitBuilders.EventBuilder()
									.setCategory("Performance")
									.setAction("SOS, Delays")
									.setLabel("WebAck-" + (timeWebmsgAck / 1000000))
									.build());


						} catch (Exception ex) {
							Log.d("mFisheries-Analytics", "Tracker exception - " + ex.getLocalizedMessage());
						}
					} else {
						if (e != null) {
							Log.d("SOS", "SOS not sent " + e.getMessage());
						} else {
							Log.d("SOS", "SOS not sent");
						}
						Toast.makeText(getApplicationContext(), "Sorry, try again", Toast.LENGTH_SHORT).show();
					}
				}
			});

		} catch (Exception ex) {
			Log.wtf("mFisheries", "sos service perform web all, Error  sending the web call message " + ex.getLocalizedMessage());
		}
	}

	public void sendSOS(@NonNull JsonArray jsonArray, FutureCallback<JsonObject> callback) {
		//building JSON
		Ion.with(this)
                .load(NetUtil.API_URL + NetUtil.POST_TRACK_URL)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                .setJsonArrayBody(jsonArray)
                .asJsonObject()
                .setCallback(callback);
	}

	public JsonArray getJsonElements(@NonNull List<TrackPointDM> points) {
		try {
			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<List> jsonAdapter = moshi.adapter(List.class);
			String json = jsonAdapter.toJson(points);
			Log.d("SOS:", "JSON: " + json);
			JsonParser parser = new JsonParser();
			return (JsonArray) parser.parse(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public void performSmsCall() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
			String mobileNum = PrefsUtil.getSOSMobile(this);
			String extraNum = PrefsUtil.getSOSExtraPhone(this);
			Log.d("SOS:", "mobile: " + mobileNum + " extra:" + extraNum);

			//TODO Add the date to the SMS
			String message = PrefsUtil.getUser(getApplicationContext()) + " " +
					LocationUtil.convertLatitude(current_location.getLatitude()) + " " +
					LocationUtil.convertLongitude(current_location.getLongitude()) + " " +
					"Dir:" + String.format(Locale.US, "%.1f", current_location.getBearing()) + (char) 0x00B0 + " " +
					"Speed:" + String.format(Locale.US, "%.1f", current_location.getSpeed()) + "m/s";

			if (!mobileNum.equals("")) {
				SMSUtil.sendSMS(this, message, mobileNum);
			} else {
				Log.d("SOSService", "No number was specified");
			}

			if (!extraNum.equals("")) {
				SMSUtil.sendSMS(this, message, extraNum);
			} else {
				Log.d("SOSService", "No Extra Number specified");
			}
		}
	}

	private void performVoiceCall() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
			String phone_number = PrefsUtil.getSOSLand(this);
			Log.d("SOS voice", "Calling:  " + phone_number);
			Intent intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel:" + phone_number));
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_FROM_BACKGROUND);
			startActivity(intent);
		}
	}

	private class ThreadResultHandler implements Handler.Callback {
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
				case UPDATE_SOS_NOTIFICATION:
					NotificationManager notification_manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
					service_notification.setContentText(web_call_status_string + "\n" + sms_call_status_string);
					notification_manager.notify(notification_tag, notification_id, service_notification.build());
					return true;
			}
			return false;
		}
	}

	private BroadcastReceiver send_receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context arg0, Intent arg1) {
			Log.d("SOSService", "SMS event received");
			switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(arg0, "SMS sent successfully", Toast.LENGTH_SHORT).show();
					sms_call_status = true;
					sms_call_status_string = "SMS Sent";
					thread_result_handler.sendEmptyMessage(UPDATE_SOS_NOTIFICATION);
					try{
						timeSmsSent= SystemClock.elapsedRealtimeNanos()-timeZero;
						appTracker.send(new HitBuilders.EventBuilder()
								.setCategory("Performance")
								.setAction("SOS, Delays")
								.setLabel("SmsSent-" + (timeSmsSent/1000000) + "ms" )
								.build());


					}catch (Exception e)
					{
						Log.d("mFisheries-Analytics", "Tracker exception - "+ e.getLocalizedMessage());
					}
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					Log.e("SOSService", "Starting SOS Service");
					Toast.makeText(getBaseContext(), "Unable to Send SMS:Generic failure",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					Log.e("SOSService", "Starting SOS Service");
					Toast.makeText(getBaseContext(), "No service",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					Log.e("SOSService", "Starting SOS Service");
					Toast.makeText(getBaseContext(), "Null PDU",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					Log.e("SOSService", "Starting SOS Service");
					Toast.makeText(getBaseContext(), "Radio off",
							Toast.LENGTH_SHORT).show();
					break;
				default:
					break;
			}
		}
	};


	private BroadcastReceiver delivered_receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context arg0, Intent arg1) {
			switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
					Log.d("SOSService", "SMS was delivered");
					sms_call_status = true;
					sms_call_status_string = "SMS Delivered";
					thread_result_handler.sendEmptyMessage(UPDATE_SOS_NOTIFICATION);
					break;
				case Activity.RESULT_CANCELED:
					Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
					Log.e("SOSService", "SMS was not delivered");
					break;
			}
		}
	};


	private BroadcastReceiver call_listener = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			//TODO Determine a better way to determine when the call successfully made
//			if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.CALL_STATE_OFFHOOK)) {
			if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
				voice_call_status = true;
				Log.d("SOS", "voice call was successfully completed true");
					timeCallOpened = SystemClock.elapsedRealtimeNanos()-timeZero;
				try{
					appTracker.send(new HitBuilders.EventBuilder()
							.setCategory("Performance")
							.setAction("SOS, Delays")
							.setLabel("CallOpened-" + (timeCallOpened/1000000))
							.build());


				}catch (Exception ex)
				{
					Log.d("mFisheries-Analytics", "Tracker exception - "+ ex.getLocalizedMessage());
				}
				performSmsCall();
			}
		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}