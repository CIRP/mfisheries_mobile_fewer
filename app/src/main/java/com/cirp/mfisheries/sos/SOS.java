package com.cirp.mfisheries.sos;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.RegisterActivity;
import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;

public class SOS extends Module {

	public SOS(Context context) {
		super(context);
	}

	@Override
	public Module initialize() {
		this.dataUrl = NetUtil.SITE_URL + PrefsUtil.getPath(this.context) + "/SOS.zip";
		return super.initialize();
	}


	@Override
	protected Module setModuleId() {
		this.moduleId = "SOS";
		return this;
	}

	@Override
	protected Module setModuleName() {
		this.name = "SOS";
		return this;
	}

	@Override
	protected Module setIsDisplayed() {
		this.displayed = false;
		return this;
	}

	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_sos;
		return this;
	}

	@Override
	protected Module setNeedsRegistration() {
		this.needsRegistration = true;
		return this;
	}

	@Override
	protected Module setActivityClass() {
		this.activityClass = SOSActivity.class;
		return this;
	}

	@Override
	public void onInstalled() {
		Log.d(moduleId, name + " Module Installed, Launching check to ensure logged in");
		String googleId = PrefsUtil.getGoogleId(context);
		Log.d("GoogleId", googleId + "");
		if (googleId.equals("")) { // Sign-in not completed (Assuming that its in the process of completion
			// Sign in occurred before however we now added the SOS after installation

			// Send Broadcast to trigger
			Intent i = new Intent(RegisterActivity.BROADCAST_ID);
			LocalBroadcastManager.getInstance(context).sendBroadcast(i);
		}

	}
}

