package com.cirp.mfisheries.sos;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.MainActivity;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.location.LocationDependentActivity;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class SOSActivity extends LocationDependentActivity {

	private long timeZero  = SystemClock.elapsedRealtimeNanos();
	private TextView sosCountdownText;
	private Button sosCancelButton2, sosCancelButton1;
	private CountDownTimer countDownTimer;
	private final int SOS_TIME_MS = 30000;
	private Tracker appTracker = null;
	private boolean COUNTDOWN_STARTED;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		module = new SOS(this);

		sosCancelButton1 = (Button) findViewById(R.id.sos_countdown_cancel_1);
		sosCancelButton2 = (Button) findViewById(R.id.sos_countdown_cancel_2);
		sosCountdownText = (TextView) findViewById(R.id.sos_countdown);

		permissions = new String[]{Manifest.permission.SEND_SMS, Manifest.permission.CALL_PHONE,
				Manifest.permission.ACCESS_FINE_LOCATION};
		requestPermissions();
	}

	@Override
	public void onPermissionGranted(String permission) {
		super.onPermissionGranted(permission);
		if(!LocationService.trackingEnabled){
			showAlert("You can only send and SOS when offshore.");
			return;
		}
		if(LocationService.userLocation == null){
			showAlert("Cannot get your current location");
			return;
		}
		if(PrefsUtil.isSOSInProgress(this)){
			showAlert("An SOS is already being sent");
			return;
		}
		if(LOCATION_ENABLED)
			startCountdown();
	}

	private void showAlert(String message) {
		new AlertDialog.Builder(this)
                .setTitle(message)
                .setCancelable(false)
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(SOSActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                })
                .show();
	}

	private synchronized void startCountdown() {
		if(!COUNTDOWN_STARTED) {
			Log.d("SOS", "Countdown started");
			COUNTDOWN_STARTED = true;
			track();

			countDownTimer = new CountDownTimer(SOS_TIME_MS, 1000) {
					@Override
					public void onTick(long millisUntilFinished) {
						String time = String.valueOf(millisUntilFinished / 1000);
						sosCountdownText.setText(time);
						Log.d("SOS", this.toString() + " " + time + "");
					}

					@Override
					public void onFinish() {
						try {
							appTracker.send(new HitBuilders.EventBuilder()
									.setCategory("Performance")
									.setAction("SOS, Countdown Finished")
									.setLabel("Time")
									.setValue((SystemClock.elapsedRealtimeNanos() - timeZero) / 1000000)
									.build());
						} catch (Exception e) {
							Log.d("mFisheries-Analytics", "Tracker exception - " + e.getLocalizedMessage());
						}
						Intent sosServiceIntent = new Intent(SOSActivity.this, SOSService.class);
						startService(sosServiceIntent);
						finish();
					}
				}.start();
		}
	}

	private void track() {
		try {
			////Analytics
			// Obtain the shared Tracker instance.
			App application = (App) getApplication();
			appTracker = application.getDefaultTracker();
			appTracker.setClientId( PrefsUtil.getCountryId(getApplicationContext()) +"-"+ ((Integer) PrefsUtil.getUserId(getApplicationContext())).toString());
			Log.d("mF-ClientID", PrefsUtil.getCountryId(getApplicationContext()) +"-"+ ((Integer) PrefsUtil.getUserId(getApplicationContext())).toString());
		} catch (Exception e) {
			Log.d("mFisheries-Analytics", "Tracker exception - "+ e.getLocalizedMessage());
		}
	}


	public void cancelSOS1(View view) {
		try{
			appTracker.send(new HitBuilders.EventBuilder()
					.setCategory("UI")
					.setAction("SOS, Button Pressed")
					.setLabel("Cancel 1")
					.setValue((SystemClock.elapsedRealtimeNanos() - timeZero)/1000000)
					.build());
		} catch (Exception e) {
			Log.d("mFisheries-Analytics", "Tracker exception - "+ e.getLocalizedMessage());
		}
		sosCancelButton1.setVisibility(Button.INVISIBLE);
		sosCancelButton2.setVisibility(Button.VISIBLE);
	}

	public void cancelSOS2(View view) {
		try{
			appTracker.send(new HitBuilders.EventBuilder()
					.setCategory("UI")
					.setAction("SOS, Button Pressed")
					.setLabel("Cancel 2")
					.setValue((SystemClock.elapsedRealtimeNanos() - timeZero)/1000000)
					.build());
		} catch (Exception e) {
			Log.d("mFisheries-Analytics", "Tracker exception - " + e.getLocalizedMessage());
		}
		cancelCountdown();
	}

	private void cancelCountdown() {
		Log.d("SOS", "Countdown canceled");
		countDownTimer.cancel();
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
	}


	public int getLayoutResourceId() {
		return R.layout.activity_sos;
	}

	@Override
	public int getColor(){
		return R.color.red;
	}

	@Override
	public int getColorDark(){
		return R.color.redDark;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(countDownTimer != null)
			countDownTimer.cancel();
	}
}