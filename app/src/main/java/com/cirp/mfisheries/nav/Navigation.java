package com.cirp.mfisheries.nav;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;

public class Navigation extends Module {

	public Navigation(Context context){
		super(context);
	}

	@Override
	public Module initialize() {
		this.dataUrl = NetUtil.SITE_URL + PrefsUtil.getPath(this.context) + "/Navigation.zip";
		return super.initialize();
	}

	@Override
	protected Module setModuleId() {
		this.moduleId = "Navigation";
		return this;
	}

	@Override
	protected Module setModuleName() {
		this.name = "Navigation";
		return this;
	}

	@Override
	protected Module setIsDisplayed() {
		this.displayed = false;
		return this;
	}

	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_nav;
		return this;
	}

	@Override
	protected Module setNeedsRegistration() {
		this.needsRegistration = true;
		return this;
	}

	@Override
	protected Module setActivityClass() {
		this.activityClass = NavigationActivity.class;
		return this;
	}

}
