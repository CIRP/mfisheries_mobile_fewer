package com.cirp.mfisheries.nav;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.core.ModuleActivity;

public class AddMarkerActivity extends ModuleActivity {
	private EditText markerName, marker_sec, marker_deg, marker_min;
	private Spinner markerType, markerDetails;
	private MapMarkerDM newPoint;
	
	private Handler thread_result_handler;
	private ProgressDialog progressDialog;

	private static final byte MARKER_SAVED = 0;
	private static final byte ERROR = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		module = new Navigation(this);
		thread_result_handler = new Handler(new ThreadResultHandler());
		
		newPoint = new MapMarkerDM();
		newPoint.user_id = getIntent().getIntExtra("user_id", 0);
		double longitude = getIntent().getDoubleExtra("longitude", 0.0);
		double latitude = getIntent().getDoubleExtra("latitude", 0.0);

		double abs_latitude = Math.abs(latitude);
		int deg = (int) abs_latitude;
		int min = (int) Math.abs((abs_latitude - deg) * 60);
		float sec = (float) Math.abs((((abs_latitude - deg) * 60) - min) * 60);
		if (latitude < 0) {
			deg = -1 * deg;
		}
		marker_deg = (EditText) findViewById(R.id.add_marker_lat_deg);
		marker_min = (EditText) findViewById(R.id.add_marker_lat_min);
		marker_sec = (EditText) findViewById(R.id.add_marker_lat_sec);
		marker_deg.setText(String.valueOf(deg));
		marker_min.setText(String.valueOf(min));
		marker_sec.setText(String.valueOf(sec));

		//convert the longitude to the required format
		double abs_longitude = Math.abs(longitude);
		deg = (int) abs_longitude;
		min = (int) Math.abs((abs_longitude - deg) * 60);
		sec = (float) Math.abs((((abs_longitude - deg) * 60) - min) * 60);
		if (longitude < 0) {
			deg = -1 * deg;
		}
		marker_deg = (EditText) findViewById(R.id.add_marker_lng_deg);
		marker_min = (EditText) findViewById(R.id.add_marker_lng_min);
		marker_sec = (EditText) findViewById(R.id.add_marker_lng_sec);
		marker_deg.setText(String.valueOf(deg));
		marker_min.setText(String.valueOf(min));
		marker_sec.setText(String.valueOf(sec));

		markerDetails = (Spinner) findViewById(R.id.add_marker_details_spinner);
		markerType = (Spinner) findViewById(R.id.add_marker_type_spinner);
		markerName = (EditText) findViewById(R.id.add_marker_name);
		
		String[] marker_type_options = getResources().getStringArray(R.array.marker_types);
		ArrayAdapter<String> marker_type_adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, marker_type_options);
		marker_type_adapter.setDropDownViewResource(R.layout.custom_spinner_item);

		String[] private_marker_types = getResources().getStringArray(R.array.private_marker_types);
		final ArrayAdapter<String> private_marker_type_adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, private_marker_types);
		private_marker_type_adapter.setDropDownViewResource(R.layout.custom_spinner_item);

		String[] hazard_marker_types = getResources().getStringArray(R.array.hazard_marker_types);
		final ArrayAdapter<String> hazard_marker_type_adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, hazard_marker_types);
		hazard_marker_type_adapter.setDropDownViewResource(R.layout.custom_spinner_item);
		markerType.setAdapter(marker_type_adapter);
		markerType.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				switch (arg2) {
					case 0:
						markerDetails.setAdapter(private_marker_type_adapter);
						break;
					case 1:
						markerDetails.setAdapter(hazard_marker_type_adapter);
						break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
	}

	private class ThreadResultHandler implements Handler.Callback {

		@Override
		public boolean handleMessage(Message msg) {
			progressDialog.dismiss();
			switch (msg.what) {
				case MARKER_SAVED:
					Toast.makeText(AddMarkerActivity.this, "Marker Saved Successfully", Toast.LENGTH_LONG).show();
					Intent data = new Intent();
					data.putExtra("id", newPoint.id);
					data.putExtra("type", newPoint.type);
					data.putExtra("name", newPoint.name);
					data.putExtra("description", newPoint.details);
					data.putExtra("latitude", newPoint.latitude);
					data.putExtra("longitude", newPoint.longitude);
					data.putExtra("timestamp", newPoint.timestamp);
					data.putExtra("user_id", newPoint.user_id);
					if (getParent() == null) {
						setResult(Activity.RESULT_OK, data);
					} else {
						getParent().setResult(Activity.RESULT_OK, data);
					}
					finish();
					return true;
				case ERROR:
					Toast.makeText(AddMarkerActivity.this, "Marker Not Saved", Toast.LENGTH_LONG).show();
					if (getParent() == null) {
						setResult(Activity.RESULT_CANCELED, null);
					} else {
						getParent().setResult(Activity.RESULT_CANCELED, null);
					}
					finish();
					return true;
			}
			return false;
		}
	}
	
	public void addMarker(View view) {
		progressDialog = ProgressDialog.show(AddMarkerActivity.this, "Please Wait", "Saving Marker Information");
		String markerTypeDetails = markerType.getSelectedItem().toString();
		String markerNameTemp = markerTypeDetails;
		String markerDetails = this.markerDetails.getSelectedItem().toString();
		int marker_type = 0;
		if (!markerName.getText().toString().equals("")) {
			markerNameTemp = markerName.getText().toString();
		}
		if (markerTypeDetails.equals("Private")) {
			marker_type = 0;
		} else if (markerTypeDetails.equals("Hazard")) {
			marker_type = 1;
		}
		markerDetails = markerDetails + " " + ((EditText) findViewById(R.id.add_marker_further_details)).getText().toString();
		newPoint.type = marker_type;
		newPoint.name = markerNameTemp;
		newPoint.details = markerDetails;
		try {
			marker_deg = (EditText) findViewById(R.id.add_marker_lat_deg);
			marker_min = (EditText) findViewById(R.id.add_marker_lat_min);
			marker_sec = (EditText) findViewById(R.id.add_marker_lat_sec);
			int deg = Integer.valueOf(marker_deg.getText().toString());
			int min = Integer.valueOf(marker_min.getText().toString());
			double sec = Double.valueOf(marker_sec.getText().toString());
			double latitude = Math.abs(deg) + (sec + 60 * min) / 3600;
			if (deg < 0) {
				latitude = latitude * -1;
			}
			
			marker_deg = (EditText) findViewById(R.id.add_marker_lng_deg);
			marker_min = (EditText) findViewById(R.id.add_marker_lng_min);
			marker_sec = (EditText) findViewById(R.id.add_marker_lng_sec);
			deg = Integer.valueOf(marker_deg.getText().toString());
			min = Integer.valueOf(marker_min.getText().toString());
			sec = Double.valueOf(marker_sec.getText().toString());
			double longitude = Math.abs(deg) + (sec + 60 * min) / 3600;
			if (deg < 0) {
				longitude = longitude * -1;
			}
			newPoint.latitude = String.valueOf(latitude);
			newPoint.longitude = String.valueOf(longitude);
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Map Marker DM, Error saving " + ex.getLocalizedMessage());
		}
		
		Thread add_marker_thread = new Thread() {
			@Override
			public void run() {
				super.run();
				try {
					DatabaseHandler database = new DatabaseHandler(AddMarkerActivity.this, 1);
					database.open();
					newPoint.id = database.mapMarkerDBHelper.addMapMarker(newPoint);
					database.close();
					thread_result_handler.sendEmptyMessage(MARKER_SAVED);
				} catch (Exception ex) {
					Log.wtf("mFisheries", ex.toString());
					thread_result_handler.sendEmptyMessage(ERROR);
				}
			}
		};
		add_marker_thread.start();
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_add_map_marker;
	}

	@Override
	public int getColor(){
		return R.color.yellow;
	}

	@Override
	public int getColorDark(){
		return R.color.yellowDark;
	}
}
