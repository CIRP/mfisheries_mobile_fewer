package com.cirp.mfisheries;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.core.BaseActivity;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.core.ModuleAdapter;
import com.cirp.mfisheries.core.ModuleFactory;
import com.cirp.mfisheries.core.NetworkChangeReceiver;
import com.cirp.mfisheries.core.download.ModuleDownloader;
import com.cirp.mfisheries.core.download.ZipService;
import com.cirp.mfisheries.core.fcm.User;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.sos.SOS;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.ModuleUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.util.RegisterBroadcastHandler;
import com.cirp.mfisheries.util.WebViewActivity;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends BaseActivity {

	private static final String TAG = "MainActivity";
	private boolean NEEDS_REGISTER;
	private boolean SIGNED_IN;
	private SharedPreferences preferences;
	private String country;
	private String countryId;
	private ArrayList<String> selectedModules;
	private boolean[] selected;
	private ProgressDialog downloadProgress;
	private RegisterBroadcastHandler registerBroadcastHandler = new RegisterBroadcastHandler(this);
	private NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();
	private ModuleDownloader downloader;

	private FirebaseAuth mAuth;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (toolbar != null) {
			setSupportActionBar(toolbar);
		}

		//Retrieve SharedPreferences object
		preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

		// Register Listener to react if the SOS module requires registration
		registerListeners();

		//Store FCM token
		PrefsUtil.setToken(this, FirebaseInstanceId.getInstance().getToken());

		permissions = new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION};
		requestPermissions();

		setGreeting();

		mAuth = FirebaseAuth.getInstance();
		if(mAuth.getCurrentUser() == null){
			firebaseSignIn();
		}
	}

	private void firebaseSignIn() {
		mAuth.signInAnonymously()
			.addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
				@Override
				public void onComplete(@NonNull Task<AuthResult> task) {
					if (task.isSuccessful()) {
						Log.d(TAG, "signInAnonymously:success");
						String token = FirebaseInstanceId.getInstance().getToken();
						String userId = PrefsUtil.getUserId(MainActivity.this) + "";
						User user = new User(userId, token);
						DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
						mDatabase.child("users").child(userId).setValue(user);
					} else {
						Log.w(TAG, "signInAnonymously:failure", task.getException());
					}
				}
			});
	}

	@Override
	public void onPermissionGranted(String permission) {
		super.onPermissionGranted(permission);

		// Retrieve the selected modules from the local File system
		List<String> modules = getSelectedModules();
		loadData(modules);
	}

	private void loadData(List<String> modules) {
		// Check if Application Running for the First Time
		if (PrefsUtil.isFirstOpen(this) || PrefsUtil.getCountry(this).equals("")) {
			Log.d(TAG, "The mFisheries App was opened for the first time");
			loadCountries();
		} else { //load already selected modules
			Log.d(TAG, "Number of Modules Selected: " + modules.size());

			//get selected country from prefs
			country = PrefsUtil.getCountry(this);
			countryId = PrefsUtil.getCountryId(this);

			Log.v(TAG, "Loading data for country: " + country + "(" + countryId + ")");

			// Get if username previously registered
			boolean isRegistered = PrefsUtil.getIsRegistered(this);

			if (downloadNeeded(modules))
				downloadModulesResources(modules);
			else
				displayModules(modules);

			if (PrefsUtil.hasUserId(this) && PrefsUtil.getUserId(this) != -999 ) {
				Log.d(TAG, "User ID was set");
				SIGNED_IN = true;
				startLocationServices();
				if (!isRegistered) { // has id but profile not completed
					// Relaunch web view activity
					Intent webViewIntent = new Intent(this, WebViewActivity.class);
					startActivity(webViewIntent);
				} else {
					Log.d(TAG, "User id set but the system is not marked as registered");
				}
			} else {
				Log.d(TAG, "User ID was not set");
				SOS s = new SOS(MainActivity.this);
				if (modules.contains(s.getId())) {
					loadCountries();
				}
			}
		}

		setGreeting();
		track();
	}

	//checks whether each selected module has finished downloading successfully
	public boolean downloadNeeded(List<String> modules) {
		for (String module : modules) {
			Module module1 = ModuleFactory.getInstance(this).getModule(module);
			if (!preferences.getBoolean(module1.getId(), false))
				return true;
		}
		return false;
	}

	public void loadCountries() {
		Log.d(TAG, "Load Countries executed");
		if (!NetUtil.isOnline(this)) {
			NetUtil.retryDialog(this, new NetUtil.OnRetryClicked() {
				@Override
				public void retry() {
					loadCountries();
				}
			});
			return;
		}
		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.loading_countries));
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(true);
		progress.setCancelable(false);
		progress.show();
		Ion.with(this)
				.load(NetUtil.API_URL + NetUtil.GET_COUNTRIES_PATH)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						try {
							Log.d(TAG, "Retrieving information from: " + NetUtil.API_URL + NetUtil.GET_COUNTRIES_PATH);
							if (e != null) {
								Log.d(TAG, "Error Loading Countries: " + e.getMessage());
								Toast.makeText(MainActivity.this, "Unable to retrieve Countries, Try again later", Toast.LENGTH_LONG).show();
							} else {
								Log.d(TAG, "Result: " + result.toString());
								int status = result.get("status").getAsInt();
								if (status == 200) {
									JsonArray array = result.getAsJsonArray("data");

									Moshi moshi = new Moshi.Builder().build();
									Type listMyData = Types.newParameterizedType(List.class, Country.class);
									JsonAdapter<List<Country>> adapter = moshi.adapter(listMyData);

									List<Country> countries = adapter.fromJson(array.toString());

									selectCountry(countries);
								} else if (status == 204) {
									Toast.makeText(MainActivity.this, result.get("data").getAsString(), Toast.LENGTH_SHORT).show();
									NetUtil.retryDialog(MainActivity.this, new NetUtil.OnRetryClicked() {
										@Override
										public void retry() {
											loadCountries();
										}
									});
									return;
								}
							}
							progress.dismiss();
						} catch (Exception ex) {
							Toast.makeText(MainActivity.this, "Unable to retrieve Countries, Try again later", Toast.LENGTH_LONG).show();
							Log.d(TAG, "Failed to retrieve country data from " + NetUtil.API_URL + NetUtil.GET_COUNTRIES_PATH);
							ex.printStackTrace();
						}
					}
				});
	}

	public void selectCountry(final List<Country> countries){
		Log.d(TAG, "Loading Select Country");
		//show country dialog
		final CountryAdapter arrayAdapter = new CountryAdapter(this, countries);
		new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.dialog_country))
				.setCancelable(false)
				.setAdapter(arrayAdapter,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								Country selectedCountry = countries.get(which);
								country = selectedCountry.getName();
								Log.d(TAG, "Setting Country to: " + selectedCountry.getName());
								Log.d(TAG, "Setting Path to: " + selectedCountry.getPath());
								Log.d(TAG, "Country selected has an id of: " + selectedCountry.getId());
								saveCountryData(selectedCountry);

								//load available modules for country
								MainActivity.this.countryId = selectedCountry.getId();
								loadModules(country, MainActivity.this.countryId);
							}
						})
				.show();
	}

	public void loadModules(final String country, final String countryId) {
		Log.d(TAG, "Load Module Executed");
		if (!NetUtil.isOnline(this)) {
			NetUtil.retryDialog(this, new NetUtil.OnRetryClicked() {
				@Override
				public void retry() {
					loadModules(country, countryId);
				}
			});
			return;
		}
		this.country = country;
		this.countryId = countryId;
		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.loading_modules));
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(true);
		progress.setCancelable(false);
		progress.show();

		final String path = NetUtil.API_URL + NetUtil.GET_MODULES_PATH + countryId.charAt(0);
		Log.i(TAG, "Country ID selected: " + countryId + " Requested the Path: " + path);

		Ion.with(this)
				.load(path)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						Log.d(TAG, "Request Information from:  " + path);
						if (e != null) {
							Log.d(TAG, "Error Loading Modules: " + e.getMessage());

						} else {
							Log.d(TAG, "Result: " + result.toString());
							int status = result.get("status").getAsInt();
							if (status == 200) {
								JsonArray array = result.getAsJsonArray("data");
								String[] modules = new String[array.size()];
								for (int i = 0; i < array.size(); i++) {
									JsonObject object = array.get(i).getAsJsonObject();
									Log.d(TAG, "Found:"+object);
									modules[i] = object.get("name").getAsString();
								}
								selectModules(modules);
							} else if (status == 204) {
								selectModules(new String[0]);
							}
						}
						progress.dismiss();
					}
				});
	}

	public void selectModules(final String[] modules) {
		Log.d(TAG, "Selecting Modules");
		selected = new boolean[modules.length];

		//loads previously selected modules and sets them to display as checked in list
		selectedModules = (ArrayList) this.getSelectedModules();
		for (int i = 0; i < modules.length; i++) {
			selected[i] = selectedModules.contains(modules[i]);
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false)
				.setTitle(getResources().getString(R.string.dialog_modules))
				.setMultiChoiceItems(modules, selected,
						new DialogInterface.OnMultiChoiceClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which, boolean isChecked) {
								//keep track of selected modules
								selected[which] = isChecked;
								String module = modules[which];
								if (isChecked) {
									selectedModules.add(module);
								} else if (selectedModules.contains(module)) {
									selectedModules.remove(module);
								}
							}
						})
				.setPositiveButton(R.string.finish, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialogInterface, int which) {
						NEEDS_REGISTER = ModuleUtil.needsRegistration(MainActivity.this, selectedModules);
						//download modules
						Log.d(TAG, "User selected: " + selectedModules.toString());
						if (selectedModules.size() == 0) {
							saveSelectedModules(selectedModules);
							displayModules(selectedModules);
							return;
						}

						sortModules(selectedModules, ModuleUtil.MODULE_ORDER);
						saveSelectedModules(selectedModules);
						downloadModulesResources(selectedModules);
					}
				})
				.show();
	}

	public void displayModules(List<String> modules) {
		Log.d(TAG, "Displaying modules " + modules.toString());
		PrefsUtil.setFirstOpen(this, false);
		if (NEEDS_REGISTER && !SIGNED_IN) {
			Log.d(TAG, "Application Requires signin");
			startLocationServices();
			signIn(null);
		}
		if (modules == null)
			modules = new ArrayList<>();
		if (modules.size() % 2 != 0 || modules.size() == 0)
			modules.add(ModuleUtil.PLACEHOLDER_TILE);
		GridView gridView = (GridView) this.findViewById(R.id.gridView);
		ModuleAdapter adapter = new ModuleAdapter(this, modules);
		adapter.setPlaceholderListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				loadModules(country, countryId);
			}
		});
		gridView.setAdapter(adapter);
	}

	private void sortModules(List<String> selected, String[] allModules) {
		// Adds the Modules in the Order Specified by the Selected Country
		List<String> moduleOrder = new ArrayList<>(Arrays.asList(allModules));
		List<String> list = new ArrayList<>();
		int size = moduleOrder.size();
		for (int i = 0; i < size; i++) {
			if (selected.contains(moduleOrder.get(i))) {
				list.add(moduleOrder.get(i));
			}
		}
		selectedModules = (ArrayList<String>) list;
	}

	public void saveSelectedModules(ArrayList<String> modules) {
		Log.d(TAG, modules.toString());
		FileUtil.saveModules(modules);
	}

	public List<String> getSelectedModules() {
		return FileUtil.loadSavedModules();
	}

	private void saveCountryData(Country country) {
		//store to preferences
		PrefsUtil.setCountry(getApplicationContext(), country.getName());
		PrefsUtil.setPath(getApplicationContext(), country.getPath());
		PrefsUtil.setCountryId(getApplicationContext(), country.getId());

		if (country.getAuthority() != null) {
			Country.Authority authData = country.getAuthority();
			boolean isSet = false;
			//fix issue here
			if (authData.getMobileNum() != null) {
				PrefsUtil.setSOSMobile(MainActivity.this, authData.getMobileNum());
				Log.d(TAG, "Configured Emergency Contact as: " + authData.getMobileNum());
				isSet = true;
			}

			if (authData.getHomeNum() != null) {
				PrefsUtil.setSOSLand(MainActivity.this, authData.getHomeNum());
				Log.d(TAG, "Configured Emergency Contact as: " + authData.getHomeNum());
				isSet = true;
			}

			if (authData.getEmail() != null) {
				PrefsUtil.setSOSEmail(MainActivity.this, authData.getEmail());
				Log.d(TAG, "Configured Emergency Contact as: " + authData.getEmail());
				isSet = true;
			}

			if (isSet)
				Toast.makeText(MainActivity.this, "Successfully Configured Country Information", Toast.LENGTH_SHORT).show();

		}
	}

	public void downloadModulesResources(List<String> modules) {
		downloadProgress =  new ProgressDialog(this);
		downloadProgress.setMessage(getString(R.string.download_alert));
		downloadProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		downloadProgress.setIndeterminate(true);
		downloadProgress.setCancelable(false);
		downloadProgress.show();

		final Context context = this.getApplicationContext();

		downloader = new ModuleDownloader(this, new ModuleDownloader.OnAllDownloadsFinishedListener() {
			@Override
			public void onFinished() {
				downloadProgress.dismiss();
				if (selectedModules == null || selectedModules.isEmpty()) {
					Log.d(TAG, "Loading saved modules");
					selectedModules = (ArrayList) FileUtil.loadSavedModules();
					Log.d(TAG, "Number of Modules Selected: " + selectedModules.size());
				}
				Log.d(TAG, "User selected: " + selectedModules.toString());
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						displayModules(selectedModules);
					}
				});
			}

			@Override
			public void onNotification(String message, String type) {
				Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
			}
		});

		downloader.getModules(this, modules);
	}

	private boolean startLocationServices() {
		try {
			Log.d(TAG, "start location services");
			preferences.edit().putBoolean("loc_service_stopped", false).apply();
			ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
			for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
				if (LocationService.class.getName().equals(service.service.getClassName())) {
					Log.d(TAG, "mFisheries location services already started");
					return true;
				}
			}
			Intent location_poll_service = new Intent(MainActivity.this, LocationService.class);
			startService(location_poll_service);
			return true;
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Home Screen Start Core Services, Error starting services " + ex.getLocalizedMessage());
			return false;
		}
	}

	public void signout(View v) {
		if (PrefsUtil.removeUser(this)) {
			Toast.makeText(this, "Log Out was Successful ", Toast.LENGTH_SHORT).show();
			Button button1 = (Button) v;
			button1.setText(R.string.register);
			button1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					signIn(null);
				}
			});
			TextView greeting = (TextView) findViewById(R.id.greetingText);
			greeting.setText(R.string.welcome_guest);
		}
	}

	public void signIn(View view) {
		Log.d(TAG, "Signing In Process Request");
		Intent signIn = new Intent(MainActivity.this, RegisterActivity.class);
		startActivity(signIn);
	}

	public void setGreeting() {
		TextView greeting = (TextView) findViewById(R.id.greetingText);
		greeting.setText(String.format("%s%s", getString(R.string.welcome), PrefsUtil.getFirstName(this)));

		final Button button = (Button) findViewById(R.id.register);
		button.setText(R.string.logout);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				signout(v);
			}
		});
	}

	private void track() {
		try {
			////Analytics
			// Obtain the shared Tracker instance.
			App application = (App) getApplication();
			Tracker appTracker = application.getDefaultTracker();
			appTracker.setSessionTimeout(300); //starts a new session after username has placed the app in background for 300seconds
			appTracker.setClientId( PrefsUtil.getCountryId(getApplicationContext()) +"-"+ ((Integer) PrefsUtil.getUserId(getApplicationContext())).toString());
			Log.d("mF-ClientID", PrefsUtil.getCountryId(getApplicationContext()) +"-"+ ((Integer) PrefsUtil.getUserId(getApplicationContext())).toString());

			// This hit will be sent with the User ID value and be visible in
			// User-ID-enabled views (profiles).
			appTracker.send(new HitBuilders.EventBuilder()
					.setCategory("General Usage")
					.setAction("mFisheries, App Opened")
					.build());

			////
		}catch (Exception e) {
			Log.d("mFisheries-Analytics", "Tracker exception - " + e.getLocalizedMessage());
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		setGreeting();
		if (PrefsUtil.hasUserId(this)) {
			SIGNED_IN = true;
			if (!preferences.getBoolean("loc_service_stopped", true) && PrefsUtil.hasCountry(this))
				startLocationServices();
		} else {
			Log.d(TAG, "Application has no ID set");
		}
		registerListeners();
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterListeners();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if(downloadProgress != null)
			downloadProgress.dismiss();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "Destroying");
		if(downloadProgress != null)
			downloadProgress.dismiss();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent settings = new Intent(this, SettingsActivity.class);
			startActivity(settings);
		}
		if (id == R.id.action_add) {
			loadModules(country, countryId + "");
		}

		return super.onOptionsItemSelected(item);
	}

	public void registerListeners() {
		// Currently the only listener is to trigger the registration process after sign in is completed
		LocalBroadcastManager
				.getInstance(this)
				.registerReceiver(registerBroadcastHandler, new IntentFilter(RegisterActivity.BROADCAST_ID));

		registerReceiver(downloadReceiver, new IntentFilter(android.app.DownloadManager.ACTION_DOWNLOAD_COMPLETE));
		registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
	}

	public void unregisterListeners() {
		Log.d(TAG, "Unregister Receivers");
		LocalBroadcastManager
				.getInstance(this)
				.unregisterReceiver(registerBroadcastHandler);

		unregisterReceiver(downloadReceiver);
		unregisterReceiver(networkChangeReceiver);
	}

	public BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			//check if the broadcast message is for our enqueued download
			Log.d(TAG, "Download broadcast received");
			long referenceId = intent.getLongExtra(android.app.DownloadManager.EXTRA_DOWNLOAD_ID, -1);
			if(referenceId == downloader.lastReferenceId){
				Log.d(TAG, "Download completed");
				unzipDownload(downloader.lastModuleId);
			}
		}
	};

	private void unzipDownload(String moduleId) {
		Log.d(TAG, "Unzipping download");
		Intent intent = new Intent(this, ZipService.class);
		intent.putExtra("module_id", moduleId);
		intent.putExtra("id", (int) (Math.random() * 1024));
		startService(intent);
	}
}
