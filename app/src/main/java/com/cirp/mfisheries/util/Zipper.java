package com.cirp.mfisheries.util;

import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Zipper {

	private String zip_file;
	private String location;
	private OnZipFinishedListener listener;
	private boolean deleteAfterUnzip = false;

	public Zipper(String zipFile, String loc) {
		zip_file = zipFile;
		location = loc;

		makeDirectory("");
	}

	public void unzip() {
		try {
			FileInputStream inputStream = new FileInputStream(zip_file);
			ZipInputStream zipStream = new ZipInputStream(inputStream);
			ZipEntry zEntry;
			while ((zEntry = zipStream.getNextEntry()) != null) {
				Log.d("Unzip", "Unzipping " + zEntry.getName());
				if (zEntry.isDirectory()) {
					makeDirectory(zEntry.getName());
				} else {
					FileOutputStream fout = new FileOutputStream(location + zEntry.getName());
					BufferedOutputStream bufout = new BufferedOutputStream(fout);
					byte[] buffer = new byte[1024];
					int read;
					while ((read = zipStream.read(buffer)) != -1) {
						bufout.write(buffer, 0, read);
					}

					zipStream.closeEntry();
					bufout.close();
					fout.close();
				}
			}
			zipStream.close();
			if (deleteAfterUnzip) {
				deleteFile();
			}
			listener.onFinished();
		} catch (Exception e) {
			Log.d("Unzip", "Unzipping failed");
			e.printStackTrace();
		}

	}

	private void makeDirectory(String dir) {
		File f = new File(location + dir);

		if (!f.isDirectory()) {
			f.mkdirs();
		}
	}

	private boolean deleteFile() {
		File file = new File(zip_file);
		return file.delete();
	}

	public void setDeleteAfterUnzip(boolean deleteAfterUnzip) {
		this.deleteAfterUnzip = deleteAfterUnzip;
	}

	public void setOnZipFinishedListener(OnZipFinishedListener listener) {
		this.listener = listener;
	}

	public interface OnZipFinishedListener {

		void onFinished();

	}
}
