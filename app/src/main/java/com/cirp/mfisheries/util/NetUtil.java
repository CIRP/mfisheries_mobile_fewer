package com.cirp.mfisheries.util;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.cirp.mfisheries.R;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Date;

public class NetUtil {
	// production urls
	//	public static String SITE_URL = "https://mfisheries.cirp.org.tt/";
	// development urls
    public static String SITE_URL = "http://mfisheries.herokuapp.com/"; // remote dev
   // public static String SITE_URL = "http://mfisheries.cirp.org.tt:8080/"; // remote dev
//]    public static String SITE_URL = "http://192.168.1.143:8080/"; // local dev
	public static String API_URL = SITE_URL + "api/";
	public static String GOOGLE_AUTH = "oauth2:https://www.googleapis.com/auth/userinfo.profile";
	public static String GET_COUNTRIES_PATH = "countries";
	public static String GET_MODULES_PATH = "get/country/modules/";
	public static String GET_USER_INFO = "#/username/info/";
	public static String ADD_GOOGLE_USER = "add/google/user";
	public static String POST_TRACK_URL = "add/username/tracks";
	public static String POST_IMAGE_URL = "add/username/image";
	public static String ADD_LEK_PATH = "add/lek";
	public static String GET_LEK_PATH = "get/lek/";

	//Alerts
	public static String USER_GROUPS(int id){
		return "user/" + id +"/groups";
	}
	public static String SUBSCRIBE = "group/subscribe";
	public static String ALERTS = "alerts";

	public static String API_KEY = "S2cI9EHrJtXQdSAx2FetiXLdA626KtFZ";
	public static String API_KEY_PARAM = "apikey";

	public static String GET_LEK(int id, String type, String category){
		return API_URL + "lek/" + id + "/" + type + "/" + category;
	}

	public static String GET_LEK_DATE(int id, String type, String category, int day, int month, int year){
		return API_URL + "lek/" + id + "/" + type + "/" + category + "/" + day + "/" + (month + 1) + "/" + year;
	}

	public static String GET_LEK_CATEGORIES(int userid){
		return API_URL + "lek/categories/" + userid;
	}

	public static String GET_LEK_DATE_RANGE(int id, String type, String category, Date oldDate, Date laterDate){
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(oldDate);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(laterDate);

		return API_URL + "lek/" + id + "/" + type + "/" + category + "/default/" + cal1.get(Calendar.DAY_OF_MONTH) + "/"
				+ (cal1.get(Calendar.MONTH) + 1) + "/" + cal1.get(Calendar.YEAR) + "/" + cal2.get(Calendar.DAY_OF_MONTH) + "/"
				+ (cal2.get(Calendar.MONTH) + 1) + "/" + cal2.get(Calendar.YEAR);
	}

	public static boolean isOnline(Context context) {
		try {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			return cm.getActiveNetworkInfo().isConnectedOrConnecting();
		} catch (Exception e) {
			return false;
		}
	}

	public static int getFileSize(String url) {
		try {
			URL url1 = new URL(url);
			URLConnection urlConnection = url1.openConnection();
			urlConnection.connect();
			int file_size = urlConnection.getContentLength();
			Log.d("FileSize", "Size: " + file_size);
			return file_size / 1048576;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static void retryDialog(Context context, final OnRetryClicked onRetryClicked) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder
				.setTitle(context.getResources().getString(R.string.no_conn_dialog))
				.setCancelable(false)
				.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						onRetryClicked.retry();
					}
				})
				.show();
	}

	public interface OnRetryClicked {

		void retry();

	}

	public static boolean exists(String url) {
		return exists(url, "GET");
	}

	public static boolean exists(String url, String requestMethod) {
		try {
			System.out.println(url);
			URL u = new URL(url);
			HttpURLConnection huc = (HttpURLConnection) u.openConnection();
			huc.setRequestMethod(requestMethod);
			huc.connect();
			int code = huc.getResponseCode();
			System.out.println(code);

			return code == 200;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
