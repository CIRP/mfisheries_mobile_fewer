package com.cirp.mfisheries.util;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.cirp.mfisheries.MainActivity;

public class WebAppInterface {
	AppCompatActivity activity;

	public WebAppInterface(AppCompatActivity activity) {
		this.activity = activity;
	}

	@JavascriptInterface
	public void displayMessage(String message) {
		Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
	}

	@JavascriptInterface
	public void alert(String message) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
			builder.setTitle("mFisheries")
					.setMessage(message)
					.create()
					.show();
		} catch (Exception e) {
			Log.e("WebAppInterface", "Error occurred when attempting to alert" + e.getMessage());
		}
	}

	@JavascriptInterface
	public void goHome() {
		activity.startActivity(new Intent(activity, MainActivity.class));
		// attempt to close the web view activity
		this.activity.finish();
	}

	@JavascriptInterface
	public void completedRegistration() {
		if (PrefsUtil.setIsRegistered(activity, true)) {
			goHome();
		}
	}

}
