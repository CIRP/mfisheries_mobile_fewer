package com.cirp.mfisheries.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.cirp.mfisheries.RegisterActivity;


public class RegisterBroadcastHandler extends BroadcastReceiver {

	private AppCompatActivity activity;

	public RegisterBroadcastHandler(AppCompatActivity activity) {
		this.activity = activity;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent webViewIntent = new Intent(activity, RegisterActivity.class);
		activity.startActivity(webViewIntent);
	}
}
