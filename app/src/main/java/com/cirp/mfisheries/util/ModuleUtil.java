package com.cirp.mfisheries.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.core.ModuleFactory;

import java.util.List;

public class ModuleUtil {

	public static String[] MODULE_ORDER = {"Navigation", "Weather", "Alerts", "SOS", "FEWER", "Photo Diary", "LEK", "First Aid", "Podcast"};

	public static String PLACEHOLDER_TILE = "BLANK";

	public static boolean needsRegistration(Context context, List<String> modules) {
		ModuleFactory factory = ModuleFactory.getInstance(context);
		for (String moduleName : modules) {
			Log.d("ModuleUtil", moduleName);
			Module module = factory.getModule(moduleName);
			if (module != null) {
				if (module.needsRegistration)
					return true;
			} else {
				Log.d("ModuleUtil", "Module does not exist");
				Toast.makeText(context, "Module selected does not exist", Toast.LENGTH_SHORT).show();
			}
		}
		return false;
	}

}
