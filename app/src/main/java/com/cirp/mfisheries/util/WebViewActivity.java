package com.cirp.mfisheries.util;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.cirp.mfisheries.MainActivity;
import com.cirp.mfisheries.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.util.ArrayList;
import java.util.List;

public class WebViewActivity extends AppCompatActivity {

	private static final String TAG = "WebViewActivity";
	private WebView webView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view);
		Log.d(TAG, "Running WebView Activity");
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (toolbar != null) {
			setSupportActionBar(toolbar);
		}

		webView = (WebView) findViewById(R.id.webView);
		final String mimeType = "text/html";
		final String encoding = "UTF-8";
		//String html = "<br /><strong>Registering ...</strong></font>";

		String html = "<html><head>" +
				"<style type='text/css'>" +
				"html,body {margin: 0;padding: 0;width: 100%;height: 100%;}" +
				"html {display: table;}" +
				"body {display: table-cell;vertical-align: middle;text-align: center;}" +
				"</style></head>" +
				"<body><p>Registering ...</p></body></html>";
		webView.loadDataWithBaseURL("", html, mimeType, encoding, "");

		sendSelectedModules();
	}

	public void sendSelectedModules() {
		Log.d(TAG, "Send Selected Modules Called");
		try {
			JsonObject json = new JsonObject();
			JsonObject data = new JsonObject();
			final JsonArray modules = new JsonArray();

			String email = PrefsUtil.getEmail(this);
			String pwd = PrefsUtil.getGoogleId(this);
			String fname = PrefsUtil.getFirstName(this);
			String lname = PrefsUtil.getLastName(this);

			data.addProperty("username", fname.toLowerCase());
			data.addProperty("password", pwd);
			data.addProperty("fname", fname);
			data.addProperty("lname", lname);
			data.addProperty("email", email);
			data.addProperty("countryid", PrefsUtil.getCountryId(this));

			List<String> selectedModules = new ArrayList<>();
			List<String> tempList = FileUtil.loadSavedModules();
			for (String module : tempList) {
				selectedModules.add(module.replaceAll("\\s", ""));
			}

			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<List> jsonAdapter = moshi.adapter(List.class);

			String selected = jsonAdapter.toJson(selectedModules);

			JsonParser parser = new JsonParser();
			JsonArray mods = (JsonArray) parser.parse(selected);
			modules.addAll(mods);

			json.add("data", data);
			json.add("modules", modules);

			Log.d(TAG, "Making Request at: " + NetUtil.API_URL + NetUtil.ADD_GOOGLE_USER);
			Log.d(TAG, "With Data: " + json.toString());

			saveGoogleInfo(json);
		} catch (Exception ex) {
			Log.wtf(TAG, "Error generating json to send");
		}
	}

	private void saveGoogleInfo(JsonObject json) {
		Ion.with(getApplicationContext())
                .load("POST", NetUtil.API_URL + NetUtil.ADD_GOOGLE_USER)
                .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result == null) {
                            Log.d(TAG, "Result Null");
                            Toast.makeText(WebViewActivity.this, "Error, please register again", Toast.LENGTH_SHORT).show();

							toMain();
                            return;
                        }
                        Log.i(TAG, "Results: " + result.toString());
                        int resultStatus = result.get("status").getAsInt();
                        Log.d(TAG, "Result status code was: " + resultStatus);
                        if (resultStatus == 201) { //Created
                            Log.d(TAG, "Registered with Google. Now we register with mFisheries site");
                            PrefsUtil.setUserId(getApplicationContext(), result.get("userid").getAsInt());
                            String registerUrl = result.get("modules").getAsString();
                            String webViewUrl = NetUtil.SITE_URL + NetUtil.GET_USER_INFO + registerUrl;
                            Log.d(TAG, "Registering with URL: " + webViewUrl);

                            WebSettings webSettings = webView.getSettings();
                            webSettings.setJavaScriptEnabled(true);
                            webSettings.setDomStorageEnabled(true);
                            webView.addJavascriptInterface(new WebAppInterface(WebViewActivity.this), "mFisheriesAndroid");
                            webView.loadUrl(webViewUrl);
                        } else if (resultStatus == 200) {
                            Log.d(TAG, "Previously registered");
                            JsonObject data = result.getAsJsonObject("data");
                            PrefsUtil.setUserId(getApplicationContext(), data.get("id").getAsInt());

                            Toast.makeText(WebViewActivity.this, "Already registered", Toast.LENGTH_SHORT).show();
                            final String mimeType = "text/html";
                            final String encoding = "UTF-8";

                            String html = "<html><head>" +
                                    "<style type='text/css'>" +
                                    "html,body {margin: 0;padding: 0;width: 100%;height: 100%;}" +
                                    "html {display: table;}" +
                                    "body {display: table-cell;vertical-align: middle;text-align: center;}" +
                                    "</style></head>" +
                                    "<body><p>Already registered</p></body></html>";
                            webView.loadDataWithBaseURL("", html, mimeType, encoding, "");

                            Log.d(TAG, "Registration was previously completed");
                            PrefsUtil.setIsRegistered(WebViewActivity.this, true);
							toMain();

                        } else {
                            Toast.makeText(WebViewActivity.this, "Error, please register again", Toast.LENGTH_SHORT).show();
                            toMain();
                        }
                    }
                });
	}

	private void toMain(){
		Intent intent = new Intent(WebViewActivity.this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(this)
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle("Closing Registration")
				.setMessage("Are you sure you want to close the registration page without completing the sign-up process?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						toMain();
					}

				})
				.setNegativeButton("No", null)
				.show();
	}

}
