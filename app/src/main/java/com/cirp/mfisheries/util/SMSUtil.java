package com.cirp.mfisheries.util;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.telephony.SmsManager;
import android.util.Log;

import com.cirp.mfisheries.core.location.TrackPointDM;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SMSUtil {

    public static TrackPointDM createSOS(Location location, Context c) {
        SimpleDateFormat simple_date_format = new SimpleDateFormat("yyyy-M-d hh:mm:ss", Locale.ENGLISH);
        String sos_time_stamp = simple_date_format.format(new Date());
        TrackPointDM point = new TrackPointDM();
        point.acc = String.valueOf(location.getAccuracy());
        point.brg = String.valueOf(location.getBearing());
        point.lat = String.valueOf(location.getLatitude());
        point.lng = String.valueOf(location.getLongitude());
        point.prov = "1";
        point.rssi = "LocationService.signalStrengthInfo";
        point.spd = String.valueOf(location.getSpeed());
        point.time = sos_time_stamp;
        point.user = PrefsUtil.getUser(c);
        point.uId = PrefsUtil.getUserId(c);
        point.type = "sos";
        point.startId = 0;
        return point;
    }

    public static void sendSMS(Context context, String message, String number){
        PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, new Intent("SMS_SENT"), 0); // PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent deliveredIntent = PendingIntent.getBroadcast(context, 0, new Intent("SMS_DELIVERED"), 0); // PendingIntent.FLAG_UPDATE_CURRENT);

        Log.d("SMSUtil", "Sending SMS msg: " + message + "\n " + message.length() + "\n intents" + sentIntent.toString() + deliveredIntent.toString());

        ArrayList<PendingIntent> sendList = new ArrayList<>();
        sendList.add(sentIntent);

        ArrayList<PendingIntent> deliverList = new ArrayList<>();
        deliverList.add(deliveredIntent);

        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> parts = smsManager.divideMessage(message);
        smsManager.sendMultipartTextMessage(number, null, parts, sendList, deliverList);
    }
}
