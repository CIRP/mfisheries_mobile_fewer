package com.cirp.mfisheries.util;

import android.location.Location;
import android.support.annotation.NonNull;

import java.util.Locale;

public class LocationUtil {

    public static String convertLatitude(double latitude) {
        double abs_latitude = Math.abs(latitude);
        int deg = (int) abs_latitude;
        int min = (int) Math.abs((abs_latitude - deg) * 60);
        float sec = (float) Math.abs((((abs_latitude - deg) * 60) - min) * 60);
        if (latitude > 0) {
            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.format(Locale.US,"%.2f",sec)  + '"' + " N");
        } else {
            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.format(Locale.US,"%.2f",sec)  + '"' + " S");
        }
    }

    public static String convertLongitude(double longitude) {
        double abs_longitude = Math.abs(longitude);
        int deg = (int) abs_longitude;
        int min = (int) Math.abs((abs_longitude - deg) * 60);
        float sec = (float) Math.abs((((abs_longitude - deg) * 60) - min) * 60);
        if (longitude < 0) {
            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.format(Locale.US,"%.2f",sec) + '"' + " W");
        } else {
            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.format(Locale.US,"%.2f",sec) + '"' + " E");
        }
    }

    @NonNull
    public static String getLocationString(double lat, double lng) {
        Location location = new Location("prov");
        location.setLongitude(lng);
        location.setLatitude(lat);
        return convertLatitude(lat) + "     " + convertLongitude(lng);
    }
    
}
