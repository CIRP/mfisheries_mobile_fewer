package com.cirp.mfisheries.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.util.Log;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FileUtil {

	public static String FOLDER = "/mFisheries/";
	public static String MODULE_FILE_NAME = "modules.json";
	public static final String IMAGE_DIRECTORY_NAME = "mFisheriesImages";


	//reads a file containing json and returns it string
	public static String readJSONFile(String filePath) {

		File sdcard = Environment.getExternalStorageDirectory();

		//Get the text file
		File file = new File(sdcard, FOLDER + filePath);
		Log.d("FileUtil", "File Path is: " + file.getAbsolutePath());

		//Read text from file
		StringBuilder text = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				text.append(line);
				text.append('\n');
			}
			br.close();
		} catch (IOException e) {
			//You'll need to add proper error handling here
		}
		return text.toString();
	}

	//writes a string of json to a file
	public static void writeJSONFile(String json, String location, String fileName) throws Exception {
		File sdcard = Environment.getExternalStorageDirectory();

		File dir = new File(sdcard + FOLDER + location);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File file = new File(sdcard, FOLDER + location + fileName);
		FileOutputStream stream = new FileOutputStream(file);
		try {
			stream.write(json.getBytes());
		} finally {
			stream.close();
		}

	}

	//loads a json array and returns it as a List
	public static List loadJSONArray(String json) {
		Moshi moshi = new Moshi.Builder().build();
		JsonAdapter<List> jsonAdapter = moshi.adapter(List.class);

		try {
			return jsonAdapter.fromJson(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

	//loads the selected modules from the cache
	public static List loadSavedModules() {
		return loadJSONArray(readJSONFile(MODULE_FILE_NAME));
	}

	//saves the selected modules to the cache
	public static boolean saveModules(List<String> modules) {
		JSONArray mJSONArray = new JSONArray(modules);
		try {
			FileUtil.writeJSONFile(mJSONArray.toString(), "", FileUtil.MODULE_FILE_NAME);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static Uri getFileUri(String extra) {
		return Uri.parse(Environment.getExternalStorageDirectory().getPath() + FileUtil.FOLDER + extra);
	}

	public static Uri getAudioUri(String extra) {
		return Uri.parse("file://" + Environment.getExternalStorageDirectory().getPath() + FileUtil.FOLDER + extra);
	}

	public static String getAudioUriString(String extra) {
		return Environment.getExternalStorageDirectory().getPath() + FileUtil.FOLDER + extra;
	}

	public static Uri getOutputMediaFileUri(File mediaFile) {
		return Uri.fromFile(mediaFile);
	}

	public static boolean directoryExists(String dir) {
		File f = new File(dir);
		return f.isDirectory();
	}

	public static boolean fileExists(String dir) {
		Log.v("fileExists", "Dir: " + dir);
		File f = new File(dir);
		return f.isFile();
	}

	public static String getAbsolutePath(Context context, Uri uri, String type) {
		try {
			if (Build.VERSION.SDK_INT >= 19)
				return getAbsolutePathFromURI_API19(context, uri, type);
			return getAbsolutePathFromURI_API11to18(context, uri);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	@SuppressLint("NewApi")
	public static String getAbsolutePathFromURI_API19(Context context, Uri uri, String type) {
		String filePath = "";
		String wholeID = DocumentsContract.getDocumentId(uri);

		// Split at colon, use second item in the array
		String id = wholeID.split(":")[1];

		String[] column = {MediaStore.Images.Media.DATA};

		// where id is equal to
		String sel = "";
		Uri media = null;
		switch (type) {
			case "image":
				sel = MediaStore.Images.Media._ID + "=?";
				media = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				break;
			case "audio":
				sel = MediaStore.Audio.Media._ID + "=?";
				media = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
				break;
			case "video":
				sel = MediaStore.Video.Media._ID + "=?";
				media = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
				break;
		}

		Cursor cursor = context.getContentResolver().query(media, column, sel, new String[]{id}, null);

		int columnIndex = cursor.getColumnIndex(column[0]);

		if (cursor.moveToFirst()) {
			filePath = cursor.getString(columnIndex);
		}
		cursor.close();
		return filePath;
	}

	@SuppressLint("NewApi")
	public static String getAbsolutePathFromURI_API11to18(Context context, Uri contentUri) {
		String[] proj = {MediaStore.Images.Media.DATA, MediaStore.Audio.Media.DATA, MediaStore.Video.Media.DATA};
		String result = null;

		CursorLoader cursorLoader = new CursorLoader(context, contentUri, proj, null, null, null);
		Cursor cursor = cursorLoader.loadInBackground();

		if (cursor != null) {
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			result = cursor.getString(column_index);
		}
		return result;
	}

	public static String getFileType(String type, String path){
		String filetype= type + "/" + path.substring(path.indexOf(".") + 1, path.length());
		Log.d("FileUtil", filetype);
		return filetype;
	}

	public static String getFileNameFromPath(String path) {
		String[] pieces = path.split("/");
		return pieces[pieces.length - 1];
	}

	public static File getOutputMediaFile(Context context, String fileName) {
		if(fileName == null || fileName.equals(""))
			fileName = generateImageName(context);
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), FileUtil.IMAGE_DIRECTORY_NAME);
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(FileUtil.IMAGE_DIRECTORY_NAME, "Oops! Failed create " + FileUtil.IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		return new File(mediaStorageDir.getPath() + File.separator + fileName);
	}

	public static String generateImageName(Context context){
		long timeStamp = new Date().getTime();
		return "USR_" + PrefsUtil.getUserId(context) + "_IMG_" + timeStamp + ".jpg";
	}

	public static String generateVideoName(Context context){
		long timeStamp = new Date().getTime();
		return "USR_" + PrefsUtil.getUserId(context) + "_VID_" + timeStamp + ".mp4";
	}

	public static float megabytesAvailable() {
		File file = Environment.getExternalStorageDirectory();
		if (Build.VERSION.SDK_INT >= 19) {
			StatFs stat = new StatFs(file.getPath());
			long bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
			Log.d("Space", "Space: " + bytesAvailable);
			return bytesAvailable / (1024.f * 1024.f);
		} else {
			StatFs stat = new StatFs(file.getPath());
			long bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
			Log.d("Space", "Space: " + bytesAvailable);
			return bytesAvailable / (1024.f * 1024.f);
		}
	}

	public static byte[] getByteArray(Context context, Uri uri){
		try {
			InputStream iStream = context.getContentResolver().openInputStream(uri);
			return getBytes(iStream);
		}
		catch (Exception e){
			Log.d("FileUtil", "Error: " + e.getMessage());
		}
		return null;
	}

	public static byte[] getBytes(InputStream inputStream) throws IOException {
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];

		int len = 0;
		while ((len = inputStream.read(buffer)) != -1) {
			byteBuffer.write(buffer, 0, len);
		}
		return byteBuffer.toByteArray();
	}

}
