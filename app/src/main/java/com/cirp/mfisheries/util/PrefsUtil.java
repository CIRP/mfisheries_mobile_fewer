package com.cirp.mfisheries.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.cirp.mfisheries.RegisterActivity;

public class PrefsUtil {
    private static String first_tag = "FIRST_OPEN";
    private static String country_tag = "COUNTRY";
    private static String path_tag = "PATH";
    private static String country_id_tag = "COUNTRY_ID";
    private static String id_tag = "USER_ID";
    private static String fname_tag = "FNAME";
    private static String lname_tag = "LNAME";
    private static String email_tag = "EMAIL";

    private static String google_id_tag = "GOOGLE_ID";
    private static String token_tag = "TOKEN";

    private static String sos_email_tag = "SOS_EMAIL";
    private static String sos_mobile_tag = "SOS_MOBILE";
    private static String sos_land_tag = "SOS_LAND";
    private static String sos_extra_tag = "SOS_EXTRA_NUMBER";
    private static String sos_in_progress = "SOS_IN_PROGRESS";

    private static String has_lek = "HAS_LEK";

    public static boolean isFirstOpen(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(first_tag, true);
    }

    public static void setFirstOpen(Context context, boolean bool) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(first_tag, bool).apply();
    }

    public static void setUserId(Context context, int id) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putInt(id_tag, id).commit();
    }

    public static int getUserId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(id_tag, -999);
        //return 62;
    }

    public static boolean hasUserId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.contains(id_tag);
    }

    public static void setFirstName(Context context, String fname) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(fname_tag, fname).commit();
    }

    public static String getFirstName(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(fname_tag, "GUEST");
    }

    public static void setLastName(Context context, String lname) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(lname_tag, lname).commit();
    }

    public static String getLastName(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(lname_tag, "USER");
    }


    public static String getUser(Context context) {
        return getFirstName(context) +" "+ getLastName(context);
    }

    public static boolean setCountry(Context context, String country) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(country_tag, country).commit();
    }

    public static String getCountry(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(country_tag, "");
    }

    public static boolean setPath(Context context, String path) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(path_tag, path).commit();
    }

    public static String getPath(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(path_tag, "");
    }

    public static boolean hasCountry(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.contains(country_tag);
    }

    public static boolean setCountryId(Context context, String cId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(country_id_tag, cId).commit();
    }

    public static String getCountryId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(country_id_tag, "2");
    }

    public static void setEmail(Context context, String email) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(email_tag, email).commit();
    }

    public static String getEmail(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(email_tag, "");
    }

    public static void setToken(Context context, String token) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(token_tag, token).apply();
    }

    public static String getToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(token_tag, "");
    }

    public static void setGoogleId(Context context, String gId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(google_id_tag, gId).commit();
    }

    public static String getGoogleId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(google_id_tag, "");
    }

    public static void setSOS(Context context, String mobile, String land, String email) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(sos_mobile_tag, mobile).apply();
        preferences.edit().putString(sos_land_tag, land).apply();
        preferences.edit().putString(sos_email_tag, email).apply();
    }

    public static void setSOSMobile(Context context, String mobile) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(sos_mobile_tag, mobile).apply();
    }

    public static String getSOSMobile(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(sos_mobile_tag, "");
    }

    public static void setSOSLand(Context context, String land) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(sos_land_tag, land).commit();
    }

    public static String getSOSEmail(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(sos_land_tag, "");
    }

    public static void setSOSEmail(Context context, String email) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(sos_email_tag, email).commit();
    }

    public static String getSOSLand(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(sos_land_tag, "");
    }

    public static void setSOSExtraPhone(Context context, String number) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(sos_extra_tag, number).commit();
        Toast.makeText(context, "Saved successfully", Toast.LENGTH_SHORT).show();

    }

    public static String getSOSExtraPhone(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(sos_extra_tag, "");
    }

    public static boolean hasSOSExtraPhone(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.contains(sos_extra_tag);
    }

    public static void setLocService(Context context, boolean bool) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean("loc_service_stopped", bool).apply();
    }

    public static boolean setIsRegistered(Context context, boolean bool) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putBoolean(RegisterActivity.HAS_REGISTER_SET, bool).commit();
    }

    public static boolean getIsRegistered(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(RegisterActivity.HAS_REGISTER_SET, false);
    }

    public static boolean removeUser(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(RegisterActivity.HAS_REGISTER_SET);
        editor
                .remove(token_tag)
                .remove(google_id_tag)
                .remove(email_tag)
                .remove(lname_tag)
                .remove(fname_tag)
                .remove(id_tag);
        return editor.commit();
    }

    public static boolean isSOSInProgress(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(sos_in_progress, false);
    }

    public static void setSOSInProgress(Context context, boolean val) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(sos_in_progress, val).commit();
    }

    public static boolean hasLekToSync(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(has_lek, false);
    }

    public static void setLekToSync(Context context, boolean hasLEK) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(has_lek, hasLEK).commit();
    }

}