package com.cirp.mfisheries;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.cirp.mfisheries.core.BaseActivity;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.util.WebViewActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;

public class RegisterActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

	private static final int RC_SIGN_IN = 0;
	public static final String BROADCAST_ID = "mfisheries-require-register";
	public static final String HAS_REGISTER_SET = "registered_settings";
	private static final String TAG = "RegisterActivity";

	/* Client used to interact with Google APIs. */
	private GoogleApiClient mGoogleApiClient;
	private boolean mSignInClicked;
	private ConnectionResult mConnectionResult;
	private boolean mIntentInProgress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (toolbar != null) {
			setSupportActionBar(toolbar);
		}

		permissions = new String[]{Manifest.permission.GET_ACCOUNTS};
		requestPermissions();
	}

	@Override
	public void onPermissionGranted(String permission) {
		super.onPermissionGranted(permission);
		buildGAPIClient();

		signInWithGoogle();
	}

	private void buildGAPIClient() {
		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestEmail()
				.build();

		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.enableAutoManage(this, this)
				.addApi(Auth.GOOGLE_SIGN_IN_API, gso)
				.build();
	}

	protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
		if (requestCode == RC_SIGN_IN) {
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(intent);
			handleSignInResult(result);
		}
	}

	private void handleSignInResult(GoogleSignInResult result) {
		Log.d(TAG, "handleSignInResult:" + result.isSuccess());
		if (result.isSuccess()) {
			// Signed in successfully, show authenticated UI.
			GoogleSignInAccount acct = result.getSignInAccount();
			getProfileInfo(acct);
		} else {
			// Signed out, show unauthenticated UI.
			Toast.makeText(this, "You signed out", Toast.LENGTH_SHORT).show();
		}
	}

	private void getProfileInfo(GoogleSignInAccount acct){
		String email = acct.getEmail();
		String fname = acct.getGivenName();
		String lname = acct.getFamilyName();
		String googleId = acct.getId();
		Log.d(TAG, "Completed the sign in with google process");
		PrefsUtil.setFirstName(this, fname);
		PrefsUtil.setLastName(this, lname);
		PrefsUtil.setEmail(this, email);
		PrefsUtil.setGoogleId(this, googleId);
		Log.d(TAG, "Launching the Activity to Handle mFisheries Registration");
		Intent webViewIntent = new Intent(this, WebViewActivity.class);
		startActivity(webViewIntent);
	}

	public void onConnectionFailed(ConnectionResult result) {
		Log.d(TAG, "failed " + result.getErrorCode());
		Log.v(TAG, "Failed: " + result.toString() + "");

		if (!mIntentInProgress) {
			mConnectionResult = result;

			if (mSignInClicked) {
				resolveSignInError();
			}
		}
	}

	private void resolveSignInError() {
		if (mConnectionResult.hasResolution()) {
			try {
				mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
				mIntentInProgress = true;
			} catch (IntentSender.SendIntentException e) {
				// The intent was canceled before it was sent.  Return to the default
				// state and attempt to connect to get an updated ConnectionResult.
				Log.d(TAG, "resolving catch");
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		} else {
			showErrorDialog(mConnectionResult);
		}
	}

	private void showErrorDialog(ConnectionResult connectionResult) {
		GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
		int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

		if (resultCode != ConnectionResult.SUCCESS) {
			if (apiAvailability.isUserResolvableError(resultCode)) {
				apiAvailability.getErrorDialog(this, resultCode, RC_SIGN_IN,
						new DialogInterface.OnCancelListener() {
							@Override
							public void onCancel(DialogInterface dialog) {
								mSignInClicked = false;
							}
						}).show();
			} else {
				Log.w(TAG, "Google Play Services Error:" + connectionResult);
				String errorString = apiAvailability.getErrorString(resultCode);
				Toast.makeText(this, errorString, Toast.LENGTH_SHORT).show();

				mSignInClicked = false;
			}
		}
	}

	private void signInWithGoogle() {
		Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
		startActivityForResult(signInIntent, RC_SIGN_IN);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_register, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
