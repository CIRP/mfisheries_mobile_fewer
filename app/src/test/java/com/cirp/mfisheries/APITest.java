package com.cirp.mfisheries;

import android.location.Location;

import com.cirp.mfisheries.alerts.AlertDM;
import com.cirp.mfisheries.alerts.AlertsActivity;
import com.cirp.mfisheries.alerts.GroupAdapter;
import com.cirp.mfisheries.alerts.GroupDM;
import com.cirp.mfisheries.alerts.SendAlertActivity;
import com.cirp.mfisheries.core.Module;
import com.cirp.mfisheries.core.ModuleFactory;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.core.location.TrackPointDM;
import com.cirp.mfisheries.lek.LEKActivity;
import com.cirp.mfisheries.lek.LEKPost;
import com.cirp.mfisheries.sos.SOSService;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.ModuleUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.util.SMSUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 23)
public class APITest {

	private MainActivity activity;
	private String[] moduleNames;
	private ModuleFactory moduleFactory;
	private String DEV_API_URL = "http://mfisheries.cirp.org.tt:8080/api";

	@Before
	public void setup() {
		//ShadowGooglePlayServicesUtil.setIsGooglePlayServicesAvailable(ConnectionResult.SUCCESS);
		moduleNames = ModuleUtil.MODULE_ORDER;
		List<String> selectedModules = Arrays.asList(moduleNames);
		FileUtil.saveModules(selectedModules);
		activity = Robolectric.buildActivity(MainActivity.class).create().get();
		moduleFactory = ModuleFactory.getInstance(activity.getApplicationContext());
	}

	//Tests that each API endpoint used for retrieving data exists at the expected URL.
	@Test
	public void doGetEndpointsExist() {
		assertTrue(NetUtil.exists(NetUtil.API_URL + NetUtil.GET_COUNTRIES_PATH + "&apikey=RYsmkN92NbzYcrcFqEULHiAZNt5ItOHj"));
		assertTrue(NetUtil.exists(NetUtil.API_URL + NetUtil.GET_MODULES_PATH + "0"));
		assertTrue(NetUtil.exists(NetUtil.API_URL + NetUtil.GET_LEK_PATH + "1/all/0"));
		assertTrue(NetUtil.exists(NetUtil.API_URL + NetUtil.GET_LEK_PATH + "1/type1/0"));
		assertTrue(NetUtil.exists(NetUtil.API_URL + NetUtil.USER_GROUPS(1)));
		assertTrue(NetUtil.exists(NetUtil.API_URL + NetUtil.SUBSCRIBE));
	}

	//Tests that the countries the app is available to can be loaded from the server and the countries received are supported in the app.
	@Test
	public void areCountriesAvailable() {
		Ion.with(activity)
				.load(NetUtil.API_URL + NetUtil.GET_COUNTRIES_PATH)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						if (e != null)
							fail("Countries not available " + e.getMessage());
						else {
							int status = result.get("status").getAsInt();
							assertEquals(200, status);
						}
					}
				});
	}

	//Ensures the phone numbers and email addresses of the authority for each country is available.
	@Test
	public void hasAuthorityData() {
		Ion.with(activity)
				.load(NetUtil.API_URL + NetUtil.GET_COUNTRIES_PATH)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						if (e != null)
							fail("Auth not available " + e.getMessage());
						else {
							int status = result.get("status").getAsInt();
							if (status != 200) {
								fail("Error Occurred reading authorities from server");
							} else {
								JsonArray array = result.getAsJsonArray("data");
								for (int i = 0; i < array.size(); i++) {
									JsonObject object = array.get(i).getAsJsonObject();
									assertTrue(object.has("authority"));
									assertTrue(object.getAsJsonObject("authority").has("mobileNum"));
									assertTrue(object.getAsJsonObject("authority").has("email"));
								}
							}
						}
					}
				});
	}

	//Ensures that modules are available on the server for each available country.
	@Test
	public void canGetModules() {
		int[] countryIds = new int[]{1, 2, 3, 4};
		// For each country code
		for (int cid : countryIds) {
			Ion.with(activity)
					.load(NetUtil.API_URL + NetUtil.GET_MODULES_PATH + cid)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							if (e != null)
								fail("Modules not available " + e.getMessage());
							else {
								int status = result.get("status").getAsInt();
								if (status != 200) {
									fail("Error Occurred reading modules from server");
								} else {
									// Retrieve Modules for specific country
									JsonArray array = result.getAsJsonArray("data");
									// Country should have at least one module
									assertTrue(array.size() > 0);
								}
							}
						}
					});
		}

	}

	//Tests that the downloadable module data is available at the expected URLs.
	@Test
	public void doModuleDownloadsExist() {
		PrefsUtil.setPath(activity, "static/country%5Fmodules/trinidad");
		for (String name : moduleNames) {
			System.out.println(name + "");
			Module module = moduleFactory.getModule(name);
			if (module == null)
				fail("Module " + name + " is not returned by factory");
			if(module.hasDownload()) {
				try {
					assertTrue(NetUtil.exists(module.getDataLoc()));
				} catch (Exception e) {
					System.out.println(e.getMessage() + "error");
				}
			}
			else{
				System.out.println("No download");
			}
		}
	}

	@Test
	public void canSendTrack() {
		Location loc = new Location("");
		loc.setLatitude(11.2956943);
		loc.setLongitude(-60.8228397);
		loc.setBearing(231);
		loc.setSpeed(100);

		Location loc2 = new Location("");
		loc2.setLatitude(22.2956943);
		loc2.setLongitude(60.8228397);
		loc2.setBearing(31);
		loc2.setSpeed(10);

		ArrayList<TrackPointDM> points = new ArrayList<>();

		LocationService service = Robolectric.buildService(LocationService.class).attach().create().get();

		TrackPointDM trackPoint1 = service.createTrackPoint(loc, 1);
		TrackPointDM trackPoint2 = service.createTrackPoint(loc2, 1);
		points.add(trackPoint1);
		points.add(trackPoint2);

		JsonArray jsonArray = service.getJsonElements(points);
		assertNotNull(jsonArray);

		service.sendTracks(points, new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				assertNull(e);
				int status = result.get("status").getAsInt();
				assertEquals(201, status);
			}
		});
	}

	@Test
	public void canSendSOS() {
		Location location = new Location("");
		location.setLatitude(11.2956943);
		location.setLongitude(-60.8228397);
		location.setBearing(231);
		location.setSpeed(100);

		TrackPointDM trackPointDM = SMSUtil.createSOS(location, activity);

		ArrayList<TrackPointDM> points = new ArrayList<>();
		points.add(trackPointDM);

		SOSService service = Robolectric.buildService(SOSService.class).attach().create().get();
		JsonArray jsonArray = service.getJsonElements(points);
		assertNotNull(jsonArray);

		service.sendSOS(jsonArray, new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				if(e != null)
					fail("SOS not sent " + e.getMessage());
				int status = result.get("status").getAsInt();
				assertEquals(201, status);
			}
		});
	}

	@Test
	public void canSubscribe() {
		GroupAdapter groupAdapter = new GroupAdapter(activity, new ArrayList<GroupDM>());
		GroupDM groupDM = new GroupDM(1, "group", false, 1);
		groupAdapter.subscribe(groupDM, null);
	}

	@Test
	public void canSendAlert() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d hh:mm:ss", Locale.ENGLISH);

		AlertDM alert = new AlertDM(1, 1, "10.281", "-61.459", format.format(new Date()), "lorem ipsum");

		SendAlertActivity activity = Robolectric.buildActivity(SendAlertActivity.class).create().get();

		JsonObject jsonObject = alert.getJsonObject();
		assertNotNull(jsonObject);

		activity.sendAlert(jsonObject, new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				if (e == null && result != null) {
					int status = result.get("status").getAsInt();
					assertEquals(200, status);
				} else {
					assertNull(e);
				}
			}
		});
	}

	@Test
	public void canSendLEK() {
		LEKPost lekPost = new LEKPost();
		lekPost.latitude = "10.281";
		lekPost.longitude = "-61.459";
		lekPost.applicableDate = new Date().toString();
		lekPost.text = "lorem ipsum dolor";
		lekPost.timestamp = new Date().toString();
		lekPost.filetype = "text/*";
		lekPost.userid = "1";
		lekPost.countryid = "1";

		LEKActivity lekActivity = Robolectric.buildActivity(LEKActivity.class).create().get();
		lekActivity.sendLEK(lekPost, null, new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				if (e != null)
					fail("LEK not sent " + e.getMessage());
				else {
					int status = result.get("status").getAsInt();
					assertEquals(201, status);
				}
			}
		});
	}

}